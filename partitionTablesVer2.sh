#!/bin/bash
echo paritions table into N partitions, each partition is stored in its own .db file AS tableName.db

echo usage: 1st param: table name, 2nd param is database to attach, 3rd param is NoOfPartitions to create
table=$1
echo table is $table
db2=$2
echo database to attach is $db2
echo number of paritions to create is $3

#RANDOMLY PARTITIONING AN SQLite Table
#PERMUTING A TABLE
echo PREMUTING TABLE $1
permutedTable="permuted""$table"
echo PERMUTED TABLE name  is $permutedTable
#sqlite3 $2 "CREATE TABLE $permutedTable AS SELECT * FROM $table ORDER BY RANDOM();"

#create tempory table to hold a randomized copy of table
echo create tempory table to hold a randomized copy of table
sqlite3 $2 "CREATE TABLE temp AS SELECT * FROM $table ORDER BY RANDOM();"

#base schema of permuted table on schema of table
schema="$(sqlite3  $2 ".schema salaries")"
echo $schema
#substitute permuted table name for table name in schema string
schema="${schema/$1/$permutedTable}"
echo $schema
#create permuted table based upon schema derived from table
sqlite3 $2 "$schema"

#load permuted table with contents of randomized temporary table(temp) preserving schema derived from table
sqlite3 $2 "INSERT INTO $permutedTable SELECT * FROM temp;"
sqlite3 $2 "DROP TABLE temp;"

#DIVIDING THE PERMUTED TABLE INTO PARTIONS OF SIZE X, {part0, part1, ... partN}
size=$(sqlite3 $2 "SELECT COUNT(*) FROM $permutedTable;")
echo table size is $size
X=$(($size / $3))
echo partition size X is $X

partNo=1
while [ $partNo -le $3 ]
do
   echo iteration $partNo
   database="$table""Part_""$partNo"".db"
   tablePart="$table""Part_""$partNo"

   echo new partition database: $database
   echo new partition table: $tablePart

   #substitute permuted table name for partition table name in schema string
   partSchema="${schema/$permutedTable/$tablePart}"
   echo new table partition schema:  $partSchema
   #create  partition database and table based upon schema derived from table
   sqlite3 $database "$partSchema"

   #load parition table with contents of randomized permuted table preserving schema derived from table
   echo load parition table, $tablePart,  with contents of randomized permuted table preserving schema derived from table
   sqlite3 $database "ATTACH '$db2' AS attached;  INSERT INTO $tablePart SELECT * FROM attached.$permutedTable WHERE  rowId > ($X * ($partNo -1)) AND rowId <= ($X * ($partNo));"


   ((partNo++))
done

sqlite3 $2 "DROP TABLE $permutedTable;"

echo All done

exit 0
