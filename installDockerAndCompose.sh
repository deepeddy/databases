#!/bin/bash
echo usage: <host name> <sudo passwd>
echo installs Docker engine and Docker-Compose on top of Ubuntu image
echo "1st argument is " $1
echo "2nd argument is " $2
passwd=$2
host=$1
echo $passwd | sudo -S sed -i '1s/^/127.0.0.1 '$host'\n/' /etc/hosts

echo "INSTALL Docker engine"
sudo -y apt-get update

sudo -y apt-get install \
   linux-image-extra-$(uname -r) \
   linux-image-extra-virtual 

sudo -y apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

docker-ce | 17.06.2~ce-0~ubuntu | https://download.docker.com/linux/ubuntu xenial/stable amd64 Packages

echo INSTALL Docker-Compose
chgrp cc /usr/local/bin
chmode g=rwx

curl -L https://github.com/docker/compose/releases/download/1.16.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

exit 0
