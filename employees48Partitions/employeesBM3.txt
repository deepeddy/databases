# BENCH MARK THREE
#  All employees that worked less than 20 years
#
# SELECT employees.emp_no, salaries.salary FROM salaries, employees WHERE  
#    (CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT)  BETWEEN 1 AND 7)
#    AND salaries.salary < 40000 AND employees.emp_no  < 30000 ORDER BY employees.emp_no DESC;
#
#	Type		Op		Predicate			L	R	Degree	Probe(JOIN)
1	COMPUTE		ORDER	ORDER BY employees.emp_no DESC		2	null	3
2	COMPUTE		PROJECT	employees.emp_no, salaries.salary	3	null	6
3	COMPUTE		JOIN	(CAST(julianday(salaries.to_date) - julianday(employees.hire_date) AS INT)  BETWEEN 1 AND 7)	4	5	48	4
4	COMPUTE		SELECT	salaries.salary < 40000		6	null	48
5	COMPUTE		SELECT	employees.emp_no  < 30000		7	null	1
6	DATA_ONLY	${HOME}/databases/employees48Partitions/employees.db	[salariesPart_1,salariesPart_2,salariesPart_3,salariesPart_4,salariesPart_5,salariesPart_6,salariesPart_7,salariesPart_8,salariesPart_9,salariesPart_10,salariesPart_11,salariesPart_12,salariesPart_13,salariesPart_14,salariesPart_15,salariesPart_16,salariesPart_17,salariesPart_18,salariesPart_19,salariesPart_20,salariesPart_21,salariesPart_22,salariesPart_23,salariesPart_24,salariesPart_25,salariesPart_26,salariesPart_27,salariesPart_28,salariesPart_29,salariesPart_30,salariesPart_31,salariesPart_32,salariesPart_33,salariesPart_34,salariesPart_35,salariesPart_36,salariesPart_37,salariesPart_38,salariesPart_39,salariesPart_40,salariesPart_41,salariesPart_42,salariesPart_43,salariesPart_44,salariesPart_45,salariesPart_46,salariesPart_47,salariesPart_48]	[emp_no int(11); salary int(11); from_date date; to_date date]                                       
7	DATA_ONLY	${HOME}/databases/employees48Partitions/employees.db	employees	[emp_no int(11) PRIMARY; birth_date date; first_name  varchar(14); last_name  varchar(16); gender text; hire_date date]
#8   DATA_ONLY	${HOME}/databases/salariesPart_1.db		salariesPart_1	[emp_no int(11); salary int(11); from_date date; to_date date]
