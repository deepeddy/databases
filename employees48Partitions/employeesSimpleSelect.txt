% BENCH MARK AGGREGATE SUM 
% All employees that make less than $200,000 annually
%
% SELECT salaries.salary, salaries.emp_no FROM salaries WHERE salaries.salary < 200000;  
% 
#
#	Type		Op		Predicate															L	R	Degree	Probe(JOIN)
1	COMPUTE		SELECT	salaries.salary, salaries.emp_no WHERE salaries.salary < 200000		2	null	48
2	DATA_ONLY	${HOME}/databases/employees48Partitions/employees.db	[salariesPart_1,salariesPart_2,salariesPart_3,salariesPart_4,salariesPart_5,salariesPart_6,salariesPart_7,salariesPart_8,salariesPart_9,salariesPart_10,salariesPart_11,salariesPart_12,salariesPart_13,salariesPart_14,salariesPart_15,salariesPart_16,salariesPart_17,salariesPart_18,salariesPart_19,salariesPart_20,salariesPart_21,salariesPart_22,salariesPart_23,salariesPart_24,salariesPart_25,salariesPart_26,salariesPart_27,salariesPart_28,salariesPart_29,salariesPart_30,salariesPart_31,salariesPart_32,salariesPart_33,salariesPart_34,salariesPart_35,salariesPart_36,salariesPart_37,salariesPart_38,salariesPart_39,salariesPart_40,salariesPart_41,salariesPart_42,salariesPart_43,salariesPart_44,salariesPart_45,salariesPart_46,salariesPart_47,salariesPart_48]	[emp_no int(11); salary int(11); from_date date; to_date date]
