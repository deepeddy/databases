#!/bin/bash
echo paritions table into N partitions, each partition is stored in its own .db file AS tableName.db

echo usage: 1st param: table name, 2nd param is database to attach, 3rd param is NoOfPartitions to create
table=$1
echo table is $table
db=$2
echo database to attach is $db2
noParts=$3
echo number of paritions to create is $noParts

# PARTITIONING AN SQLite Table

#get schema of  table
schema="$(sqlite3  $db ".schema $table")"
echo $schema


#DIVIDING THE TABLE INTO PARTIONS OF SIZE X, {part0, part1, ... partN}
size=$(sqlite3 $db "SELECT COUNT(*) FROM $table;")
echo table size is $size
X=$(($size / $noParts))
echo partition size X is $X

partNo=1
while [ $partNo -le $noParts ]
do
   echo iteration $partNo
   database="$table""Part_""$partNo"".db"
   tablePart="$table""Part_""$partNo"

   echo new partition database: $database
   echo new partition tablename: $tablePart

   #substitute table name for partition table name in schema string
   partSchema="${schema/$table/$tablePart}"
   echo new table partition schema:  $partSchema
   #create  partition database and table based upon schema derived from table
   sqlite3 $database "$partSchema"

   #load parition table with contents of table preserving schema derived from table
   echo load parition table, $tablePart,  with contents of  table preserving schema derived from table
   sqlite3 $database "ATTACH '$2' AS attached;  INSERT INTO $tablePart SELECT * FROM attached.$table WHERE  rowId > ($X * ($partNo -1)) AND rowId <= ($X * ($partNo));"


   ((partNo++))
done


echo All done

exit 0
