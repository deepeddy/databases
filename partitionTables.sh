#!/bin/bash
echo paritions table into N partitions, each partition is stored in its own .db file AS tableName.db

echo usage: 1st param: table name, 2nd param is database to attach, 3rd param is NoOfPartitions to create
table=$1
echo table is $table
db2=$2
echo database to attach is $db2
echo number of paritions to create is $3

#RANDOMLY PARTITIONING AN SQLite Table
#PERMUTING A TABLE
echo PREMUTING TABLE $1
permutedTable="permuted""$table"
echo PERMUTED TABLE is $permutedTable
sqlite3 $2 "CREATE TABLE $permutedTable AS SELECT * FROM $table ORDER BY RANDOM();"

#DIVIDING THE PERMUTED TABLE INTO PARTIONS OF SIZE X, {part0, part1, ... partN}
size=$(sqlite3 $2 "SELECT COUNT(*) FROM $permutedTable;")
echo table size is $size
X=$(($size / $3))
echo partition size X is $X

partNo=1
while [ $partNo -le $3 ]
do
echo iteration $partNo
database="$table""Part_""$partNo"".db"
tablePart="$table""Part_""$partNo"

echo $database
echo $tablePart
echo sqlite3 $database "ATTACH '$db2' AS db2;  CREATE TABLE $tablePart AS SELECT * FROM db2.$permutedTable WHERE  rowId > ($X * ($partNo -1)) AND rowId <= ($X * ($partNo));"

sqlite3 $database "ATTACH '$db2' AS db2;  CREATE TABLE $tablePart AS SELECT * FROM db2.$permutedTable  WHERE  rowId > ($X * ($partNo -1)) AND rowId <= ($X * ($partNo));"

((partNo++))
done

echo All done

exit 0
