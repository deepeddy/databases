/***************************************************************************
                  osdb.c  -  OSDB Driver Program
                     -------------------
    begin        : Fri Dec  1 09:23:32 EST 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb.c,v $
Revision 1.57  2004/10/19 15:08:36  ariebs
Reduce extraneous output from all but rank 0 on MPI jobs.

Revision 1.56  2004/10/17 13:39:22  ariebs
Reimplement restrictions as run-time option, rather than as a compile-time
option.

Revision 1.55  2004/10/12 01:57:00  ariebs
Tweak the help text.

Revision 1.54  2004/10/11 22:17:41  ariebs
Make the help message helpful!

Revision 1.53  2004/10/11 02:35:27  ariebs
Make the "hep" text helpful.

Revision 1.52  2004/10/10 15:37:52  ariebs
Clean up "make install" to handle "make mpi" nicely. Note that
"make install" no longer does an implicit "make".

Revision 1.51  2004/10/10 14:43:00  ariebs
Clean up "make mpi" and replace "make LCD=1" with "make MYSQL=1" to allow
additional db-specific ifdefs in the future.

Revision 1.50  2004/10/08 20:07:51  ariebs
Implement the "least common denominator" compile-time flag.

Revision 1.49  2004/10/07 21:23:41  ariebs
Add simple MPI support (only rank 0 is allowed to do destructive tests,
single user tests are always skipped).

Revision 1.48  2004/03/09 00:11:22  ariebs
Clean up the "integer constant to large" warning.

Revision 1.47  2004/02/18 11:45:36  ariebs
Move the GPL notice to the usage message, tidy up the output a bit.

Revision 1.46  2003/06/04 02:28:58  ariebs
Fix the names of --dbhost, --dbuser, etc., to be --host, --user, ...

Revision 1.45  2003/05/15 03:10:31  ariebs
Implement --dbuser, --dbpassword, --dbhost, and --dbport for embedded pgsql.

Revision 1.44  2003/05/14 03:48:11  ariebs
Implement --dbuser and --dbpassword.

Revision 1.43  2003/05/14 02:10:11  ariebs
Clean up printing the header so that "make test" output will make sense.

Revision 1.42  2003/05/13 11:13:37  ariebs
Another minor tweak to the message formatting.

Revision 1.41  2003/05/13 11:08:49  ariebs

Tweak a formatting problem.

Revision 1.40  2003/05/07 00:57:48  ariebs
Remove one more extraneous set of quotes.

Revision 1.39  2003/05/07 00:42:50  ariebs
Remove the extraneous quotation marks from section headings. (They were
originally there to make it easy to import the results into a spreadsheet,
but they just look ugly to the human eye.)

Revision 1.38  2003/05/06 23:09:13  ariebs
Report the version number on execution.

Revision 1.37  2003/04/22 10:17:22  ariebs
Remove the "EXPERIMENTAL!" warnings.

Revision 1.36  2003/03/22 02:48:52  ariebs
Check in the changes to create data files.

Revision 1.35  2002/12/08 04:13:32  ariebs
Check the value supplied for --size

Revision 1.34  2002/12/01 20:32:04  ariebs
Make --size accept a proper argument. Note that the argument may be
postfixed with "MB" or "M" for megabyte (10**6), or "MiB" or "Mi" for
mebibyte (2**20).

Revision 1.33  2002/11/12 03:37:51  ariebs
Replace the RAND macro with a routine that correctly handles negative
minimum values

Revision 1.32  2002/10/06 16:06:57  ariebs
Making serious progress towards create_data() for the embedded SQL code;
but not yet ready for prime time.

Revision 1.31  2002/10/03 02:44:22  ariebs
Commit the next chunk of data creation changes (but it doesn't work yet!)

Revision 1.30  2002/07/12 21:32:36  ariebs
Apply Neil Conway's cleanups for compiler warnings

Revision 1.29  2002/06/03 01:17:02  ariebs
The key fix is resetting dontHangUp=1 before calling runUntilHUP(). Other
changes here reflect minor things I encountered while debugging on Solaris.

Revision 1.28  2002/05/24 23:23:16  ariebs
Handle the case of processes dying later than we expected in the multiuser
tests. (AFAIK, this is only a problem on Solaris.)

Revision 1.27  2002/05/22 02:45:13  ariebs
Fix failures on Solaris due to waitpid() expiring unexpectedly.

Revision 1.26  2002/05/20 04:15:19  ariebs
Fix a simple error in waiting for a process to terminate

Revision 1.25  2002/05/18 18:14:39  ariebs
Fix uninitialized OSDB_ERROR.text which causes a failure on Solaris

Revision 1.24  2002/03/13 00:09:51  ariebs
Print the waitpid() return value on failure in step 8 like in step 2 of
the multiuser tests.

Revision 1.23  2002/03/12 03:58:34  ariebs
Clean up the usage() message formatting.

Revision 1.22  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.21  2002/01/29 00:10:13  ariebs
Apply Peter Eisentraut's portability patches.

Revision 1.20  2001/10/10 02:51:13  ariebs
Implement --strict_conformance option, allowing implementers to skip or work around missing features
(e.g. PostgreSQL prefers not to use HASH indexes) -- the default is FALSE, i.e., allow work arounds

Revision 1.19  2001/09/13 08:05:30  vapour
Replaced the --nogeneratedata option with --generate and --size n.  Improved the output readability.

Revision 1.18  2001/09/12 05:11:12  vapour
Started adding structures for --nogeneratedata & improved output readability

Revision 1.17  2001/09/09 06:05:32  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.16  2001/08/17 01:18:22  ariebs
Set routineName="mu_oltp_update()", as appropriate, to report more
helpful error messages when deadlocks occur

Revision 1.15  2001/08/15 21:45:16  ariebs
Include --help as a synonym for --usage

Revision 1.14  2001/07/25 01:40:48  ariebs
Replace the obsolete "CLK_TCK" with a call to sysconf(_SC_CLK_TCK)

Revision 1.13  2001/04/14 16:02:36  ariebs
Fix the fix: "--osdblogfile" resulted from the blind change of "log"
to "osdblog"...

Revision 1.12  2001/04/13 15:46:20  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

Revision 1.11  2001/03/28 00:06:33  ariebs
Reflect the decision to separate program-control.c from osdb.c

Revision 1.10  2001/03/25 12:46:15  ariebs
Clean up programFeed2()'s --watch output to show both strings
concatenated on a single line to better reflect what is getting
passed to the program.

Revision 1.9  2001/03/22 02:40:25  ariebs
It seems that some programs may need .005 of a second to complete
their output before they terminate (search for "tv.rv_usec=5000").
This algorithm seems to work OK, but I'm still not real happy with it.

Revision 1.8  2001/03/18 02:40:32  ariebs
Add support for osdb-inf-ui

Revision 1.7  2001/03/15 03:37:24  ariebs
Seems we can safely wait just 1/1000 of a second to ensure that we've
got all output from a child process

Revision 1.6  2001/03/11 20:40:53  ariebs
Fix programGetOutput() to eliminate the 5-10 second/test penalty

Revision 1.5  2001/02/23 20:38:27  ariebs
Cosmetic changes to
(a) Use forkpty() (so I added LDFLAGS to all the makefiles while I was
    at it, and
(b) Add "diagnostics()" to osdb.c (which perturbed most programs)

Revision 1.3  2001/02/21 03:40:50  ariebs
Make programStartPipe() and osdb-my-ui work better together (but still a
couple of problems to go)

Revision 1.2  2001/02/18 14:30:30  ariebs
Implement programStartPty() for MySQL, and eliminate my hideous patch
for the mysql program. Also renamed "programStart()" to
"programStartPipe()"

*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

/*
 * This program implements OSDB, "the Open Source Database
 * Benchmark," which is based on the "ANSI SQL Standard Scalable
 * and Portable Benchmark for Relational Database Systems," as
 * described at <http://www.benchmarkresources.com/handbook/5.html>.
 *
 * The paper at that URL, written by Carolyn Turbyfill, Cyril Orji,
 * and Dina Bitton, seems to be the 2nd draft of a chapter that was
 * (will be?) used in Jim Gray's "Benchmark Handbook." Currently
 * out of print (as of November, 2000), it appears that Morgan
 * Kaufmann Publishers plan to update or reissue it in the near
 * future. For the moment (01 December 2000), the Web site at
 * <http://www.benchmarkresources.com/handbook/introduction.asp>
 * is "please to present this online book, The Benchmark Handbook,
 * Edited by Jim Gray free of charge to our readers."
 *
 * Signal usage
 *
 *   SIGCHLD is handled to acknowledge the termination of subprocesses;
 *   this is required in order for waitpid() to be useful.
 *
 *   SIGHUP -- the parent process uses SIGHUP to indicate that the
 *   children should terminate at the conclusion of their current loop.
 *   It is also used to tell everyone to go away if anyone encounters
 *   a fatal error.
 *
 * formatting note
 *
 *    We use the vim commands "ts=4" (tabstop=4) and "et" (expand
 *    tabs to spaces) to try to ensure readable, consistent formatting.
 *
 * MPI
 *    If the symbol "MPI" is defined (via "-D" to gcc), we run N_RANKS
 *    complete copies of OSDB. This allows us to control both the number
 *    of processors used in multiprocessor MPI systems, and the load 
 *    that we place on each one.
*/

#define osdb_main_c 1
#include "osdb.h"

#ifdef MPI
#include "mpi.h"
    int my_rank,
    	Nranks;
#endif /* MPI */

int  restrict_insert_into=0,
     restrict_rollback=0,
     restrict_subqueries=0,
     restrict_views=0;
long volatile dontHangUp;
long    ticksPerSecond;
void    processRestrictions(),
	usage();

void
crossSectionTests(char *mode) {
    int     startTime;
    struct  tms t;

    startTime=times(&t);
    timeIt("sel_1_ncl()",sel_1_ncl);
    if (!restrict_subqueries)
        timeIt("agg_simple_report()",agg_simple_report);
#ifdef MPI
    if (my_rank==0)
#endif /* MPI */
    {
        if (!restrict_insert_into) {
            timeIt("mu_sel_100_seq()",mu_sel_100_seq);
            timeIt("mu_sel_100_rand()",mu_sel_100_rand);
            timeIt("mu_mod_100_seq_abort()",mu_mod_100_seq_abort);
            timeIt("mu_mod_100_rand()",mu_mod_100_rand);
            timeIt("mu_unmod_100_seq()",mu_unmod_100_seq);
            timeIt("mu_unmod_100_rand()",mu_unmod_100_rand);
	}
        startTime=times(&t)-startTime;
        fprintf(osdblog, "crossSectionTests(%s)\t%.02f\n",
            mode, (double)startTime/ticksPerSecond);
    }
}


void
diagnostics() {
    if (errno)
        perror("perror() reports");
    if (OSDB_ERROR.error)
        fprintf(stderr,
            "OSDB_ERROR: %s, %s\n",
            OSDB_ERROR_NAMES[OSDB_ERROR.error], OSDB_ERROR.text);
}


char*
elapsedTime(int T) {
    static  char   res[16];
    double  seconds;
    int     hours,
            minutes;

    hours=T/(3600*ticksPerSecond);
    T=T%(3600*ticksPerSecond);
    minutes=T/(60*ticksPerSecond);
    seconds=((double)(T%(60*ticksPerSecond)))/(double)ticksPerSecond;
    snprintf(res, 16, "%d:%02d:%05.2f", hours, minutes, seconds);
    return res;
}


void
fieldHups(int sig) {
    if (watch) {
        printf("\nSIGHUP/SIGINT handler invoked: (%d)\n", myPid);
        fflush(stdout);
    }
    if (iAmParent) {
        fflush(osdblog);
        if (sig==SIGHUP) {
            fprintf(stderr,"\n\nsomeone sighup'd the parent\n");
            exit(1);
        }
        /* Hmmm... user must have interrupted us */
        fprintf(stderr,"\n\nTerminated by user\n");
        kill(-getpid(),SIGHUP);
        sleep(5);
        exit(1);
    }
    dontHangUp=0;
}


void
harvestZombies(int sig) {
    /* Simply reset the signal handler for SIGCHLD */
    if (signal(SIGCHLD, harvestZombies)==SIG_ERR)
        BUGOUT("SIG_ERR");  /* This is triggered when setting the new signal handler fails.  JC */

    if (watch) {
        if (sig==SIGCHLD) {
            fflush(stdout);
            printf("\nSIGCHLD handler invoked\n");
            fflush(stdout);
        }
        else
            printf("SIGCHLD handler initialized\n");
    }
}



char*
left40(char* inPtr) {
    static char outStg[41];
    int         i;

    for (i=0; (i<40) && inPtr[i] && (inPtr[i]!='\n'); i++)
        outStg[i]=inPtr[i];
    outStg[i]=0;
    return outStg;
}


void
locked_pause() {
    struct  timeval tv;
    tv.tv_sec=0;
    tv.tv_usec=RAND(50,1000);    /* pause for .0005 to .001 seconds */
    select(0, NULL, NULL, NULL, &tv);
}


int
main(int argc, char* argv[]) {
    char    *elapsed;
    int     clocks,
            dbSize;
    struct  tms tmsStart,
                tmsEnd;

    ticksPerSecond=sysconf(_SC_CLK_TCK);
    currentTest=(char*)"Test Initialization";
    OSDB_ERROR.text=(char*)"(no error text)";
    myPid=getpid();
#ifdef MPI
        if (MPI_Init(&argc, &argv) != MPI_SUCCESS)
		        BUGOUT("MPI init failure");
	MPI_Comm_size(MPI_COMM_WORLD, &Nranks);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
#endif
    setDefaults();
    parseCommands(argc, argv);  /* Parse command-line options, exit if any are wrong, set variables appropriately */
#ifdef MPI
	if (my_rank)
	    runCreate=0;	 /* If creating data, only create it once! */
	    runSingleUser=0;	/* Single user tests don't work with multiple users! */
#endif /* MPI */
    harvestZombies(0);

    /* Start of database table creation */

    if (runCreate) {
        databaseCreate(DBNAME);
        databaseConnect();
        printf("\n");  /* Print a newline to enhance readability. JC */

        timeIt("create_tables()",create_tables);
        if (runGenerate) {
            timeIt("create_data()",create_data);
        } else {
            timeIt("load()",load);
        }
        if (generateFiles) {    /* If we're only creating data files... */
            databaseDisconnect();
            return(0);
        }
        timeIt("populateDataBase()",populateDataBase);
        databaseDisconnect();
    }

#ifdef MPI
    MPI_Barrier(MPI_COMM_WORLD); /* Get synchronized, as necessary */
#endif /* MPI */
    currentTest=(char*)"Counting tuples";
    databaseConnect();
    if ((tupleCount=countTuples("updates"))==0)
        BUGOUT("empty database -- empty results");
    dbSize=(4*tupleCount*100)/1000000;
        /* dbSize: 4 relations, N tuples/relation, 100 bytes/tuple */
#ifdef MPI
    if (!my_rank)
#endif /* MPI */
    {
	    fprintf(osdblog, "\nLogical database size %0.4fMB (%0.4fMiB, %d tuples/relation)\n\n",
       		(4.0*tupleCount*100)/(1000*1000),
       		(4.0*tupleCount*100)/(1024*1024),
       		tupleCount);
    		FLUSH_LOG;
    }
    databaseDisconnect();

    /* Start of the single user test */

#ifdef MPI
    MPI_Barrier(MPI_COMM_WORLD); /* Why don't we sing this song all together? */
#endif /* MPI */

    if (runSingleUser) {
        currentTest=(char*)"Preparing single user test";
        clocks=times(&tmsStart);
        databaseConnect();
        singleUserTests();
        databaseDisconnect();
        elapsed=elapsedTime(clocks=(times(&tmsEnd)-clocks));
        fprintf(osdblog, "\nSingle User Test\t%.02f seconds\t(%s)\n\n",
                    (double)clocks/ticksPerSecond, elapsed);
        FLUSH_LOG;
    }

    /* Start of the multi-user test */

#ifdef MPI
    MPI_Barrier(MPI_COMM_WORLD); /* Get synchronized, as necessary */
#endif /* MPI */

    if (runMultiUser) {
        currentTest=(char*)"Preparing multi-user test";
        databaseConnect();
        if (tupleCount != countTuples("updates")) {
            BUGOUT("data corrupted; skipping multi-user test");
        }
        databaseDisconnect();
        printf("\nStarting multi-user test\n");
        fflush(stdout);
        clocks=times(&tmsStart);
        multiUserTests((nUsers? nUsers : dbSize/4));
        elapsed=elapsedTime(clocks=(times(&tmsEnd)-clocks));
        fprintf(osdblog, "\nMulti-User Test\t%.02f seconds\t(%s)\n\n",
            (double)clocks/ticksPerSecond, elapsed);
        FLUSH_LOG;
    }

    /* End of the test */
    if (osdblog != stdout)
        fclose(osdblog);
#ifdef MPI
        MPI_Finalize();
#endif /* MPI */

    return 0;
}

long long
makeRandom(double min, double max) {
    double   range;

    range=max-min;
    return min + ((range*rand())/((double)RAND_MAX+1.0));
}

void
multiUserTests(int nInstances) {
    float   fTime;
    int     endOfTime,
            i,
            iters,
            pStatus,
            startOfTime,
            timeToRun;
    pid_t   *kid_pids,
            pid;
    struct  timeval tv;
    struct  tms     t;

  currentTest="Multi-user tests";
  fprintf(osdblog, "Executing multi-user tests with %d user task%s\n",
            nInstances, PLURAL(nInstances, "s", ""));

  /* Step 1 -- start N instances of ir_select */

    fflush(osdblog);
    if ((kid_pids=calloc(nInstances? nInstances : 1, sizeof(pid_t)))==0)
        BUGOUT("calloc() failed");
    for (i=0; i<nInstances; i++) {
        if ((pid=fork())==-1) BUGOUT("multiuser forks");
        if (pid==0) {   /* child */
            iAmParent=0;
            if (osdblog != stdout) {
                fclose(osdblog);
                osdblog=stdout;
            }
            srand(myPid=getpid());  /* ensure children get different rands! */
            databaseConnect();
            dontHangUp=1;
            runUntilHUP(mu_ir_select);
            databaseDisconnect();
            exit(0);
        }
        if (watch) {
            fprintf(stderr, "\n...spawning %d\n", pid);
            fflush(stderr);
        }
        kid_pids[i]=pid;
    }

  /* Step 2 -- let everyone run for 15 minutes */

    for (i=(shortForm? 1:15); i>0; i--) {
        printf("warming up: %d minute%s to go \r",
                i, PLURAL(i, "s", ""));
        fflush(stdout);
        tv.tv_sec = 60;     /* check for zombies every minute */
        tv.tv_usec = 0;
        select(1, NULL, NULL, NULL, &tv);
        while ((pid=waitpid(-1, &pStatus, WNOHANG))) { /* something died */
            if ((pid==-1) || (pid>=kid_pids[0])) {
                printf("\nwaitpid()=%d\n",pid);
                BUGOUT("ir_select workload error");
            }
        }
    }

  /* Step 3 -- run ir_select locally for 5 minutes, measure throughput */

    timeToRun=(shortForm? 1 : 5);
    printf("\nProceeding with 5 minute test\n");
    if (timeToRun != 5)
        printf ("\t(running it for %d minute%s)\n",
                timeToRun, PLURAL(timeToRun, "s", ""));
    databaseConnect();
    iters=0;
    startOfTime=times(&t);
    endOfTime=startOfTime+(timeToRun*60*ticksPerSecond);
    while (times(&t)<endOfTime) {
        mu_ir_select();
        iters++;
    }
    fTime=((double)(times(&t)-startOfTime))/(double)ticksPerSecond;
    fprintf(osdblog, "Mixed IR (tup/sec)\t%.2f"
                    "\treturned in %.2f minutes (%d tuples)\n",
            (double)iters/fTime, fTime/60., iters);

  /* Step 4 -- kill one user instance of ir_select,
               then run & time the "cross section" script */

    kill(kid_pids[0],SIGHUP);
    if (watch)
        printf("killing process %d\n", kid_pids[0]);
    while (waitpid(kid_pids[0], &pStatus, 0)<1);
    crossSectionTests("Mixed IR");

  /* Step 5 -- kill subprocesses, run queries to check integrity */

    for (i=1; i<nInstances; i++) {
        kill(kid_pids[i],SIGHUP);
        if (watch)
            printf("killing process %d\n", kid_pids[i]);
        while (waitpid(kid_pids[i], &pStatus, 0)<1);
    }
#ifdef MPI
    if (my_rank==0)
#endif /* MPI */
    {
	if (!restrict_insert_into) {
            timeIt("mu_checkmod_100_seq()",mu_checkmod_100_seq);
            timeIt("mu_checkmod_100_rand()",mu_checkmod_100_rand);
	}
    }
    databaseDisconnect();

  /* Step 6 -- do some integrity tests */

  /* Step 7 -- start N instances of oltp_update */

    fflush(osdblog);
    for (i=0; i<nInstances; i++) {
        if ((pid=fork())==-1) BUGOUT("multiuser forks");
        if (pid==0) {   /* child */
            iAmParent=0;
            if (osdblog != stdout) {
                fclose(osdblog);
                osdblog=stdout;
            }
            srand(myPid=getpid());
            currentTest=(char*)"mu_oltp_update()";
            databaseConnect();
            dontHangUp=1;
            runUntilHUP(mu_oltp_update);
            databaseDisconnect();
            exit(0);
        }
        kid_pids[i]=pid;
        if (watch) {
            fprintf(stderr, "\n...spawning %d\n", pid);
            fflush(stderr);
        }
        sleep(2);
    }

  /* Step 8 -- let everyone run for 15 minutes */

    for (i=(shortForm? 1:15); i>0; i--) {
        printf("warming up: %d minute%s to go \r",
                i, PLURAL(i, "s", ""));
        fflush(stdout);
        tv.tv_sec = 60;     /* check for zombies every minute */
        tv.tv_usec = 0;
        select(1, NULL, NULL, NULL, &tv);
        while ((pid=waitpid(-1, &pStatus, WNOHANG))) { /* something died */
            if ((pid==-1) || (pid>=kid_pids[0])) {    /* ignore old processes dying */
                printf("\nwaitpid()=%d\n",pid);
                BUGOUT("ir_select(2) workload error");
            }
        }
    }

  /* Step 9 -- run ir_select locally again; measure throughput */

    printf("\nProceeding with 5 minute test\n");
    if (timeToRun != 5)
        printf ("\t(running it for %d minute%s)\n",
                timeToRun, PLURAL(timeToRun, "s", ""));
    databaseConnect();
    iters=0;
    startOfTime=times(&t);
    endOfTime=startOfTime+(timeToRun*60*ticksPerSecond);
    while (times(&t)<endOfTime) {
        mu_ir_select();
        iters++;
    }
    fTime=((double)(times(&t)-startOfTime))/(double)ticksPerSecond;
    fprintf(osdblog, "Mixed OLTP (tup/sec)\t%.2f"
                "\treturned in %.2f minutes (%d tuples)\n",
            (double)iters/fTime, fTime/60., iters);

  /* Step 10 -- kill one user instance of oltp_update,
                then run & time the "cross section" script */

    kill(kid_pids[0],SIGHUP);
    if (watch)
        printf("killing process %d\n", kid_pids[0]);
    while (waitpid(kid_pids[0], &pStatus, 0)<1);
    crossSectionTests("Mixed OLTP");

  /* Step 11 -- run some integrity tests */

#ifdef MPI
    if (my_rank==0)
#endif /* MPI */
    {
	if (!restrict_insert_into) {
            timeIt("mu_checkmod_100_seq()", mu_checkmod_100_seq);
            timeIt("mu_checkmod_100_rand()", mu_checkmod_100_rand);
	}
    }

  /* Step 12 -- clean up and go home */

    databaseDisconnect();
    for (i=1; i<nInstances; i++) {
        kill(kid_pids[i],SIGHUP);
        if (watch)
            printf("killing process %d\n", kid_pids[i]);
        while (waitpid(kid_pids[i], &pStatus, 0)<1);
    }
    free(kid_pids);
}

void
parseCommands(int argc, char* argv[])
{
    char    *fileTest;
    int     i;
    struct  stat statBuf;

    for (i=1; i<argc; i++) {

        if (strcmp(argv[i], "--datadir") == 0) {
            if (++i<argc) dataDir=argv[i];  /* Should be careful of buffer overflows */
        }
        else if (strcmp(argv[i], "--dbhost") == 0) {
            if (++i<argc) DBHOST=argv[i];
        }
        else if (strcmp(argv[i], "--dbpassword") == 0) {
            if (++i<argc) DBPASSWORD=argv[i];
        }
        else if (strcmp(argv[i], "--dbport") == 0) {
            if (++i<argc) DBPORT=atoi(argv[i]);
        }
        else if (strcmp(argv[i], "--dbuser") == 0) {
            if (++i<argc) DBUSER=argv[i];
        }
        else if (strcmp(argv[i], "--generate-files") == 0) {
            generateFiles = TRUE;
            runGenerate = TRUE;
        }
        else if (strcmp(argv[i], "--logfile") == 0) {
            if (++i<argc) {
                if ((osdblog=fopen(argv[i],"w")) == NULL)  /* Should be careful of buffer overflows */
                    BUGOUT("invalid arg to --logfile");
            }
            else
                BUGOUT("arg required for --logfile");
        }
	else if (strcmp(argv[i], "--restrict") == 0) {
	    if (++i<argc) {
		    processRestrictions(argv[i]);
	    }
	    else
		BUGOUT("arg required for --restrict");
	}
        else if (strcmp(argv[i], "--size") == 0) {
            if (++i < argc) {
                long long   acc,
                            j;

                acc=0;
                j=0;
                while ( (argv[i][j]>='0') && (argv[i][j]<='9') ) {
                    acc=(acc*10)+(argv[i][j++]-'0');
                }
                if ( (strcasecmp(&argv[i][j], "KiB")==0)
                  || (strcasecmp(&argv[i][j],"ki")==0)) {
                    acc=acc*1024;
                } else if ( (strcasecmp(&argv[i][j],"KB")==0)
                      ||    (strcasecmp(&argv[i][j],"K")==0)) {
                    acc=acc*1000;
                } else if ( (strcasecmp(&argv[i][j], "MiB")==0)
                  || (strcasecmp(&argv[i][j],"mi")==0)) {
                    acc=acc*(1024*1024);
                } else if ( (strcasecmp(&argv[i][j],"MB")==0)
                      ||    (strcasecmp(&argv[i][j],"M")==0)) {
                    acc=acc*(1000*1000);
                } else if ( (strcasecmp(&argv[i][j], "GiB")==0)
                  || (strcasecmp(&argv[i][j],"gi")==0)) {
                    acc=acc*(1024*1024*1024);
                } else if ( (strcasecmp(&argv[i][j],"GB")==0)
                      ||    (strcasecmp(&argv[i][j],"G")==0)) {
                    acc=acc*(1000*1000*1000);
                } else if (argv[i][j]) {
                    BUGOUT("invalid argument to --size");
                } 
                if ( (acc < 40000) || (acc > 400000000000LL) ) {
                    BUGOUT("size must be between 40KB and 400GB");
                }
                dataSize=acc/400;           /* dbSize: 4 relations, N tuples/relation, 100 bytes/tuple */
                runGenerate = TRUE;        /* Generate the benchmark test data, don't load it from files */
            }
        }
        else if (strcmp(argv[i], "--nocreate") == 0) {
            runCreate = FALSE;
        }
        else if (strcmp(argv[i], "--noindexes") == 0 || strcmp(argv[i], "--noindices") == 0) {
            doIndexes = FALSE;
        }
        else if (strcmp(argv[i], "--nomulti") == 0) {
            runMultiUser = FALSE;
        }
        else if (strcmp(argv[i], "--nosingle") == 0) {
            runSingleUser = FALSE;
        }
        else if (strcmp(argv[i], "--short") == 0) {
            shortForm = TRUE;
        }
        else if (strcmp(argv[i], "--strict_conformance") == 0) {
            strictConformance = TRUE;
        }
        else if ((strcmp(argv[i], "--usage") == 0) ||
                 (strcmp(argv[i], "--help") == 0)) {
            usage();
            exit(0);
        }
        else if (strcmp(argv[i], "--users") == 0) {
            if (++i < argc) nUsers = atoi(argv[i]);  /* Should be careful of buffer overflows.  JC */
        }
        else if (strcmp(argv[i], "--watch") == 0) {
            watch=-1;
        }
        else if (strcmp(argv[i], "--watchall") == 0) {
            watch=-1;
            watchAll=-1;
        }
        else if (argument_check(argv[i])) {
            /* it was a product-specific option, and accepted */
        }
        else {
            fprintf(stderr, "Invalid argument: %s\n", argv[i]);
            usage();
            exit(1);
        }
    }
    signal(SIGHUP,fieldHups);
    signal(SIGINT,fieldHups);

#ifdef MPI
    if (!my_rank)
#endif /* MPI */
    {
	fprintf(osdblog, "OSDB %s\n\nInvoked: %s", VERSION_NAME, argv[0]);
    	for (i=1; i<argc; i++)
        	fprintf(osdblog, " %s", argv[i]);
    	fprintf(osdblog, "\nCompile-time options: ");
    }
    i=0;
#ifdef MPI
    	if (!my_rank)
		fprintf(osdblog, "MPI ");
	i=1;
	if (!my_rank)
#endif /* MPI */
	{
	    if ( i==0 )
		fprintf(osdblog, "none");
	    fprintf(osdblog, "\nRestrictions: ");
	    i=0;
	    if (restrict_insert_into) {
		fprintf(osdblog, "insert_into ");
		i=1;
	    }
	    if (restrict_rollback) {
		fprintf(osdblog, "rollback ");
		i=1;
	    }
	    if (restrict_subqueries) {
		fprintf(osdblog, "subqueries ");
		i=1;
	    }
	    if (restrict_views) {
		fprintf(osdblog, "views ");
		i=1;
	    }
	    if ( i==0 )
		fprintf(osdblog, "none");
	    fprintf(osdblog, "\n");
	    if (stdout != osdblog) {
		fprintf(stdout, "\n\nOSDB %s\n\nInvoked: %s", VERSION_NAME, argv[0]);
		for (i=1; i<argc; i++)
		    fprintf(stdout, " %s", argv[i]);
		fprintf(stdout, "\n\n");
	    }
	}

    if (dataDir == NULL) {
        i = 50;
        dataDir = malloc(i);
        while (getcwd(dataDir,i) == NULL && (errno == ERANGE)) {
            dataDir = realloc(dataDir,i+=50);
        }
        dataDir = realloc(dataDir,i+=5);
        strcat(dataDir, "/data");
    } else if ((dataDir[0] == '.') || (dataDir[0] == '~'))
        BUGOUT("must specify absolute path for --datadir");
    if (runCreate) {
        if (!runGenerate) { 
            if (stat(dataDir, &statBuf)) BUGOUT(dataDir);
            fileTest = malloc(strlen(dataDir)+FILENAME_MAX_LENGTH+2);
            strcpy(fileTest, dataDir);
            strcat(fileTest, "/");
            strcat(fileTest, FILENAME_HUNDRED);
            if (stat(fileTest, &statBuf)) BUGOUT("can't access data file");
            strcpy(fileTest, dataDir);
            strcat(fileTest, "/");
            strcat(fileTest, FILENAME_TENPCT);
            if (stat(fileTest, &statBuf)) BUGOUT("can't access data file");
            strcpy(fileTest, dataDir);
            strcat(fileTest, "/");
            strcat(fileTest, FILENAME_TINY);
            if (stat(fileTest, &statBuf)) BUGOUT("can't access data file");
            strcpy(fileTest, dataDir);
            strcat(fileTest, "/");
            strcat(fileTest, FILENAME_UNIQUES);
            if (stat(fileTest, &statBuf)) BUGOUT("can't access data file");
            strcpy(fileTest, dataDir);
            strcat(fileTest, "/");
            strcat(fileTest, FILENAME_UPDATES);
            if (stat(fileTest, &statBuf)) BUGOUT("can't access data file");
            free(fileTest);
        }
    }
}


int
populateDataBase() {
    timeIt("create_idx_uniques_key_bt()",create_idx_uniques_key_bt);
    timeIt("create_idx_updates_key_bt()",create_idx_updates_key_bt);
    timeIt("create_idx_hundred_key_bt()",create_idx_hundred_key_bt);
    timeIt("create_idx_tenpct_key_bt()",create_idx_tenpct_key_bt);
    timeIt("create_idx_tenpct_key_code_bt()",create_idx_tenpct_key_code_bt);
    timeIt("create_idx_tiny_key_bt()",create_idx_tiny_key_bt);
    timeIt("create_idx_tenpct_int_bt()",create_idx_tenpct_int_bt);
    timeIt("create_idx_tenpct_signed_bt()",create_idx_tenpct_signed_bt);
    timeIt("create_idx_uniques_code_h()",create_idx_uniques_code_h);
    timeIt("create_idx_tenpct_double_bt()",create_idx_tenpct_double_bt);
    timeIt("create_idx_updates_decim_bt()",create_idx_updates_decim_bt);
    timeIt("create_idx_tenpct_float_bt()",create_idx_tenpct_float_bt);
    timeIt("create_idx_updates_int_bt()",create_idx_updates_int_bt);
    timeIt("create_idx_tenpct_decim_bt()",create_idx_tenpct_decim_bt);
    timeIt("create_idx_hundred_code_h()",create_idx_hundred_code_h);
    timeIt("create_idx_tenpct_name_h()",create_idx_tenpct_name_h);
    timeIt("create_idx_updates_code_h()",create_idx_updates_code_h);
    timeIt("create_idx_tenpct_code_h()",create_idx_tenpct_code_h);
    timeIt("create_idx_updates_double_bt()",create_idx_updates_double_bt);
    timeIt("create_idx_hundred_foreign()",create_idx_hundred_foreign);
    return 0;
}


void
processRestrictions(char *t) {
/* Arguments to "--restrict" can come to us in two forms. For 2 restrictions,
 * A and B, the user can specify
 * 	--restrict A --restrict B  <=> We see "A", then "B"
 * 	--restrict A,B             <=> We see "A,B"
 */

#define PR_BUFSIZE 32
	char	buf[PR_BUFSIZE+1];
	char	*eptr,		/* end of token pointer */
		*errPtr,
		*iptr,
		*optr;
	iptr=t;
	while (*iptr) {
		optr=buf;
		errPtr=iptr;
		while (*iptr) {
			if ((*(optr++)=*(iptr++))== ',') {
				--optr;
				break;
			}
			if ((optr-buf)>=PR_BUFSIZE) {
				fprintf(stderr, "Invalid restriction: \"%s\"\n", errPtr);
				BUGOUT("");
			}
		}
		*optr = NULL;

		if (strcmp(buf, "insert_into")==0) {
			restrict_insert_into = 1;
		}
		else if (strcasecmp(buf, "MySQL")==0) {
			restrict_insert_into = 1;
			restrict_rollback = 1;
			restrict_subqueries = 1;
			restrict_views = 1;
		}
		else if (strcmp(buf, "rollback")==0) {
			restrict_rollback = 1;
		}
		else if (strcmp(buf, "subqueries")==0) {
			restrict_subqueries = 1;
		}
		else if (strcmp(buf, "views")==0) {
			restrict_views = 1;
		}
		else {
			fprintf(stderr, "Unknown restriction: \"%s\"\n", buf);
			BUGOUT("error during processRestrictions()");
		}
	}
}


void
runUntilHUP(int (*routine)()) {
    if (signal(SIGHUP,fieldHups)==SIG_ERR)
        BUGOUT("SIG_ERR, runUntilHUP");
    while (dontHangUp) {
        (*routine)();
    }
}


void
singleUserTests() {
    timeIt("sel_1_cl()",sel_1_cl);
    timeIt("join_3_cl()",join_3_cl);
    timeIt("sel_100_ncl()",sel_100_ncl);
    timeIt("table_scan()",table_scan);
    timeIt("agg_func()",agg_func);
    timeIt("agg_scal()",agg_scal);
    timeIt("sel_100_cl()",sel_100_cl);
    timeIt("join_3_ncl()",join_3_ncl);
    timeIt("sel_10pct_ncl()",sel_10pct_ncl);
    if (!restrict_subqueries)
        timeIt("agg_simple_report()",agg_simple_report);
    timeIt("agg_info_retrieval()",agg_info_retrieval);
    if (!restrict_views) {
        timeIt("agg_create_view()",agg_create_view);
        timeIt("agg_subtotal_report()",agg_subtotal_report);
        timeIt("agg_total_report()",agg_total_report);
    }
    timeIt("join_2_cl()",join_2_cl);
    timeIt("join_2()",join_2);
    timeIt("sel_variable_select_low()",sel_variable_select_low);
    timeIt("sel_variable_select_high()",sel_variable_select_high);
    timeIt("join_4_cl()",join_4_cl);
    timeIt("proj_100()",proj_100);
    timeIt("join_4_ncl()",join_4_ncl);
    timeIt("proj_10pct()",proj_10pct);
    timeIt("sel_1_ncl()",sel_1_ncl);
    timeIt("join_2_ncl()",join_2_ncl);
    if (!restrict_rollback)
        timeIt("integrity_test()",integrity_test);
    timeIt("drop_updates_keys()",drop_updates_keys);
    timeIt("bulk_save()",bulk_save);
    timeIt("bulk_modify()",bulk_modify);
    timeIt("upd_append_duplicate()",upd_append_duplicate);
    timeIt("upd_remove_duplicate()",upd_remove_duplicate);
    timeIt("upd_app_t_mid()",upd_app_t_mid);
    timeIt("upd_mod_t_mid()",upd_mod_t_mid);
    timeIt("upd_del_t_mid()",upd_del_t_mid);
    timeIt("upd_app_t_end()",upd_app_t_end);
    timeIt("upd_mod_t_end()",upd_mod_t_end);
    timeIt("upd_del_t_end()",upd_del_t_end);
    timeIt("create_idx_updates_code_h()",create_idx_updates_code_h);
    timeIt("upd_app_t_mid()",upd_app_t_mid);
    timeIt("upd_mod_t_cod()",upd_mod_t_cod);
    timeIt("upd_del_t_mid()",upd_del_t_mid);
    timeIt("create_idx_updates_int_bt()",create_idx_updates_int_bt);
    timeIt("upd_app_t_mid()",upd_app_t_mid);
    timeIt("upd_mod_t_int()",upd_mod_t_int);
    timeIt("upd_del_t_mid()",upd_del_t_mid);
    timeIt("bulk_append()",bulk_append);
    timeIt("bulk_delete()",bulk_delete);
}

void
timeIt(char *rtnName, int(*routine)()) {
    int clocks,
        retval;
    struct tms start, end;

    currentTest=rtnName;
    testFailed=0;
    clocks=times(&start);
    errno=0;
    retval=(*routine)();
    clocks=times(&end)-clocks;
    fprintf(osdblog, "%32s\t", rtnName);  /* Make the test outputs easier to read.  JC */
    if (testFailed)
        fprintf(osdblog, "failed\t(%.02f)\t%d\n",
                (double)clocks/ticksPerSecond, retval);
    else
        fprintf(osdblog, "%.02f seconds\treturn value = %d\n",(double)clocks/ticksPerSecond, retval);  /* Make the test outputs easier to read.  JC */
    FLUSH_LOG;
}

void
usage() { 
    printf("OSDB %s\n\n", VERSION_NAME);
    printf( "\tThis program is distributed in the hope that it will be useful,\n"
            "\tbut WITHOUT ANY WARRANTY; without even the implied warranty of\n"
            "\tMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
            "\tGNU General Public License for more details.\n\n");
    printf("usage: osdb [options]\n"
"\n  Specifying data for the test:\n"
"    [--datadir path]    For initital data, read it from files in <path>.\n"
"                        If --generate-files is specified, write them to\n"
"                        <path> instead.\n"
"    [--generate-files]  Generate data files for future test runs; used\n"
"                        with --size.\n"
"    [--size n]          Generate a database of size 'n', where 'n' may\n"
"                        be post-fixed with 'KB', KiB', 'MB', 'MiB',\n"
"                        'GB', or 'GiB'. If --generate-files is also\n"
"                        specified, create data files that will result\n"
"                        in a database of size 'n'.\n"
"\n  Controlling the tests:\n"
"    [--logfile <file>]  Log output to <file> as well as stdout.\n"
"    [--nocreate]        Skip the database creation phase; database must\n"
"                        already be populated.\n"
"    [--noindexes]       Do not create indexes for key attributes.\n"
"    [--nosingle]        Skip the single user tests.\n"
"    [--nomulti]         Skip the multiuser tests.\n"
"    [--restrict <list>] Skip tests that require the functions named in\n"
"                        <list>.  <list> may contain one or more of the\n"
"                        following values:\n"
"                          insert_into - skip tests that require\n"
"                                        \"insert into table select from\n"
"                                        table\".\n"
"                          rollback    - skip tests that use \"rollback\"\n"
"                          subqueries  - skip tests that use \"select\n"
"                                        from table where key in\n"
"                                        (select from table)\".\n"
"                          views       - skip tests that use views.\n"
"                          MySQL       - macro restriction that includes\n"
"                                        insert_into, rollback, subqueries,\n"
"                                        and views.\n"
"                        For 2 restrictions, say \"A\" and \"B\", you may use:\n"
"                         --restrict A --restrict B\n"
"                         --restrict A,B\n"
"    [--short]           Execute a reduced-time version of the timed tests.\n"
"    [--users n]         Execute multiuser tests with n independent users.\n"
"\n  Database location:\n"
"    [--dbhost <host>]     Access the database on system <host>.\n"
"    [--dbport <port>]     Access the database system on IP port <port> on\n"
"                        the host system.\n"
"\nUser authentication:\n"
"    [--dbuser <name>]     Access the database as user <name>.\n"
"    [--dbpassword <pw>]   Access the database using <pw> as the password.\n"

"\n  Debugging options:\n"
"    [--watch]           Primarily for debugging, watch the database\n"
"                        calls issued.\n"
"    [--watchall]        Purely for debugging, watch everything happening.\n"
"    [--logfile file]    Log results to <file> in addition to <stdout>.\n"
);
    argument_choices();
}
