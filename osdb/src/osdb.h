/***************************************************************************
                  osdb.h  -  description
                     -------------------
    begin        : Fri Dec 1 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb.h,v $
Revision 1.28  2003/05/14 03:48:11  ariebs
Implement --dbuser and --dbpassword.

Revision 1.27  2003/05/07 00:59:25  ariebs
Change the default to a 40MB database (up from 4MB).

Revision 1.26  2003/05/06 23:09:13  ariebs
Report the version number on execution.

Revision 1.25  2003/04/19 01:29:00  ariebs
Default --size 4m, just in case they didn't specify anything.

Revision 1.24  2003/03/22 02:48:52  ariebs
Check in the changes to create data files.

Revision 1.23  2003/03/15 22:23:09  markir
Clean up order of includes for sys/time.h and sys/resource.h in osdb.h.
Additionally remove unneeded includes from the-tests.m4.
Finally time.h is needed as well ans sys/time.h.

Revision 1.22  2003/03/14 04:20:08  ariebs
Reorder sys/time.h and sys/resource.h, per note from Peter Eisentraut.

Revision 1.21  2003/02/17 18:00:36  ariebs
Bunch of changes to use a common source, the-tests.m4, for both "embedded"
and "callable" tests. This version is a bit hacky, and only supports
PostgreSQL (except -ui) and Informix (I hope).

Revision 1.20  2002/12/08 04:13:32  ariebs
Check the value supplied for --size

Revision 1.19  2002/11/12 03:37:51  ariebs
Replace the RAND macro with a routine that correctly handles negative
minimum values

Revision 1.18  2002/10/31 11:42:16  ariebs
This change was suggested to allow us to compile on Max OS X

Revision 1.17  2002/10/03 02:44:23  ariebs
Commit the next chunk of data creation changes (but it doesn't work yet!)

Revision 1.16  2002/09/29 16:58:06  ariebs
Check in the first pass of the data generation code. Note that it is NOT
yet ready to be used, but others can see (and comment on) where it's going.

Revision 1.15  2002/06/03 01:17:02  ariebs
The key fix is resetting dontHangUp=1 before calling runUntilHUP(). Other
changes here reflect minor things I encountered while debugging on Solaris.

Revision 1.14  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.13  2002/02/18 23:09:28  ariebs
Oops -- more forkpty() residue.

Revision 1.12  2002/02/17 13:28:10  ariebs
A variety of changes to remove forkpty() from the code.

Revision 1.11  2002/01/29 00:10:14  ariebs
Apply Peter Eisentraut's portability patches.

Revision 1.10  2001/10/10 02:51:13  ariebs
Implement --strict_conformance option, allowing implementers to skip or work around missing features
(e.g. PostgreSQL prefers not to use HASH indexes) -- the default is FALSE, i.e., allow work arounds

Revision 1.9  2001/09/13 08:05:30  vapour
Replaced the --nogeneratedata option with --generate and --size n.  Improved the output readability.

Revision 1.8  2001/09/12 05:11:12  vapour
Started adding structures for --nogeneratedata & improved output readability

Revision 1.7  2001/04/13 15:46:20  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

Revision 1.6  2001/03/28 00:06:33  ariebs
Reflect the decision to separate program-control.c from osdb.c

Revision 1.5  2001/03/25 12:41:59  ariebs
Add ctype.h for ischar(), isblank(), etc.  (I thought I did this ages
ago -- no wonder I was getting those warnings!)

Revision 1.4  2001/02/23 20:38:27  ariebs
Cosmetic changes to
(a) Use forkpty() (so I added LDFLAGS to all the makefiles while I was
    at it, and
(b) Add "diagnostics()" to osdb.c (which perturbed most programs)

Revision 1.2  2001/02/18 14:30:31  ariebs
Implement programStartPty() for MySQL, and eliminate my hideous patch
for the mysql program. Also renamed "programStart()" to
"programStartPipe()"

*/

#ifndef OSDB_H
#define OSDB_H 1

/* Define values for TRUE and FALSE */
#ifndef TRUE
    #define TRUE  1
    #define FALSE 0
#endif

#define RANKS_normal 99     /* Number of possible values for normal distribution */
#define RANKS_zipfian 10    /* How many possible values in Zipfian distribution? */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "version.h"

struct OSDB_ERROR
{
    enum
    {
        ERR_OK=0,
        ERR_DDL,            /* random DDL error */
        ERR_DML,            /* random DML error */
        ERR_UNKNOWN,        /* really random error */
        ERR_DUPLICATE,      /* too many records */
        ERR_NOTFOUND,       /* not enough records */
        ERR_DEADLOCK,       /* self evident */
        ERR_INVALIDTEST     /* This test would make no sense */
    }       error;
    char    *text;      /* lotsa room for other stuff */
};

typedef struct 
{
    pid_t   pid;        /* child's process id */
    int     toChild,    /* child's STDIN */
            toParent;   /* child's STDOUT and STDERR */
} OSDB_RUN;

/* Globals */

#ifdef osdb_main_c
struct OSDB_ERROR  OSDB_ERROR; 
char   *OSDB_ERROR_NAMES[]=
                {"ERR_OK", "ERR_DDL", "ERR_DML", "ERR_UNKNOWN",
                 "ERR_DUPLICATE", "ERR_NOTFOUND",
                 "ERR_INVALIDTEST"}; 

char    *DBHOST,
        *DBNAME,
        *DBPASSWORD,
        *DBUSER;
int     DBPORT;
char    *currentTest,
        *dataDir;
FILE    *osdblog;
int     failureCount=0,
        iAmParent = TRUE,
        myPid,
        doIndexes = TRUE,     /* Create indices on the tables by default */
        nUsers=0,
        generateFiles = FALSE,/* Create files to be loaded later */
        runCreate = TRUE,     /* Create the database structure by default */
        runGenerate = FALSE,  /* Don't generate the benchmark data by default, load it from files instead */
        runMultiUser = TRUE,  /* Run the multi-user tests by default */
        runSingleUser = TRUE, /* Run the single-user tests by default */
        shortForm = FALSE,    /* Run the warmup transactions by default */
        strictConformance = FALSE, /* Allow "cheating" on the test selection */
        testFailed = FALSE,
        watch = FALSE,        /* Set --watch OFF by default */
        watchAll = FALSE;     /* Set --watchall OFF by default */

long long       dataSize=100000, /* default to 100,000 100 byte records in 4 tables */
                tupleCount=0;

#else
extern  char    *DBHOST,
                *DBNAME,
                *DBPASSWORD,
                *DBUSER,
                *currentTest,
                *dataDir;
extern  FILE    *osdblog;
extern  int     create_data(),
                doIndexes,
                failureCount,
                generateFiles,
                iAmParent,
                testFailed,
                watch,
                watchAll;
extern  int     DBPORT;
extern long long    dataSize,
                    tupleCount;
extern  struct  OSDB_ERROR OSDB_ERROR;
extern  char    *OSDB_ERROR_NAMES[];
#endif

#define BUGOUT(stg) bugout(1, stg, __FILE__, __LINE__)
#define TESTFAILED(stg) bugout(0, stg, __FILE__, __LINE__)

#define FILENAME_HUNDRED    "asap.hundred"
#define FILENAME_TENPCT     "asap.tenpct"
#define FILENAME_TINY       "asap.tiny"
#define FILENAME_UNIQUES    "asap.uniques"
#define FILENAME_UPDATES    "asap.updates"
#define FILENAME_MAX_LENGTH 12

#define FLUSH_LOG { fflush(osdblog); if (osdblog != stdout) fflush(stdout); }

#define MAX_FAILURES 50
#define MAX_RETRIES 50
    /* How many times to try to get a locked tuple */

#define MAX_TUPLE_LENGTH 4096
    /* Use this to ensure that strncat()/strncpy() can do their thing */

#define PGO_BUFSIZE 8192
    /* Largest string returned by programGetOutput */

#define PLURAL(N,yes,no) (N!=1? yes : no)

#define RAND(low,high) makeRandom(low,high)

/* C routine definitions */

int   argument_check(char*);
void  argument_choices();
void  bugout(int, char*, char*, int);
int   countTuples(char*);
int   create_data();
void  crossSectionTests(char*);
void  databaseConnect();
void  databaseCreate(char*);
void  databaseDisconnect();
void  diagnostics();
char* elapsedTime(int);
void  fieldHups(int);
void  harvestZombies(int);
char* left40(char*);
void  locked_pause();
long long  makeRandom(double min, double max);
void  multiUserTests(int);
void  parseCommands(int argc, char* argv[]);
int   populateDataBase();
int   programEnd(OSDB_RUN*, char*);
int   programFeed(OSDB_RUN*, char*);
int   programFeed2(OSDB_RUN*, char*, char*);
int   programFlushOutput(OSDB_RUN*);
char* programGetOutput(OSDB_RUN*);
char* programGetOutputReally(OSDB_RUN*);
OSDB_RUN* programStartPipe(char**);
int   runProg(char**, char**);
void  runUntilHUP(int  (*module)());
void  setDefaults();
void  singleUserTests();
void  timeIt(char*, int (*routine)());

/* the tests */
int   agg_create_view();
int   agg_func();
int   agg_info_retrieval();
int   agg_scal();
int   agg_simple_report();
int   agg_subtotal_report();
int   agg_total_report();
int   backup_updates_table();
int   bulk_append();
int   bulk_delete();
int   bulk_modify();
int   bulk_save();
int   create_idx_hundred_code_h();
int   create_idx_hundred_foreign();
int   create_idx_hundred_key_bt();
int   create_idx_tenpct_code_h();
int   create_idx_tenpct_decim_bt();
int   create_idx_tenpct_double_bt();
int   create_idx_tenpct_float_bt();
int   create_idx_tenpct_int_bt();
int   create_idx_tenpct_key_bt();
int   create_idx_tenpct_key_code_bt();
int   create_idx_tenpct_name_h();
int   create_idx_tenpct_signed_bt();
int   create_idx_tiny_key_bt();
int   create_idx_uniques_code_h();
int   create_idx_uniques_key_bt();
int   create_idx_updates_code_h();
int   create_idx_updates_decim_bt();
int   create_idx_updates_double_bt();
int   create_idx_updates_int_bt();
int   create_idx_updates_key_bt();
int   create_tables();
int   drop_updates_keys();
int   integrity_test();
int   join_2();
int   join_2_cl();
int   join_2_ncl();
int   join_3_cl();
int   join_3_ncl();
int   join_4_cl();
int   join_4_ncl();
int   load();
int   mu_checkmod_100_rand();
int   mu_checkmod_100_seq();
int   mu_ir_select();
int   mu_mod_100_rand();
int   mu_mod_100_seq_abort();
int   mu_oltp_update();
int   mu_sel_100_rand();
int   mu_sel_100_seq();
int   mu_unmod_100_rand();
int   mu_unmod_100_seq();
int   proj_100();
int   proj_10pct();
int   sel_100_cl();
int   sel_100_ncl();
int   sel_10pct_ncl();
int   sel_1_cl();
int   sel_1_ncl();
int   sel_variable_select_high();
int   sel_variable_select_low();
int   table_scan();
int   upd_app_t_end();
int   upd_app_t_mid();
int   upd_append_duplicate();
int   upd_del_t_end();
int   upd_del_t_mid();
int   upd_mod_t_cod();
int   upd_mod_t_end();
int   upd_mod_t_int();
int   upd_mod_t_mid();
int   upd_remove_duplicate();

#endif
