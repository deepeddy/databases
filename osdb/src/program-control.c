/***************************************************************************
                  program-control.c  -  OSDB Driver support
                     -------------------
    begin        : Fri Dec  1 09:23:32 EST 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: program-control.c,v $
Revision 1.3  2002/02/17 13:28:10  ariebs
A variety of changes to remove forkpty() from the code.

Revision 1.2  2001/09/09 06:05:32  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.1  2001/03/28 00:06:33  ariebs
Reflect the decision to separate program-control.c from osdb.c

*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*
 * This module contains the routines that are used to control
 * the execution of utility and user interface programs from
 * OSDB.
 *
 * formatting note
 *
 *    We use the vim commands "ts=4" (tabstop=4) and "et" (expand
 *    tabs to spaces) to try to ensure readable, consistent formatting.
*/

#include "osdb.h"
int programFeedDoIt(OSDB_RUN* pgm, char* cmd);


int
programEnd(OSDB_RUN* pgm, char* lastCmd) {
    int     ret;

    if (lastCmd != NULL)
        programFeed(pgm, lastCmd);
    ret=programFlushOutput(pgm);
    close(pgm->toChild);
    close(pgm->toParent);
    waitpid(pgm->pid, 0, 0);
    free(pgm);
    return ret;
}


int
programFeed(OSDB_RUN* pgm, char* cmd) {
    programFlushOutput(pgm);      /* ready for us? */
    if (watch)
        fprintf(stderr, "\nfeed> %s\n", cmd);
	programFeedDoIt(pgm, cmd);
    return 0;
}


int
programFeed2(OSDB_RUN* pgm, char* cmd, char* cmd2) {
    programFlushOutput(pgm);      /* ready for us? */
    if (watch)
        fprintf(stderr, "\nfeed> %s%s\n", cmd, cmd2);
	programFeedDoIt(pgm, cmd);
	programFeedDoIt(pgm, cmd2);
    return 0;
}

int
programFeedDoIt(OSDB_RUN* pgm, char* cmd) {
    int    len,
           wrote;
    len=strlen(cmd);
    while (len>0) {
        wrote=write(pgm->toChild, cmd, len);
        if (wrote==-1) {
            if (errno == EAGAIN) {
                int     limit=5;
                struct  timeval tv;

                while ((wrote==-1) && (limit-->0)) {
                    tv.tv_sec=1;
                    tv.tv_usec=0;
                    select(0, NULL, NULL, NULL, &tv);
                    wrote=write(pgm->toChild, cmd, len);
                }
                if (wrote==-1)
                    BUGOUT("child input busy");
            } else 
                BUGOUT("writing to child");
        }
        len-=wrote;
        cmd+=wrote;
    }
    return 0;
}


int
programFlushOutput(OSDB_RUN* pgm) {
    char    *buffer;

    while ((buffer=programGetOutput(pgm))) {
        if (watchAll) {
            fprintf(stderr, "\nfl> %s", buffer);
            fflush(stdout);
        }
    }
    return 0;
}

/* programGetOutput(pgm)
 *
 * args:    pgm, which describes the program under control
 * returns: text buffer pointer, if there was program 
 *               output available
 *          NULL, if the program is ready for input
 *
 * note:    We ALWAYS prefer to return program output if
 *          we have the option!
 */
char*
programGetOutput(OSDB_RUN* pgm) {
    char            *buffer;
    fd_set          fdsOut;

    if ((buffer=programGetOutputReally(pgm)))
        return buffer;
    if (waitpid(pgm->pid, 0, WNOHANG)) { /* child gone? */
        return programGetOutputReally(pgm);
    }
    FD_ZERO(&fdsOut);
    FD_SET(pgm->toChild, &fdsOut);
    select(pgm->toChild+1, NULL, &fdsOut, NULL, NULL);
        /* wait until child is ready for input */
    return programGetOutputReally(pgm);
        /* tell the caller we've got nothing to say,
           unless there's something to say */
}

char*
programGetOutputReally(OSDB_RUN* pgm) {
    static char     buffer[PGO_BUFSIZE+1];
    int             bytes;
    struct timeval  tv;

    tv.tv_sec=0;
    tv.tv_usec=50000;
    select(0, NULL, NULL, NULL, &tv);
         /* Pause briefly to give the child a chance to speak */
    if ((bytes=read(pgm->toParent, buffer, PGO_BUFSIZE))>0) {
            /* Anything in the output buffer? */
        buffer[bytes]=0;
        return buffer;
    }
    return NULL;
}

OSDB_RUN*
programStartPipe(char* cmdLineArgs[]) {
    int     i,
            pid,
            toChild[2],
            toParent[2];
    OSDB_RUN *pgm;

    pipe(toChild);
    pipe(toParent);
    if ((pid=fork())==-1) BUGOUT("fork");

    if (pid==0) {   /* if child */
        close(toParent[0]);
        close(toChild[1]);
        if (dup2(toParent[1], STDOUT_FILENO)<0) BUGOUT("child stdout");
        if (dup2(toParent[1], STDERR_FILENO)<0) BUGOUT("child stdout");
        if (dup2(toChild[0], STDIN_FILENO)<0) BUGOUT("child stdin");
        execvp(cmdLineArgs[0], cmdLineArgs);
        BUGOUT("execvp()");
    }

    close(toChild[0]);
    close(toParent[1]);
    pgm=calloc(1, sizeof(OSDB_RUN));
    pgm->pid=pid;
    pgm->toChild=toChild[1];
    pgm->toParent=toParent[0];
    if (watch) {
        printf("prog> ");
        for (i=0; cmdLineArgs[i]!=NULL; i++) {
            printf(" %s", cmdLineArgs[i]);
        }
        printf("\n");
    }
    fcntl(toParent[0],F_SETFL, O_NONBLOCK);
    programFlushOutput(pgm); /* solicit any startup messages */
    return pgm;
}


int
runProg(char* cmdLineArgs[], char* inputs[]) {
    char        *buf;
    int         i;
    OSDB_RUN    *pgm;

    pgm=(OSDB_RUN*)programStartPipe(cmdLineArgs);
    i=0;
    if (inputs[0]!=NULL) {
        for (i=0; inputs[i+1]!=NULL; i++) {
            programFeed(pgm, inputs[i]);
            buf=programGetOutput(pgm);
            if (watch) {
                fprintf(stderr, "%s", buf);
                fflush(stderr);
            }
        }
    }
    return programEnd(pgm, inputs[i]);
}
