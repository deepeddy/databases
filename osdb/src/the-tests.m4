/***************************************************************************
                  the-tests.m4 -- the source code of the OSDB tests
                     -------------------
    begin        : Fri Jan 17 2003
    copyright    : (C) 2003 by Andy Riebs
    email        : ariebs@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: the-tests.m4,v $
Revision 1.15  2004/10/17 13:39:25  ariebs
Reimplement restrictions as run-time option, rather than as a compile-time
option.

Revision 1.14  2004/03/07 22:19:10  ariebs
Per Anna's suggestion, simply remove the (non-standard) LIMIT clause
during data generation.

Revision 1.13  2003/05/04 18:14:35  ariebs
More changes to incorporate the-tests.m4 across the board.

Revision 1.12  2003/04/22 10:19:00  ariebs
Add DROP_INDEX macro to accomodate differing syntax between databases.
(Note that SQL92 appears not to have defined standard language for this.)

Revision 1.11  2003/04/09 02:59:43  ariebs
Getting closer to working with both PostgreSQL and MySQL.

Revision 1.10  2003/03/24 03:46:35  ariebs
Use a temp file in create_data to avoid mixing FETCH from a cursor
with INSERTs, which are currently a problem for MySQL (and others?)

Revision 1.9  2003/03/22 02:48:52  ariebs
Check in the changes to create data files.

Revision 1.8  2003/03/15 22:23:09  markir
Clean up order of includes for sys/time.h and sys/resource.h in osdb.h.
Additionally remove unneeded includes from the-tests.m4.
Finally time.h is needed as well ans sys/time.h.

Revision 1.7  2003/03/12 03:43:20  ariebs
Oops! Put the error check back in the loop.

Revision 1.6  2003/03/05 03:43:31  ariebs
Fix an error check that had migrated out of its loop.

Revision 1.5  2003/03/04 03:29:44  ariebs
Change field names that overlap the SQL keyword name space, i.e., change
field "random_tenpct.double" to "random_tenpct.col_double".

Revision 1.4  2003/02/17 18:00:37  ariebs
Bunch of changes to use a common source, the-tests.m4, for both "embedded"
and "callable" tests. This version is a bit hacky, and only supports
PostgreSQL (except -ui) and Informix (I hope).

*/

#include "osdb.h"

/* m4 pre-processing
 *
 *   To be able to use this code for multiple implementations of 
 *   SQL, some pre-processing is required. ESQL processors
 *   tend to assume that the C pre-processor will follow them, and it 
 *   can get ugly if that's not the case.
 *
 *   m4 introduces a minor level of additional complexity, but with
 *   the advantage that it won't break E-SQL or cpp. (Use Gnu m4's
 *   --prefix-builtins to ensure no conflicts.)
 *
 *   This file is m4_include'd from some other file, further down
 *   the tree.
 *
 *   To process this file, use something like
 *
 *     m4 --prefix-builtins >file.c file.m4 
 *     gcc -o file file.c
 *
 * formatting note
 *
 *    We use the vim commands "ts=4" (tabstop=4) and "et" (expand
 *    tabs to spaces) to try to ensure readable, consistent formatting. 
*/

/* M4-based definitions */

m4_changequote([,])
m4_changequote(`,~) /* SQL uses apostrophe, so we'll use tilde */

/* The WARNM4 and BUGOUTM4 macros generate compile-time messages
 * during m4 processing. WARNM4 simply prints the message;
 * BUGOUTM4 terminates the processing as well.
*/

m4_define(`WARNM4~, `m4_errprint($* (m4___file__:m4___line__))~)
m4_define(`BUGOUTM4~, `m4_errprint($@ (m4___file__:m4___line__)) m4_m4exit(1)~)

m4_define(`WRITELOG~,
            `{  if (osdblog != stdout)
                    printf($1, $2);
                fprintf(osdblog, $1, $2);
             }~)

/* Using "SQL_MODE" SQL mode */ 

m4_ifelse(SQL_MODE, embedded, ,
          SQL_MODE, callable, ,
          `BUGOUTM4(`Bogus value for SQL MODE, "SQL_MODE"~)~);

m4_define(`CREATE_TABLE_STANDARD~,
    `CREATE_TABLE(`$1~,
       `INT4(       col_key,    `not null~),        m4_dnl
        INT4(       col_int,    `not null~),        m4_dnl
        INT4(       col_signed            ),        m4_dnl
        REAL4(      col_float,  `not null~),        m4_dnl
        DOUBLE8(    col_double, `not null~),        m4_dnl
        NUMERIC18_2(col_decim,  `not null~),        m4_dnl
        DATETIME8(  col_date,   `not null~),        m4_dnl
        CHAR10(     col_code,   `not null~),        m4_dnl
        CHAR20(     col_name,   `not null~),        m4_dnl
        VARCHAR80(  col_address,`not null~)~)~)

/* Protect a transaction from lock troubles with LOCK_PROTECT */

m4_define(`LOCK_PROTECT_S~, LOCK_PROTECT(`$1~,`s~))
m4_define(`LOCK_PROTECT~, `
            {
                int counter, tryAgain;

                counter=MAX_RETRIES;
                tryAgain=1;
                while (tryAgain && ((counter--)>0))
                {
                    EXPECT_SUCCESS
                    tryAgain=0;
                    m4_ifelse($2, s,
                        DML_S(`$1~),
                        DML(`$1~));
                    EXPECT_ANY(`
                            if (watch)
                            {
                                printf("tuple locked; retrying (%d:%d)\n",
                                    myPid, counter);
                            }
                            if (counter)
                            {
                                EXPECT_SUCCESS
                                locked_pause();
                            }
                    ~)
                }
                if (tryAgain)
                    BUGOUT("lock problem");

                EXPECT_SUCCESS
            }
~)



/* C pre-processor definitions */

#define HUNDREDMILLION 10*10*10*10*10*10*10*10
#define THOUSANDMILLION HUNDREDMILLION*10


int NO_HASH_INDEX=FALSE;

int myPid;

int
agg_create_view()
{
    TRANSACTION_BEGIN
    DDL(`create view                                                m4_dnl
                reportview(col_key,col_signed,col_date,col_decim,   m4_dnl
                           col_name,col_code,col_int) as            m4_dnl
                    select updates.col_key, updates.col_signed,     m4_dnl
                           updates.col_date, updates.col_decim,     m4_dnl
                           hundred.col_name, hundred.col_code,      m4_dnl
                           hundred.col_int                          m4_dnl
                      from updates, hundred                         m4_dnl
                      where updates.col_key = hundred.col_key~);
    EXPECT_SUCCESS(`return -1;~)
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
agg_func()
{
    DECLARE_BEGIN
        char    col_key[21];
    DECLARE_END
    int         count=0;

    CURSOR_DECLARE(` select min(col_key) from hundred group by col_name~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~, `col_key~, `%s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("agg_func says: %s\n", col_key);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
agg_info_retrieval()
{
    DECLARE_BEGIN
        char    count[21];
    DECLARE_END

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
           ` select count(col_key)                                      m4_dnl
             from tenpct                                                m4_dnl
             where col_name = 'THE+ASAP+BENCHMARKS+'                    m4_dnl
               and col_int <= 100000000                                 m4_dnl
               and col_signed between 1 and 99999999                    m4_dnl
               and not (col_float between -450000000 and 450000000)     m4_dnl
               and col_double > 600000000                               m4_dnl
               and col_decim < -600000000                               m4_dnl
           ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~, `count~, `%s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        if (watchAll)
            printf("agg_info_retrieval says: %s\n", count);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT 
    EXPECT_SUCCESS
    return atoi(count);
}


int
agg_scal()
{
    DECLARE_BEGIN
        char    col_key[20];
    DECLARE_END

    TRANSACTION_BEGIN
    CURSOR_DECLARE(`select min(col_key) from uniques~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~, `col_key~, `%s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        if (watchAll)
            printf("agg_scal says: %s\n", col_key);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return atoi(col_key);
}


int
agg_simple_report()
{
    DECLARE_BEGIN
        char    avg[21];
    DECLARE_END
    TRANSACTION_BEGIN
    CURSOR_DECLARE(
           `select avg(updates.col_decim)                       m4_dnl
             from updates                                       m4_dnl
             where updates.col_key in                           m4_dnl
                   (select updates.col_key                      m4_dnl
                    from updates, hundred                       m4_dnl
                    where hundred.col_key = updates.col_key     m4_dnl
                      and updates.col_decim > 980000000)~)
    EXPECT_SUCCESS(`return 0;~)
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~, `avg~, `%s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        if (watchAll)
            printf("agg_simple_report says: %s\n", avg);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return atoi(avg);
}


int
agg_subtotal_report()
{
    DECLARE_BEGIN
        char    avg[21],
                min[21],
                max_signed[21],
                max_date[21],
                min_date[21],
                cd_name[21],
                c_name[21],
                code[21],
                r_int[21];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
           `select avg(col_signed), min(col_signed), max(col_signed),   m4_dnl
                    max(col_date), min(col_date),                       m4_dnl
                    count(distinct col_name), count(col_name),          m4_dnl
                    col_code, col_int                                   m4_dnl
             from reportview                                            m4_dnl
             where col_decim >980000000                                 m4_dnl
             group by col_code, col_int~)
    EXPECT_SUCCESS(`return 0;~)
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
            `avg, min, max_signed, max_date, min_date, cd_name, c_name, code, r_int~,
            `%s   %s   %s          %s        %s        %s       %s      %s    %s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("agg_subtotal_report says:"
                        "%s %s %s %s %s %s %s %s %s\n",
                avg, min, max_signed, max_date, min_date,
                cd_name, c_name, code, r_int);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return count;
}


int
agg_total_report()
{
    DECLARE_BEGIN
        char    avg[21],
                min[21],
                max_signed[21],
                max_date[21],
                min_date[21],
                cd_name[21],
                c_name[21],
                code[21],
                r_int[21];
    DECLARE_END

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
           `select avg(col_signed), min(col_signed), max(col_signed),   m4_dnl
                    max(col_date), min(col_date),                       m4_dnl
                    count(distinct col_name), count(col_name),          m4_dnl
                    count(col_code), count(col_int)                     m4_dnl
             from reportview                                            m4_dnl
             where col_decim >980000000~)
    EXPECT_SUCCESS(`return 0;~)
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
            `avg, min, max_signed, max_date, min_date, cd_name, c_name, code, r_int~,
            `%s   %s   %s          %s        %s        %s       %s      %s    %s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        if (watch)
            printf("agg_total_report says:"
                        "%s %s %s %s %s %s %s %s %s\n",
                avg, min, max_signed, max_date, min_date,
                cd_name, c_name, code, r_int);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return atoi(avg);
}


int
bulk_append()
{
    TRANSACTION_BEGIN
    DML(` insert into updates select * from saveupdates~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
bulk_delete()
{
    TRANSACTION_BEGIN
    DML(` delete from updates where col_key < 0~)
    EXPECT_SUCCESS 
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
bulk_modify()
{
    TRANSACTION_BEGIN
    DML(` update updates                            m4_dnl
             set col_key = col_key - 100000         m4_dnl
             where col_key between 5000 and 5999~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
bulk_save()
{
    TRANSACTION_BEGIN
        CREATE_TABLE_STANDARD(`saveupdates~)
        DML(` insert into saveupdates                   m4_dnl
                select * from updates                   m4_dnl
                where col_key between 5000 and 5999~)
        EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
countTuples(char *relation)
{
    DECLARE_BEGIN
            int Ntuples;
    DECLARE_END

    if (strcmp(relation, "updates"))
    {
        BUGOUT("call to countTuples handles only updates now");
    }
    TRANSACTION_BEGIN
    
    CURSOR_DECLARE(` select count(col_key) from updates~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    FETCH(`CURRENT_CURSOR~, `Ntuples~, `%d~)
    EXPECT_SUCCESS
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return Ntuples;
} 


int
create_data() 
{
    DECLARE_BEGIN
        char    col_address[81],
                col_code[11],
                col_name[21],
                date_string[32],
                hundred_address[81],
                hundred_name[81],
                hundred_unique_address[100][81],
                hundred_unique_code[100][11],
                hundred_unique_name[100][21],
                name[21];
        float   col_float,
                hundred_float,
                hundred_unique_float[100],
                uniform100_float,
                zipf10[10],
                zipf10_float,
                zipf100[100],
                zipf100_float;
        long    col_key,
                col_signed,
                date_random,
                dense_key,
                hundred_key,
                i,
                randomizer,
                rec,
                r10pct_key,
                sparse_key,
                sparse_signed,
                sparse_key_spread,
                sparse_signed_spread,
                tenpct,
                tenpct_key,
                uniform100_dense;
        double  col_decimal,
                col_double,
                double_normal,
                hundred_double,
                hundred_unique_double[100];
    DECLARE_END

    char        csv_safe_chars[]="#%&()[]{};:/~@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-=";
                    /* These characters can be used without hassle in comma-separated-value files */
    int         Ncsv_safe_chars,
                Nchars,
                Nlen;
    struct  tm tm;
    FILE        *fHundred,
                *fTemp,
                *fTenpct,
                *fTiny,
                *fUniques,
                *fUpdates;


    if (generateFiles) {
        char    nameBuf[512];

        fprintf(stderr, "\nCreating data files:\n");
        sprintf(nameBuf, "%s/%s", dataDir, FILENAME_HUNDRED);
        if ((fHundred=fopen(nameBuf, "w"))==NULL)
            BUGOUT(nameBuf);
        fprintf(stderr, "\t\t%s\n", nameBuf);
        sprintf(nameBuf, "%s/%s", dataDir, FILENAME_TENPCT);
        if ((fTenpct=fopen(nameBuf, "w"))==NULL)
            BUGOUT(nameBuf);
        fprintf(stderr, "\t\t%s\n", nameBuf);
        sprintf(nameBuf, "%s/%s", dataDir, FILENAME_TINY);
        if ((fTiny=fopen(nameBuf, "w"))==NULL)
            BUGOUT(nameBuf);
        fprintf(stderr, "\t\t%s\n", nameBuf);
        sprintf(nameBuf, "%s/%s", dataDir, FILENAME_UNIQUES);
        if ((fUniques=fopen(nameBuf, "w"))==NULL)
            BUGOUT(nameBuf);
        fprintf(stderr, "\t\t%s\n", nameBuf);
        sprintf(nameBuf, "%s/%s", dataDir, FILENAME_UPDATES);
        if ((fUpdates=fopen(nameBuf, "w"))==NULL)
            BUGOUT(nameBuf);
        fprintf(stderr, "\t\t%s\n", nameBuf);
    }
    if (watchAll) printf("Creating data set...\n");
    Ncsv_safe_chars=strlen(csv_safe_chars);
    /* For our Zipfian distributions, we'll generate values that occur
       most often at Zipf[0], and decay across an asymptotic curve to
       the value at zipf[RANKS_zipfian-1].  (If someone has a better
       algorithm for generating better distributions, please submit it!)
    */
    for (i=0; i<10; i++)
    {
        zipf10[i]=(float)RAND(-5*(HUNDREDMILLION),5*(HUNDREDMILLION));
    }
    for (i=0; i<100; i++)
    {
        zipf100[i]=(float)RAND(-5*(HUNDREDMILLION),5*(HUNDREDMILLION));
    }

    tenpct=dataSize/10;
    if ((sparse_key_spread=(THOUSANDMILLION)/dataSize)<1) sparse_key_spread=1;
    if ((sparse_signed_spread=(10*(HUNDREDMILLION))/dataSize)<1) sparse_signed_spread=1;
    

    CREATE_TABLE(`random_data~,
       `INT4(randomizer, `not null~),               m4_dnl
        INT4(sparse_key, `not null~),               m4_dnl
        INT4(dense_key, `not null~),                m4_dnl
        INT4(sparse_signed, `not null~),            m4_dnl
        INT4(uniform100_dense, `not null~),         m4_dnl
        REAL4(zipf10_float,   `not null~),          m4_dnl
        REAL4(zipf100_float,   `not null~),         m4_dnl
        REAL4(uniform100_float,   `not null~),      m4_dnl
        DOUBLE8(double_normal, `not null~),         m4_dnl
        INT4(r10pct_key, `not null~),               m4_dnl
        CHAR20(col_date, `not null~),               m4_dnl
        CHAR10(col_code, `not null~),               m4_dnl
        CHAR20(col_name, `not null~),               m4_dnl
        VARCHAR80(col_address, `not null~)          m4_dnl
       ~)
    EXPECT_SUCCESS

    CREATE_TABLE(`random_tenpct~,
        `INT4(col_key, `not null~),                 m4_dnl
         INT4(col_float, `not null~),               m4_dnl
         INT4(col_signed, `not null~),              m4_dnl
         DOUBLE8(col_double, `not null~),           m4_dnl
         VARCHAR80(col_address, `not null~)         m4_dnl
        ~)
    EXPECT_SUCCESS

    TRANSACTION_BEGIN
    EXPECT_SUCCESS
    for (rec=1; rec<=dataSize; rec++)
    {
        int     Dlen,
                Drec;

        randomizer = RAND(0,THOUSANDMILLION);
        dense_key  = (rec==1)?0:rec;       
        sparse_key = dense_key*sparse_key_spread;
        sparse_signed = (-5*(HUNDREDMILLION))+((dense_key)*sparse_signed_spread);
        uniform100_dense = 100+(rec%100);
        zipf10_float = zipf10[RAND(0,(rec%10))];
        zipf100_float = zipf100[RAND(0,(rec%100))];
        uniform100_float = 100+(float)((rec%100));
        double_normal = (double)RAND(-(THOUSANDMILLION),(THOUSANDMILLION));
        /* To ensure uniqueness, we'll start by generating the record number
         * in base (Ncsv_safe_chars), followed by "_". We'll then fill out
         * the field with additional randomly selected characters. (By writing
         * the digits backwards, we should help to keep the data disorderly :)
        */
        Dlen=0;
        Drec=rec;
        while (Drec > 0) {
            col_code[Dlen++]=csv_safe_chars[Drec%Ncsv_safe_chars];
            Drec/=Ncsv_safe_chars;
        }
        col_code[Dlen++]='_';
        for (i=Dlen; i < 10; i++)
        {
            col_code[i]=csv_safe_chars[RAND(0, Ncsv_safe_chars)];
        }
        col_code[10]=0;
        strncpy(col_name, col_code, Dlen);
        for (i=Dlen; i < 20; i++)
        {
            col_name[i]=csv_safe_chars[RAND(0, Ncsv_safe_chars)];
        } 
        col_name[20]=0;
        strncpy(col_address, col_code, Dlen);
        Nlen=RAND(2,6+(25*(rec&3)));
        for (i=Dlen; i < Nlen; i++)
        {
            col_address[i]=csv_safe_chars[RAND(0, Ncsv_safe_chars)];
        }
        col_address[(Dlen>Nlen? Dlen : Nlen)]=0;
        if (++r10pct_key>tenpct)
        {
            r10pct_key = 0;
        } else if (r10pct_key==1) {
            r10pct_key++;
        } 
        tm.tm_sec = 0;
        tm.tm_min = 0;
        tm.tm_hour = 0;
        tm.tm_mday = 0;
        tm.tm_mon = 0;
        tm.tm_yday = 0;
        tm.tm_wday = 0;
        tm.tm_isdst = 0;
        date_random = dense_key%36835;       /* roughly 36,835 days from 1/1/1900-12/1/2000 */
        tm.tm_year = (int)(date_random/365); /* ignore leap year considerations */
        date_random = (date_random%365)+1;
        if ( date_random <=31 ) {
            tm.tm_mon=0;
            tm.tm_mday=date_random;
        } else if ( (date_random-=31) <=28 ) {
            tm.tm_mon=1;
            tm.tm_mday=date_random;
        } else if ( (date_random-=28) <=31 ) {
            tm.tm_mon=2;
            tm.tm_mday=date_random;
        } else if ( (date_random-=31) <=30 ) {
            tm.tm_mon=3;
            tm.tm_mday=date_random;
        } else if ( (date_random-=30) <=31 ) {
            tm.tm_mon=4;
            tm.tm_mday=date_random;
        } else if ( (date_random-=31) <=30 ) {
            tm.tm_mon=5;
            tm.tm_mday=date_random;
        } else if ( (date_random-=30) <=31 ) {
            tm.tm_mon=6;
            tm.tm_mday=date_random;
        } else if ( (date_random-=31) <=31 ) {
            tm.tm_mon=7;
            tm.tm_mday=date_random;
        } else if ( (date_random-=31) <=30 ) {
            tm.tm_mon=8;
            tm.tm_mday=date_random;
        } else if ( (date_random-=30) <=31 ) {
            tm.tm_mon=9;
            tm.tm_mday=date_random;
        } else if ( (date_random-=31) <=30 ) {
            tm.tm_mon=10;
            tm.tm_mday=date_random;
        } else {
            tm.tm_mon=11;
            tm.tm_mday=date_random-30;
        } 
        if (strftime(date_string, sizeof date_string, "%Y-%m-%d", &tm) == 0)
        {
            BUGOUT("random date error");
        }
        DML_INSERT_VARIABLE_VALUES(`random_data~,
               `randomizer, sparse_key, dense_key, sparse_signed,       m4_dnl
                uniform100_dense, zipf10_float, zipf100_float,          m4_dnl
                uniform100_float, double_normal, r10pct_key,            m4_dnl
                col_date, col_code, col_name, col_address~,
               `VARIABLES_LIST(
                randomizer %ld, sparse_key %ld, dense_key %ld, sparse_signed %ld,  m4_dnl
                uniform100_dense %ld, zipf10_float %f , zipf100_float %f,          m4_dnl
                uniform100_float %f, double_normal %lf, r10pct_key %ld,            m4_dnl
                date_string %s, col_code %s, col_name %s, col_address %s)~)
        EXPECT_SUCCESS
    } 
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    TRANSACTION_BEGIN
    EXPECT_SUCCESS
    m4_ifelse(SQL_MODE, `embedded~,
        `exec sql update random_data
                    set col_address='SILICON VALLEY'
                    where randomizer=:randomizer;~,
        `sprintf(queryString,
            "update random_data set col_address='SILICON VALLEY' where randomizer=%ld",
            randomizer);
         dml(queryString); ~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS

    /* Now generate a table with 10% of some of the fields */
    TRANSACTION_BEGIN
    EXPECT_SUCCESS
    if ((fTemp=tmpfile())==NULL) {
        BUGOUT("10%% temp file");
    }
    m4_ifelse(SQL_MODE, `embedded~,
        `CURSOR_DECLARE(
            `SELECT sparse_signed, double_normal, col_address   m4_dnl
                FROM random_data                                m4_dnl
                ORDER BY randomizer ~)~,
        `sprintf(queryString,
            "SELECT sparse_signed, double_normal, col_address   m4_dnl
                    FROM random_data ORDER BY randomizer");
         cursorDeclare(queryString); ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS

    for (rec=1; rec<=tenpct; rec++)
    {
        col_key=(rec==1)? 0 : rec;
        FETCH(`CURRENT_CURSOR~,
              `col_signed, col_double, col_address~,
              `%ld         %lf         %s~)
        EXPECT_SUCCESS
        col_float=col_double/2.0;
        fprintf(fTemp, "%ld,%ld,%f,%lf,%s\n",
            col_key, col_signed, col_float, col_double, col_address);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    if (fseek(fTemp, 0, SEEK_SET)) {
        BUGOUT("fTemp I/O error");
    }
    for (rec=1; rec<=tenpct; rec++)
    {
        fscanf(fTemp, "%d,%ld,%f,%lf,%s\n",
            &col_key, &col_signed, &col_float, &col_double, &col_address);
        DML_INSERT_VARIABLE_VALUES(`random_tenpct~,
                `col_key, col_signed, col_float, col_double, col_address~,
                `VARIABLES_LIST(col_key %ld, col_signed %ld, col_float %f, 
                        col_double %lf, col_address %s)~)
        EXPECT_SUCCESS
    }
    DDL(`create index random10_ix on random_tenpct(col_key)~)
    EXPECT_SUCCESS
    
    /* Now generate a table with only 100 tuples of interesting data */
    CURSOR_DECLARE( 
       `SELECT uniform100_float, double_normal, col_name, col_address   m4_dnl
        FROM random_data                                                m4_dnl
        ORDER BY randomizer~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS

    for (i=0; i<100; i++)
    {
        FETCH(`CURRENT_CURSOR~,
              `col_float, col_double, col_name, col_address~,
              `%f         %lf         %s        %s~)
        EXPECT_SUCCESS
        hundred_unique_float[i]=col_double/2;
        hundred_unique_double[i]=col_double;
        strncpy(hundred_unique_name[i], col_name, 20);
        hundred_unique_name[i][20]=0;
        strncpy(hundred_unique_address[i], col_address, 80);
        hundred_unique_address[i][80]=0;
    }
    i=RAND(0,10);
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    col_double=hundred_unique_double[i];
    m4_ifelse(SQL_MODE, `embedded~,
        `exec sql update random_data set col_code='BENCHMARKS', col_name='THE+ASAP+BENCHMARKS+'
            where double_normal=:double_normal;~,
        `sprintf(queryString,
            "update random_data set col_code='BENCHMARKS', col_name='THE+ASAP+BENCHMARKS+' m4_dnl
            where double_normal=%lf", double_normal);
         dml(queryString); ~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    strcpy(hundred_unique_name[i], "THE+ASAP+BENCHMARKS+");
    
    /* Now generate our testing tables */
    if (watchAll) {
        if (generateFiles) {
            printf("Generating the data files...\n");
        } else {
            printf("Generating the testing tables...\n");
        }
    }
    hundred_key=0;
    tenpct_key=0;
    TRANSACTION_BEGIN
    EXPECT_SUCCESS
    CURSOR_DECLARE( 
       `SELECT randomizer, sparse_key, dense_key, sparse_signed,            m4_dnl
                uniform100_dense, zipf10_float, zipf100_float,              m4_dnl
                uniform100_float, double_normal,                            m4_dnl
                col_date, col_code, col_name, col_address                   m4_dnl
        FROM random_data                                                    m4_dnl
        ORDER BY randomizer~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              ` randomizer, sparse_key, dense_key, sparse_signed,       m4_dnl
                uniform100_dense, zipf10_float, zipf100_float,          m4_dnl
                uniform100_float, double_normal,                        m4_dnl
                date_string, col_code, col_name, col_address~,
              ` %ld         %ld         %ld        %ld                  m4_dnl
                %ld               %f            %f                      m4_dnl
                %f                %lf                                   m4_dnl
                %s           %s        %s        %s                 ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        if (generateFiles) {
            fprintf(fUniques, "%ld,%ld,%ld,%f,%lf,%lf,%s,%s,%s,%s\n",
                sparse_key, sparse_key, sparse_signed,
                zipf100_float, double_normal, double_normal,
                date_string, col_code, col_name, col_address);
            fprintf(fUpdates, "%ld,%ld,%ld,%f,%lf,%lf,%s,%s,%s,%s\n",
                dense_key, dense_key, sparse_signed,
                zipf10_float, double_normal, double_normal,
                date_string, col_code, col_name, col_address);
        } else {
            DML_INSERT_VARIABLE_VALUES(`uniques~,
                   `col_key, col_int, col_signed,                           m4_dnl
                    col_float, col_double, col_decim,                       m4_dnl
                    col_date, col_code, col_name, col_address~,
                   `VARIABLES_LIST(
                     sparse_key %ld, sparse_key %ld, sparse_signed %ld,       m4_dnl
                     zipf100_float %f, double_normal %lf, double_normal %lf), m4_dnl
                    SQL_DATE(date_string) -,                                  m4_dnl
                    VARIABLES_LIST(col_code %s, col_name %s, col_address %s)~)
            EXPECT_SUCCESS
            DML_INSERT_VARIABLE_VALUES(`updates~,
                   `col_key, col_int, col_signed,                           m4_dnl
                    col_float, col_double, col_decim,                       m4_dnl
                    col_date, col_code, col_name, col_address~,
                   `VARIABLES_LIST(
                     dense_key %ld, dense_key %ld, sparse_signed %ld,         m4_dnl
                     zipf10_float %f, double_normal %lf, double_normal %lf),  m4_dnl
                    SQL_DATE(date_string) -,                                  m4_dnl
                    VARIABLES_LIST(col_code %s, col_name %s, col_address %s)~)
            EXPECT_SUCCESS
        }
        if (++hundred_key>=100) hundred_key=0;
        hundred_float=hundred_unique_float[hundred_key];
        hundred_double=hundred_unique_double[hundred_key];
        strncpy(hundred_name, hundred_unique_name[hundred_key], 20);
        hundred_name[20]=0;
        strncpy(hundred_address, hundred_unique_address[hundred_key], 80);
        hundred_address[80]=0;
        if (generateFiles) {
            fprintf(fHundred, "%ld,%ld,%ld,%f,%lf,%lf,%s,%s,%s,%s\n",
                dense_key, sparse_key, uniform100_dense,
                hundred_float, hundred_double, hundred_double,
                date_string, col_code, hundred_name, hundred_address);
        } else {
            DML_INSERT_VARIABLE_VALUES(`hundred~,
                   `col_key, col_int, col_signed,                           m4_dnl
                    col_float, col_double, col_decim,                       m4_dnl
                    col_date, col_code, col_name, col_address~,             m4_dnl
                   `VARIABLES_LIST(
                     dense_key %ld, sparse_key %ld, uniform100_dense %ld,       m4_dnl
                     hundred_float %f, hundred_double %lf, hundred_double %lf), m4_dnl
                    SQL_DATE(date_string) -,                                    m4_dnl
                    VARIABLES_LIST(col_code %s, hundred_name %s, hundred_address %s)~)
            EXPECT_SUCCESS
        }
    }
    CURSOR_CLOSE(CURRENT_CURSOR)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS

    /* now create tenpct here */

    TRANSACTION_BEGIN
    EXPECT_SUCCESS
    CURSOR_DECLARE(
        `SELECT random_data.sparse_key, random_tenpct.col_signed,               m4_dnl
                random_tenpct.col_float, random_tenpct.col_double,              m4_dnl
                random_data.col_date, random_data.col_code,                     m4_dnl
                random_data.col_name, random_tenpct.col_address                 m4_dnl
          FROM  random_data, random_tenpct                                      m4_dnl
          WHERE random_data.r10pct_key = random_tenpct.col_key~)
    EXPECT_SUCCESS
    CURSOR_OPEN(CURRENT_CURSOR)
    EXPECT_SUCCESS

    while (1)
    {
        FETCH(CURRENT_CURSOR,
            `sparse_key,  col_signed, col_float, col_double, date_string,
             col_code, col_name, col_address~,
            `%ld          %ld         %f         %lf         %s             m4_dnl
            %s         %s        %s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        if (generateFiles) {
            fprintf(fTenpct, "%ld,%ld,%ld,%f,%lf,%lf,%s,%s,%s,%s\n",
                sparse_key, sparse_key, col_signed, col_float,
                col_double, col_double,
                date_string, col_code, col_name, col_address);
        } else {
            DML_INSERT_VARIABLE_VALUES(tenpct,
                `col_key, col_int, col_signed, col_float,                       m4_dnl
                 col_double, col_decim, col_date,                               m4_dnl
                 col_code, col_name, col_address~,
                `VARIABLES_LIST(
                  sparse_key %ld, sparse_key %ld, col_signed %ld, col_float %f, m4_dnl
                  col_double %lf, col_double %lf),                              m4_dnl
                  SQL_DATE(date_string) -,                                      m4_dnl
                  VARIABLES_LIST(col_code %s, col_name %s, col_address %s)~)
            EXPECT_SUCCESS 
        }
    }
    CURSOR_CLOSE(CURRENT_CURSOR)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    if (generateFiles) {
        fprintf(fTiny, "0\n");  /* a really tiny data set */
        if (fclose(fHundred))
            BUGOUT("closing asap.hundred");
        if (fclose(fTenpct))
            BUGOUT("closing asap.tenpct");
        if (fclose(fUniques))
            BUGOUT("closing asap.uniques");
        if (fclose(fUpdates))
            BUGOUT("closing asap.updates");
        if (fclose(fTiny))
            BUGOUT("closing asap.tiny");
    }
    DDL(`drop table random_data~)
    EXPECT_SUCCESS
    DDL(`drop table random_tenpct~)
    EXPECT_SUCCESS
    return 0;
}

int
create_idx_hundred_code_h()
{
    if (doIndexes)
    {
        if (NO_HASH_INDEX) {
            DDL(`create index hundred_code_h on hundred (col_code)~)
        } else {
            DDL(`create index hundred_code_h on hundred HASH(col_code)~)
        }
        EXPECT_SUCCESS 
    }
    return 0;
}


int
create_idx_hundred_key_bt()
{
    if (doIndexes)
    {
        DDL(`create unique index hundred_key_bt on hundred BTREE(col_key)~)
        EXPECT_SUCCESS
        CLUSTER(`hundred_key_bt~,`hundred~)
        EXPECT_SUCCESS
    }
    return 0;
}


int
create_idx_tenpct_code_h()
{
    if (doIndexes)
    {
        if (NO_HASH_INDEX) {
            DDL(`create index tenpct_code_h on tenpct (col_code)~)
        } else {
            DDL(`create index tenpct_code_h on tenpct HASH(col_code)~)
        }
        EXPECT_SUCCESS 
    }
    return 0;
}


int
create_idx_tenpct_decim_bt()
{
    if (doIndexes)
    {
        DDL(`create index tenpct_decim_bt on tenpct BTREE(col_decim)~)
        EXPECT_SUCCESS
    }
    return 0; 
}


int
create_idx_tenpct_double_bt()
{
    if (doIndexes)
    {
        DDL(`create index tenpct_double_bt on tenpct BTREE(col_double)~)
        EXPECT_SUCCESS
    } 
    return 0;
}


int
create_idx_tenpct_float_bt()
{
    if (doIndexes)
    {
        DDL(`create index tenpct_float_bt on tenpct BTREE(col_float)~)
        EXPECT_SUCCESS
    }
    return 0;
}


int
create_idx_tenpct_int_bt()
{
    if (doIndexes)
    {
        DDL(`create index tenpct_int_bt on tenpct BTREE(col_int)~)
        EXPECT_SUCCESS
    }
    return 0;
}

int
create_idx_tenpct_key_bt()
{
    if (doIndexes)
    {
        DDL(`create unique index tenpct_key_bt on tenpct BTREE(col_key, col_code)~)
        EXPECT_SUCCESS
        CLUSTER(`tenpct_key_bt~,`tenpct~)
        EXPECT_SUCCESS
    }
    return 0;
}


int
create_idx_tenpct_key_code_bt()
{
    if (doIndexes)
    {
        DDL(`create unique index tenpct_key_code_bt on tenpct BTREE(col_key, col_code)~)
        EXPECT_SUCCESS
    }
    return 0;
}


int
create_idx_tenpct_name_h()
{
    if (doIndexes)
    {
        if (NO_HASH_INDEX) {
            DDL(`create index tenpct_name_h on tenpct (col_name)~)
        } else {
            DDL(`create index tenpct_name_h on tenpct HASH(col_name)~)
        }
        EXPECT_SUCCESS
    } 
    return 0;
}


int
create_idx_tenpct_signed_bt()
{
    if (doIndexes)
    {
        DDL(`create index tenpct_signed_bt on tenpct BTREE(col_signed)~)
        EXPECT_SUCCESS
    } 
    return 0;
}


int
create_idx_tiny_key_bt()
{
    if (doIndexes)
    {
        DDL(`create unique index tiny_key_bt on tiny BTREE(col_key)~)
        EXPECT_SUCCESS
    }
    return 0;
}


int
create_idx_uniques_code_h()
{
    if (doIndexes)
    {
        if (NO_HASH_INDEX) {
            DDL(`create index uniques_code_h on uniques (col_code)~)
        } else {
            DDL(`create index uniques_code_h on uniques HASH(col_code)~)
        }
        EXPECT_SUCCESS
    }
    return 0; 
} 


int
create_idx_uniques_key_bt()
{
    if (doIndexes)
    {
        DDL(`create unique index uniques_key_bt on uniques BTREE(col_key)~)
        EXPECT_SUCCESS
        CLUSTER(`uniques_key_bt~,`uniques~)
        EXPECT_SUCCESS
    }
    return 0;
} 


int
create_idx_updates_code_h()
{
    if (doIndexes)
    {
        if (NO_HASH_INDEX) {
            DDL(`create index updates_code_h on updates (col_code)~)
        } else {
            DDL(`create index updates_code_h on updates HASH(col_code)~)
        }
        EXPECT_SUCCESS 
    }
    return 0;
}


int
create_idx_updates_decim_bt()
{
    if (doIndexes)
    {
        DDL(`create index updates_decim_bt on updates BTREE(col_decim)~)
        EXPECT_SUCCESS
    }
    return 0; 
}


int
create_idx_updates_double_bt()
{
    if (doIndexes)
    {
        DDL(`create index updates_double_bt on updates BTREE(col_double)~)
        EXPECT_SUCCESS
    } 
    return 0;
} 


int
create_idx_updates_int_bt()
{
    if (doIndexes)
    {
        DDL(`create index updates_int_bt on updates BTREE(col_int)~)
        EXPECT_SUCCESS
    }
    return 0;
} 


int
create_idx_updates_key_bt()
{
   if (doIndexes)
   {
        DDL(`create unique index updates_key_bt on updates BTREE(col_key)~)
        EXPECT_SUCCESS
        CLUSTER(`updates_key_bt~,`updates~)
        EXPECT_SUCCESS
    }
    return 0;
}


int
create_tables()
{ 
    CREATE_TABLE_STANDARD(`uniques~)
    EXPECT_SUCCESS
    CREATE_TABLE_STANDARD(`updates~)
    EXPECT_SUCCESS
    CREATE_TABLE_STANDARD(`hundred~)
    EXPECT_SUCCESS
    CREATE_TABLE_STANDARD(`tenpct~)
    EXPECT_SUCCESS
    CREATE_TABLE(`tiny~, `INT4(col_key, `not null~)~)
    EXPECT_SUCCESS
    return 0;
}


int
drop_updates_keys()
{
    if (doIndexes)
    {
        TRANSACTION_BEGIN
        EXPECT_SUCCESS 
        DROP_INDEX(updates, updates_int_bt);
        EXPECT_SUCCESS
        DROP_INDEX(updates, updates_double_bt);
        EXPECT_SUCCESS
        DROP_INDEX(updates, updates_decim_bt);
        EXPECT_SUCCESS
        DROP_INDEX(updates, updates_code_h);
        EXPECT_SUCCESS
        TRANSACTION_COMMIT
        EXPECT_SUCCESS
    }
    return 0;
}


int
integrity_test()
{
    TRANSACTION_BEGIN
        CREATE_TABLE_STANDARD(`integrity_temp~)
        DML(`insert into integrity_temp select * from hundred where col_int=0~)
        EXPECT_SUCCESS(`TRANSACTION_ROLLBACK return -1;~)
        DDL(
            `update hundred set             m4_dnl
                col_signed = '-500000000'   m4_dnl
                where col_int = 0           m4_dnl
            ~)
        EXPECT_ANY(`TRANSACTION_ROLLBACK return 0;~)
                /* Presume a referential integrity error */
        TRANSACTION_COMMIT    /* maybe we don't check until commit?  */
        EXPECT_ANY(`TRANSACTION_ROLLBACK return 0;~)
                /* Presume a referential integrity error */
        TESTFAILED("integrity test failed; trying to recover\n"
		"(consider running with \"--restrict rollback\")");
        TRANSACTION_BEGIN
        DML(`delete from hundred where col_int = 0~)
        EXPECT_SUCCESS(`TRANSACTION_ROLLBACK return -1;~)
        DML(`insert into hundred select * from integrity_temp~)
        EXPECT_SUCCESS(`TRANSACTION_ROLLBACK return -1;~)
        DDL(`drop table integrity_temp~)
        EXPECT_SUCCESS(`TRANSACTION_ROLLBACK return -1;~)
    TRANSACTION_COMMIT
    return 0;
}


int
join_2()
{
    DECLARE_BEGIN
        char    uc_signed[21],
                uc_name[21],
                hc_signed[21],
                hc_name[21];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
        ` select uniques.col_signed, uniques.col_name,              m4_dnl
                    hundred.col_signed, hundred.col_name            m4_dnl
             from uniques, hundred                                  m4_dnl
             where uniques.col_address = hundred.col_address        m4_dnl
               and uniques.col_address = 'SILICON VALLEY'           m4_dnl
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `uc_signed, uc_name, hc_signed, hc_name~,
              `%s         %s       %s         %s ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("join_2 says: %s %s %s %s\n",
                    uc_signed, uc_name, hc_signed, hc_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
join_2_cl()
{
    DECLARE_BEGIN
        char    uc_signed[21],
                uc_name[21],
                hc_signed[21],
                hc_name[21];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select uniques.col_signed, uniques.col_name,        m4_dnl
                    hundred.col_signed, hundred.col_name        m4_dnl
             from uniques, hundred                              m4_dnl
             where uniques.col_key = hundred.col_key            m4_dnl
               and uniques.col_key =1000~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `uc_signed, uc_name, hc_signed, hc_name~,
              `%s         %s       %s         %s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("join_2 says: %s %s %s %s\n",
                    uc_signed, uc_name, hc_signed, hc_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
join_2_ncl()
{
    DECLARE_BEGIN
        char    uc_signed[21],
                uc_name[21],
                hc_signed[21],
                hc_name[21];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
        ` select uniques.col_signed, uniques.col_name,              m4_dnl
                    hundred.col_signed, hundred.col_name            m4_dnl
             from uniques, hundred                                  m4_dnl
             where uniques.col_code = hundred.col_code              m4_dnl
               and uniques.col_code = 'BENCHMARKS'                  m4_dnl
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `uc_signed, uc_name, hc_signed, hc_name~,
              `%s         %s       %s         %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("join_2 says: %s %s %s %s\n",
                    uc_signed, uc_name, hc_signed, hc_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return count;
}


int
join_3_cl()
{
    DECLARE_BEGIN
    char    uc_signed[21],
            uc_date[21],
            hc_signed[21],
            hc_date[21],
            tc_signed[21],
            tc_date[21];
    DECLARE_END
    int     count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select uniques.col_signed, uniques.col_date,        m4_dnl
                    hundred.col_signed, hundred.col_date,       m4_dnl
                    tenpct.col_signed, tenpct.col_date          m4_dnl
             from uniques, hundred, tenpct                      m4_dnl
             where uniques.col_key = hundred.col_key            m4_dnl
               and uniques.col_key = tenpct.col_key             m4_dnl
               and uniques.col_key = 1000~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `uc_signed, uc_date, hc_signed, hc_date, tc_signed, tc_date~,
              `%s         %s       %s         %s       %s         %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
        {
            printf("join_3_cl says: %s %s %s %s %s %s\n",
                uc_signed, uc_date, hc_signed, hc_date,
                tc_signed, tc_date);
        }
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
join_3_ncl()
{
    DECLARE_BEGIN
        char    uc_signed[20],
                uc_date[20],
                hc_signed[20],
                hc_date[20],
                tc_signed[20],
                tc_date[20];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
        ` select uniques.col_signed, uniques.col_date,          m4_dnl
                    hundred.col_signed, hundred.col_date,       m4_dnl
                    tenpct.col_signed, tenpct.col_date          m4_dnl
             from uniques, hundred, tenpct                      m4_dnl
             where uniques.col_code = hundred.col_code          m4_dnl
               and uniques.col_code = tenpct.col_code           m4_dnl
               and uniques.col_code = 'BENCHMARKS'              m4_dnl
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `uc_signed, uc_date, hc_signed, hc_date, tc_signed, tc_date~,
              `%s         %s       %s         %s       %s         %s   ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
        {
            printf("join_3_ncl says: %s %s %s %s %s %s\n",
                uc_signed, uc_date, hc_signed, hc_date,
                tc_signed, tc_date);
        }
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
join_4_cl()
{
    DECLARE_BEGIN
            char    uc_date[21],
                    hc_date[21],
                    tc_date[21],
                    up_date[21];
    DECLARE_END
    int             count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select uniques.col_date, hundred.col_date,          m4_dnl
                    tenpct.col_date, updates.col_date           m4_dnl
             from uniques, hundred, tenpct, updates             m4_dnl
             where uniques.col_key = hundred.col_key            m4_dnl
               and uniques.col_key = tenpct.col_key             m4_dnl
               and uniques.col_key = updates.col_key            m4_dnl
               and uniques.col_key = 1000~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `uc_date, hc_date, tc_date, up_date~,
              `%s       %s       %s       %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("join_4_cl says: %s %s %s %s\n",
                uc_date, hc_date, tc_date, up_date);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
join_4_ncl()
{
    DECLARE_BEGIN
            char    unc_date[21],
                    hun_date[21],
                    ten_date[21],
                    upd_date[21];
    DECLARE_END
    int             count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
        ` select uniques.col_date, hundred.col_date,            m4_dnl
                    tenpct.col_date, updates.col_date           m4_dnl
             from uniques, hundred, tenpct, updates             m4_dnl
             where uniques.col_code = hundred.col_code          m4_dnl
               and uniques.col_code = tenpct.col_code           m4_dnl
               and uniques.col_code = updates.col_code          m4_dnl
               and uniques.col_code = 'BENCHMARKS'              m4_dnl
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `unc_date, hun_date, ten_date, upd_date~,
              `%s        %s        %s        %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("join_4_ncl says: %s %s %s %s\n",
                unc_date, hun_date, ten_date, upd_date);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
mu_checkmod_100_rand()
{
    DECLARE_BEGIN
        int     checkCount;
    DECLARE_END

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select count(*) from updates, sel100rand                    m4_dnl
                where updates.col_key=sel100rand.col_key                m4_dnl
                  and not (updates.col_double=sel100rand.col_double)~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    FETCH(`CURRENT_CURSOR~, `checkCount~, `%d~)
    EXPECT_SUCCESS
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    if (checkCount!=100)
    {
        fprintf(stderr, "\ncheckmod_100_rand() returned %d\n",
            checkCount);
        TESTFAILED("checkmod_100_rand()");
    }
    DDL(`drop table sel100rand~)
    EXPECT_SUCCESS
    return checkCount;
}


int
mu_checkmod_100_seq()
{
    DECLARE_BEGIN
        int     checkCount;
    DECLARE_END

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select count(*) from updates, sel100seq                 m4_dnl
                where updates.col_key=sel100seq.col_key             m4_dnl
                  and not (updates.col_double=sel100seq.col_double)~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    FETCH(`CURRENT_CURSOR~, `checkCount~, `%d~)
    EXPECT_SUCCESS
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    if (checkCount!=100)
    {
        fprintf(stderr, "\ncheckmod_100_seq() returned %d\n",
            checkCount);
        TESTFAILED("checkmod_100_seq()");
    }
    DDL(`drop table sel100seq~)
    EXPECT_SUCCESS 
    return checkCount;
}


int
mu_ir_select()
{
    DECLARE_BEGIN
        int     randomKey;
        char    col_key[20],
                col_code[20],
                col_date[20],
                col_signed[20],
                col_name[20];
    DECLARE_END
    int         count=0;

    struct      tms tv;

    currentTest="mu_ir_select()";
    srand(times(&tv));
    randomKey=1;
    while (randomKey==1)
        randomKey=RAND(0,tupleCount);   /* there IS no key 1 */

    TRANSACTION_BEGIN
    m4_ifelse(SQL_MODE, `callable~,
        `sprintf(queryString,
            "select col_key, col_code, col_date, col_signed, col_name       m4_dnl
             from updates                                                   m4_dnl
             where col_key = %ld", randomKey);
             CURSOR_DECLARE_SVAR(queryString);~,
        `CURSOR_DECLARE(` 
            select col_key, col_code, col_date, col_signed, col_name 
            into :col_key, :col_code, :col_date, :col_signed, :col_name 
            from updates 
            where col_key = :randomKey;~)~)
    IF_NOTFOUND(
        `printf("?mu_ir_select() missing record %d\n", randomKey);
         return 0;
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    FETCH(`CURRENT_CURSOR~,
        `col_key, col_code, col_date, col_signed, col_name~,
        `%s       %s        %s        %s          %s~)
    EXPECT_SUCCESS
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    count++;
    if (watchAll)
        printf("mu_ir_select says: %s %s %s %s %s\n",
            col_key, col_code, col_date, col_signed, col_name);
    TRANSACTION_COMMIT
    return count;
}


int
mu_mod_100_rand()
{
    int         count=0;
    LOCK_PROTECT(
       `update updates                          m4_dnl
        set col_double=col_double+100000000     m4_dnl
        where col_int between 1001 and 1100~)
    return count;
}


int
mu_mod_100_seq_abort()
{ 
    int        counter, tryAgain;

    counter=MAX_RETRIES;
    tryAgain=1;
    while (tryAgain && (counter--)>0)
    {
        TRANSACTION_BEGIN
        DML(`update updates                         m4_dnl
             set col_double = col_double+100000000  m4_dnl
             where col_int between 1001 and 1100~)
        tryAgain=0;
        EXPECT_ANY(`
            tryAgain=1;
            if (watch)
                printf("tuple locked; retrying (%d:%d)\n",
                        myPid, counter);
            if (counter)
            { 
                TRANSACTION_ROLLBACK
                EXPECT_SUCCESS
                locked_pause();
            }~)
    }
    if (tryAgain)
        BUGOUT("lock problem");
    TRANSACTION_ROLLBACK
    EXPECT_SUCCESS
    return 0;
}


int
mu_oltp_update()
{
    DECLARE_BEGIN
        long     randKey;
    DECLARE_END
    struct      tms t;

    srand(times(&t));
    randKey=1;
    while (randKey==1)
        randKey=RAND(0, tupleCount);    /* There IS no col_key 1 */

    m4_ifelse(SQL_MODE, `callable~,
          `sprintf(queryString,
              "update updates set col_signed=col_signed+1 where col_key=%ld",
              randKey);
           LOCK_PROTECT_S(queryString)~, 
          `LOCK_PROTECT(
          `update updates set col_signed=col_signed+1 where col_key=:randKey;~)~
    )
    return 0;
}


int
mu_sel_100_rand()
{
    TRANSACTION_BEGIN
    CREATE_TABLE_STANDARD(`sel100rand~)
    EXPECT_SUCCESS
    DML(`insert into sel100rand select * from updates           m4_dnl
                where updates.col_int between 1001 and 1100     m4_dnl
                USE_INTO_TEMP(`sel100rand~)~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return 0;
}


int
mu_sel_100_seq()
{
    TRANSACTION_BEGIN
    CREATE_TABLE_STANDARD(`sel100seq~)
    EXPECT_SUCCESS
    DML(`insert into sel100seq select * from updates            m4_dnl
                where updates.col_key between 1001 and 1100     m4_dnl
                USE_INTO_TEMP(`sel100seq~)~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
mu_unmod_100_rand()
{
    LOCK_PROTECT(
           `update updates                              m4_dnl
            set col_double=col_double-100000000         m4_dnl
            where col_key between 1001 and 1100~)
    return 0;
}


int
mu_unmod_100_seq()
{
    LOCK_PROTECT(
           `update updates                          m4_dnl
            set col_double=col_double-100000000     m4_dnl
            where col_key between 1001 and 1100~)
    return 0;
}


int
proj_100()
{
    DECLARE_BEGIN
            char    c_address[81],
                    c_signed[21];
    DECLARE_END
    int             count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(`select distinct col_address, col_signed from hundred~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,` c_address, c_signed~, `%s %s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("proj_100 says: %s %s\n", c_address, c_signed);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS 
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
proj_10pct()
{
    DECLARE_BEGIN
            char    c_signed[21];
    DECLARE_END
    int             count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(`select distinct col_signed from tenpct~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~, `c_signed~, `%s~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("proj_10pct says: %s\n", c_signed);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS 
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
sel_1_cl()
{
    DECLARE_BEGIN
            char    c_key[21],
                    c_int[21],
                    c_signed[21],
                    c_code[21],
                    c_double[21],
                    c_name[21];
    DECLARE_END
    int             count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select col_key, col_int, col_signed, col_code,      m4_dnl
                    col_double, col_name                        m4_dnl
        from updates where col_key = 1000~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while(1)
    {
        FETCH(`CURRENT_CURSOR~,
              `c_key, c_int, c_signed, c_code, c_double, c_name~,
              `%s     %s     %s        %s      %s        %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("sel_1_ncl says: %s %s %s %s %s %s\n",
                    c_key, c_int, c_signed, c_code, c_double, c_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
sel_1_ncl()
{
    DECLARE_BEGIN
            char    c_key[21],
                    c_int[21],
                    c_signed[21],
                    c_code[21],
                    c_double[21],
                    c_name[21];
    DECLARE_END
    int             count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
        ` select col_key, col_int, col_signed, col_code, col_double, col_name m4_dnl
            from updates where col_code = 'BENCHMARKS'                        m4_dnl
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `c_key, c_int, c_signed, c_code, c_double, c_name~,
              `%s     %s     %s        %s      %s        %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("sel_1_ncl says: %s %s %s %s %s %s\n",
                    c_key, c_int, c_signed, c_code, c_double, c_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
sel_100_cl()
{
    DECLARE_BEGIN
        char    col_key[21],
                col_int[21],
                col_signed[21],
                col_code[21],
                col_double[21],
                col_name[21];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select col_key, col_int, col_signed, col_code,      m4_dnl
                    col_double, col_name                        m4_dnl
        from updates where col_key <= 100~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `col_key, col_int, col_signed, col_code, col_double, col_name~,
              `%s       %s       %s          %s        %s          %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
        {
            printf("sel_100_cl says: %s %s %s %s %s %s\n",
                    col_key, col_int, col_signed,
                    col_code, col_double, col_name);
        }
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
sel_100_ncl()
{
    DECLARE_BEGIN
        char    col_key[21],
                col_int[21],
                col_signed[21],
                col_code[21],
                col_double[21],
                col_name[21];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE( 
           `select col_key, col_int, col_signed, col_code,      m4_dnl
                    col_double, col_name                        m4_dnl
        from updates where col_int <= 100~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `col_key, col_int, col_signed, col_code, col_double, col_name~,
              `%s       %s       %s          %s        %s          %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
        {
            printf("sel_100_cl says: %s %s %s %s %s %s\n",
                    col_key, col_int, col_signed,
                    col_code, col_double, col_name);
        }
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
sel_10pct_ncl()
{
    DECLARE_BEGIN
        char    c_key[20],
                c_int[20],
                c_signed[20],
                c_code[20],
                c_double[20],
                c_name[20];
    DECLARE_END
    int         count=0;

    TRANSACTION_BEGIN
    CURSOR_DECLARE(
        ` select col_key, col_int, col_signed, col_code, col_double, col_name       m4_dnl
            from tenpct where col_name = 'THE+ASAP+BENCHMARKS+'                     m4_dnl
        ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `c_key, c_int, c_signed, c_code, c_double, c_name~,
              `%s     %s     %s        %s      %s        %s   ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("sel_10pct_ncl says: %s %s %s %s %s %s\n",
                c_key, c_int, c_signed, c_code, c_double, c_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT 
    EXPECT_SUCCESS
    return count;
}


int
sel_variable_select_high()
{
    DECLARE_BEGIN
    long    prog_var;
    char    c_key[21],
            c_int[21],
            c_signed[21],
            c_code[21],
            c_double[21],
            c_name[21];
    DECLARE_END
    int     count=0;

    TRANSACTION_BEGIN
    prog_var = -250000000;
    m4_ifelse(SQL_MODE, `embedded~,
        `CURSOR_DECLARE(`select col_key, col_int, col_signed, col_code, col_double, col_name
                from tenpct where col_signed < :prog_var;~)~,
        `sprintf(queryString,
            "select col_key, col_int, col_signed, col_code, col_double, col_name        m4_dnl
                from tenpct where col_signed < %ld", prog_var);
         cursorDeclare(queryString); ~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `c_key, c_int, c_signed, c_code, c_double, c_name~,
              `%s     %s     %s        %s      %s        %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("sel_variable_select_high says: "
                   "%s %s %s %s %s %s\n",
                   c_key, c_int, c_signed, c_code, c_double, c_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return count;
}


int
sel_variable_select_low()
{
    DECLARE_BEGIN
    long    prog_var;
    char    c_key[21],
            c_int[21],
            c_signed[21],
            c_code[21],
            c_double[21],
            c_name[21];
    DECLARE_END
    int     count=0;

    TRANSACTION_BEGIN
    prog_var = -500000000;
    m4_ifelse(SQL_MODE, callable,
        `sprintf(queryString,
            "select col_key, col_int, col_signed, col_code, col_double, col_name     m4_dnl
             from tenpct where col_signed <%ld", prog_var);
         cursorDeclare(queryString);~,
        `CURSOR_DECLARE(`select col_key, col_int, col_signed, col_code, col_double, col_name
                    from tenpct where col_signed < :prog_var;~)~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH(`CURRENT_CURSOR~,
              `c_key, c_int, c_signed, c_code, c_double, c_name~,
              `%s     %s     %s        %s      %s        %s  ~)
        IF_NOTFOUND(`break;~)
        EXPECT_SUCCESS
        count++;
        if (watchAll)
            printf("sel_variable_select_high says: "
                   "%s %s %s %s %s %s\n",
                   c_key, c_int, c_signed, c_code, c_double, c_name);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return count;
}


int
table_scan()
{
    int         count=0;

    DECLARE_BEGIN
        char   col_key[20], 
               col_int[20], 
               col_signed[20], 
               col_float[20], 
               col_double[30], 
               col_decim[22], 
               col_date[20], 
               col_code[11], 
               col_name[21]; 
        char   col_address[81]; 
    DECLARE_END
    char  column_buffer[300];


    TRANSACTION_BEGIN
    CURSOR_DECLARE(`select * from uniques where col_int = 1~)
    EXPECT_SUCCESS
    CURSOR_OPEN(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    while (1)
    {
        FETCH_TUPLE;
        IF_NOTFOUND(`break;~)
        count++;
        if (watchAll)
            printf("table_scan says: %s\n", column_buffer);
    }
    CURSOR_CLOSE(`CURRENT_CURSOR~)
    EXPECT_SUCCESS
    return count;
}


int
upd_app_t_end()
{
    int         count=0;
    TRANSACTION_BEGIN
    DML(
        `insert into updates                                                    m4_dnl
             values (1000000001, 50005, 50005, 50005.00, 50005.00, 50005.00,    m4_dnl
             '1/1/1988', 'CONTROLLER', 'ALICE IN WONDERLAND',                   m4_dnl
                     'UNIVERSITY OF ILLINOIS AT CHICAGO')                       m4_dnl
        ~) 
    EXPECT_SUCCESS
    count++;
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
upd_app_t_mid()
{
    int         count=0;
    TRANSACTION_BEGIN
    DML(
         ` insert into updates  (col_key, col_int, col_signed, col_float,               m4_dnl
                        col_double, col_decim, col_date, col_code, col_name, col_address) m4_dnl
           values (5005, 5005, 50005, 50005.00, 50005.00, 50005.00,             m4_dnl
                    '1/1/1988', 'CONTROLLER', 'ALICE IN WONDERLAND',            m4_dnl
                     'UNIVERSITY OF ILLINOIS AT CHICAGO')                       m4_dnl
        ~)
    EXPECT_SUCCESS
    count++;
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return count;
}


int
upd_append_duplicate()
{
    if (doIndexes)
    {
        TRANSACTION_BEGIN
        DML(
            ` insert into updates  (col_key, col_int, col_signed, col_float,    m4_dnl
                        col_double, col_decim, col_date, col_code, col_name,    m4_dnl
                        col_address)                                            m4_dnl
              values( 6000, 0, 60000, 39997.90, 50005.00, 50005.00,             m4_dnl
                     '11/10/1985', 'CONTROLLER', 'ALICE IN WONDERLAND',         m4_dnl
                     'UNIVERSITY OF ILLINOIS AT CHICAGO')                       m4_dnl
            ~)
        EXPECT_ANY(`TRANSACTION_ROLLBACK
                    EXPECT_SUCCESS
                    return 0;~)
        TESTFAILED("upd_append_duplicate() failed");
    }
    return 0;
}


int
upd_del_t_end()
{
    TRANSACTION_BEGIN
    DML( ` delete from updates where col_key = '-1000' ~) 
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return 0;
}


int
upd_del_t_mid()
{
    TRANSACTION_BEGIN
    DML( `delete from updates where (col_key='5005') or (col_key='-5000') ~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return 0;
}


/* USED?
int
upd_integrity_prep()
{
    TRANSACTION_BEGIN
        CREATE_TABLE_STANDARD(`integrity_temp~)
        EXPECT_SUCCESS 
        DML(`insert into integrity_temp select * from hundred where col_int = 0~)
        EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
}


int
upd_integrity_restore()
{
    TRANSACTION_BEGIN
    DML(`delete from hundred where col_int = 0~)
    EXPECT_SUCCESS 
    DML(`insert into hundred select * from integrity_temp~)
    EXPECT_SUCCESS 
    DDL(`drop table integrity_temp~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
}


int
upd_integrity_test()
{
    if (doIndexes)
    {
        TRANSACTION_BEGIN
        DML( `update hundred set col_signed = '-500000000' where col_int = 0 ~)
        EXPECT_FAILURE(300,"upd_integrity_test() failed")
        TRANSACTION_ROLLBACK
        EXPECT_SUCCESS
    }
}
*/

int
upd_mod_t_cod()
{
    TRANSACTION_BEGIN
    DML( `update updates set col_code = 'SQL+GROUPS' where col_key = 5005 ~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return 0;
}


int
upd_mod_t_end()
{
    TRANSACTION_BEGIN
    DML( `update updates set col_key = '-1000' where col_key = 1000000001 ~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return 0;
}


int
upd_mod_t_int()
{
    TRANSACTION_BEGIN
    DML(`update updates set col_int = 50015 where col_key = 5005~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS
    return 0;
}


int
upd_mod_t_mid()
{
    TRANSACTION_BEGIN
    DML( `update updates set col_key = '-5000' where col_key = 5005 ~)
    EXPECT_SUCCESS
    TRANSACTION_COMMIT
    EXPECT_SUCCESS 
    return 0;
}


int
upd_remove_duplicate()
{
    TRANSACTION_BEGIN
    DML(`delete from updates where col_key = 6000 and col_int = 0~) 
    /* just cleaning up possible problems from upd_append_duplicate(),
       so we don't really care what the result is.
    */
    return 0;
}
