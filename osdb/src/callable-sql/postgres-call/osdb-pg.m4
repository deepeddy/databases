/***************************************************************************
                  postgres-call.m4  -  PostgreSQL calls for OSDB/callable-sql
                     -------------------
    begin        : Fri Dec 1 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : ariebs@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-pg.m4,v $
Revision 1.6  2004/10/11 22:17:44  ariebs
Make the help message helpful!

Revision 1.5  2003/05/14 03:48:13  ariebs
Implement --dbuser and --dbpassword.

Revision 1.4  2003/05/04 18:14:39  ariebs
More changes to incorporate the-tests.m4 across the board.

Revision 1.3  2003/04/22 10:19:02  ariebs
Add DROP_INDEX macro to accomodate differing syntax between databases.
(Note that SQL92 appears not to have defined standard language for this.)

Revision 1.2  2003/03/04 03:32:15  ariebs
Modify osdb-pg-ui to compile with (but not yet work with) the-tests.m4

Revision 1.1  2003/02/17 18:09:36  ariebs
Oops -- check this file in!

*/

#include "callable-sql.h"
#include "osdb.h"
#include "libpq-fe.h"

void interpretPGresult();

char        cmd[MAX_TUPLE_LENGTH],   /* frequently used, but very transient */
            DB_CONNECT_CMD[MAX_TUPLE_LENGTH]; /* PQfinish() wants this string to exist! */
PGresult    *PGRESULT;
PGconn      *PGCONN;


#define EMB_DUPLICATE_ERROR -400

m4_changequote([,])
m4_changequote(`,~)

m4_define(`BTREE~,`using btree($*)~)m4_dnl
m4_define(`HASH~,`using hash($*)~)m4_dnl
m4_define(`CLUSTER~,`ddl("cluster $1 on $2");~)m4_dnl
m4_define(`SQL_DATE~,
        `qvPtr+=sprintf(qvPtr, "%s DATE(%s%s%s)", comma, apostrophe, $1, apostrophe);~)m4_dnl 

m4_define(`USE_INTO_TABLE~, `into table $1~)m4_dnl
m4_define(`USE_INTO_TEMP~, `~)m4_dnl


/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    timestamp $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float4 $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`DROP_INDEX~, `ddl("drop index $2");~)

m4_include(`../callable-sql.m4~)
m4_include(`../../the-tests.m4~)

int
argument_check(char* arg)
{
    char*   p;

    if (strncmp(arg, "--postgresql=", 13)==0) {
        p=arg+13;
        if (strncmp(p, "no_hash_index", 13)==0) {
            NO_HASH_INDEX=TRUE;
            return(TRUE);
        }
    }
    return(FALSE);
}

void
argument_choices() {
    printf("\n  PostgreSQL-specific options:\n"
"    [--postgresql=no_hash_index]\n"
"                        Do not use hash indexes (historically\n"
"                        a performance problem in PostgreSQL.\n"
);
}

void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    if (PGRESULT)
        fprintf(stderr,
            "PQresultStatus: %d\n", PQresultStatus(PGRESULT));
    if (PGCONN) {
        if (PQerrorMessage(PGCONN))
            fprintf(stderr, "postgres reports: %s", PQerrorMessage(PGCONN));
    }
    diagnostics();
    if (++failureCount >= MAX_FAILURES) {
        terminate = TRUE;  /* Updated to be = TRUE.  JC */
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate) {
        PQfinish(PGCONN);
        if (iAmParent) {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed = TRUE;  /* Updated to be = TRUE.  JC */
}


void
createIndexBtree(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create index %s on %s using btree (%s)",
        iName, tName, fields);
    transactionBegin();
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
    transactionCommit();
}


void
createIndexCluster(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create unique index %s on %s using btree (%s)",
        iName, tName, fields);
    transactionBegin();
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
        PQclear(PGRESULT);
        sprintf(cmd, "cluster %s on %s", iName, tName );
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("cluster error"); 
    transactionCommit();
}


void
createIndexForeign(char* tName, char* keyName, char* keyCol,
                        char* fTable, char* fFields) {
    sprintf(cmd,
        "alter table %s add constraint %s foreign key (%s) references %s (%s)",
        tName, keyName, keyCol, fTable, fFields);
    transactionBegin(); 
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("foreign key error"); 
    transactionCommit();
}


void
createIndexHash(char* iName, char* tName, char* fields) {
    if (NO_HASH_INDEX) {    /* works around a PostgreSQL 7.1/7.2 problem */
        sprintf(cmd,
            "create index %s on %s (%s)",
            iName, tName, fields);
    } else {
        sprintf(cmd,
            "create index %s on %s using hash (%s)",
            iName, tName, fields);
    }
    transactionBegin();
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("hash error"); 
    transactionCommit();
}


int
create_idx_hundred_foreign() {
    if (doIndexes) {  /* Pointless without index on updates.key */
        createIndexForeign("hundred", "fk_hundred_updates", "col_signed",
                            "updates", "col_key");
    }
    return 0;
}

void
createTable(char* stg) {
    ddl(stg);
}


void
cursorClose() {
    PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "close MYCURSOR");
    interpretPGresult();
    /* return with the error status */
}


void
cursorDeclare(char* stg) {
    sprintf(cmd, "declare MYCURSOR cursor for %s", stg);
    PGRESULT=PQexec(PGCONN, cmd);
    interpretPGresult(); 
    /* return with the error status */
}


void
cursorFetch(char *dest) {
    int     i;

    if (PGRESULT!=NULL)
        PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "fetch MYCURSOR");
    interpretPGresult();
    switch (OSDB_ERROR.error) {
        case ERR_OK:        break;
        case ERR_NOTFOUND:  return; 
        default:            TESTFAILED("fetch error");
    }
    i=PQntuples(PGRESULT);
    if (i==0)
        TESTFAILED("fetched no tuples");
    if (i>1)
        TESTFAILED("fetched too many tuples");
    strncpy(dest,PQgetvalue(PGRESULT,0,0), MAX_TUPLE_LENGTH);
    for (i=1; i<PQnfields(PGRESULT); i++) {
        strncat(dest, (char*)" ", MAX_TUPLE_LENGTH);
        strncat(dest, PQgetvalue(PGRESULT,0,i), MAX_TUPLE_LENGTH);
    }
}


void
cursorOpen() {
    /* no-op under Postgres */
}

void
databaseConnect() {
    sprintf(DB_CONNECT_CMD,
        "dbname='%s' user='%s' password='%s'",
        DBNAME, DBUSER, DBPASSWORD);
    if (DBHOST) {
        strcat(DB_CONNECT_CMD, " host='");
        strcat(DB_CONNECT_CMD, DBHOST);
        strcat(DB_CONNECT_CMD, "'");
    }
    PGCONN=PQconnectdb(DB_CONNECT_CMD);
    if (PQstatus(PGCONN)==CONNECTION_BAD)
        BUGOUT(DB_CONNECT_CMD);
}

void
databaseCreate(char* dName) {
    char    *args[3];
    OSDB_RUN *pgm;

    fprintf(stderr, "Ignore message if \"drobdb\" fails\n");
    args[0]=(char*)"dropdb";
    args[1]=dName;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
    args[0]=(char*)"createdb";
    args[1]=dName;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
}


void
databaseDisconnect() {
    PQfinish(PGCONN);
}


void
ddl(char* stg) {
    if (watch) fprintf(stderr, "ddl(): %s\n", stg);
    PGRESULT=PQexec(PGCONN,  stg);
    interpretPGresult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DDL;
}


void
dml(char* stg) {
    if (watch) fprintf(stderr, "dml(): %s\n", stg);
    PGRESULT=PQexec(PGCONN, stg);
    interpretPGresult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DML;
}


void
interpretPGresult() {
    if (PGRESULT==NULL)
        TESTFAILED("interpretPGresult error");
    switch (PQresultStatus(PGRESULT)) {
        case PGRES_COMMAND_OK:  OSDB_ERROR.error=ERR_OK;
                                break;
        case PGRES_TUPLES_OK:   if (PQntuples(PGRESULT)) {
                                    OSDB_ERROR.error=ERR_OK;
                                } else {
                                    OSDB_ERROR.error=ERR_NOTFOUND;
                                }
                                break;
        case PGRES_BAD_RESPONSE:
        case PGRES_NONFATAL_ERROR:
        case PGRES_FATAL_ERROR: OSDB_ERROR.error=ERR_UNKNOWN;
                                break;
        case PGRES_COPY_OUT:
        case PGRES_COPY_IN:
        case PGRES_EMPTY_QUERY:
        default:                OSDB_ERROR.error=ERR_UNKNOWN;
                                TESTFAILED(PQresultErrorMessage(PGRESULT));
    }
}


int
load() {
    char    cmd[256];

    snprintf(cmd, 255,
        "copy updates from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UPDATES);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy hundred from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_HUNDRED);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy tenpct from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TENPCT);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy uniques from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UNIQUES);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy tiny from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TINY);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    return 0;
}

void
setDefaults() {
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=getlogin();
    DBPASSWORD=NULL;
    DBNAME="osdb";
    DBPORT=0;
}


void
transactionBegin() {
    PGRESULT=PQexec(PGCONN, "begin");
    interpretPGresult(); 
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
    PQclear(PGRESULT);
}


void
transactionCommit() {
    PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "commit");
    interpretPGresult(); 
    PQclear(PGRESULT);
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}


void
transactionRollback() {
    PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "rollback");
    interpretPGresult(); 
    PQclear(PGRESULT);
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}

