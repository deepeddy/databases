/***************************************************************************
                  callable-sql.h  -  description
                     -------------------
    begin        : Fri Dec 1 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: callable-sql.h,v $
Revision 1.4  2002/07/12 21:32:36  ariebs
Apply Neil Conway's cleanups for compiler warnings

Revision 1.3  2001/08/15 01:07:35  ariebs
Apply Maciej's fixes for foreign key support

Revision 1.2  2001/02/18 14:34:50  ariebs
Clean out obsolete modification history

*/

/* These routines must be tailored for each database system */

int  countTuples(char*);
void createIndexBtree(char*, char*, char*);
void createIndexCluster(char*, char*, char*);
void createIndexForeign(char*, char*, char*, char*, char*);
void createIndexHash(char*, char*, char*);
void createTable(char*);
void cursorClose();
void cursorDeclare(char*);
void cursorFetch(char*);
void cursorOpen();
void databaseConnect();
void databaseCreate(char*);
void databaseDisconnect();
void ddl(char*);
void dml(char*);
int  load();
void transactionBegin();
void transactionCommit();
void transactionRollback();
