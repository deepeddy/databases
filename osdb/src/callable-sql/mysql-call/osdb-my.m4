/***************************************************************************
                  mysql-call.c  -  MySQL calls for OSDB/callable-sql
                     -------------------
    begin        : Fri Dec 1 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-my.m4,v $
Revision 1.8  2005/01/05 04:00:16  ariebs
Remove the cursorClose() code that was breaking on --generate-files, and
which appeared when we removed the LIMIT clause while generating the
random_tenpct table. (I *think* the broken code had originally been put
in to work around a MySQL bug that no longer exists.)

Revision 1.7  2004/10/11 22:17:43  ariebs
Make the help message helpful!

Revision 1.6  2003/05/14 03:48:13  ariebs
Implement --dbuser and --dbpassword.

Revision 1.5  2003/05/04 18:14:39  ariebs
More changes to incorporate the-tests.m4 across the board.

Revision 1.4  2003/04/22 10:19:01  ariebs
Add DROP_INDEX macro to accomodate differing syntax between databases.
(Note that SQL92 appears not to have defined standard language for this.)

Revision 1.3  2003/04/08 03:15:52  ariebs
MySQL doesn't handle BTREE or HASH index declarations; remove them.

Revision 1.2  2003/03/12 03:43:58  ariebs
Incomplete attempt to get the-tests.m4 to work with MySQL.

Revision 1.1  2003/03/05 04:02:53  ariebs
Make the changes necessary to make the MySQL tests work with the-tests.m4;
note that the new stuff doesn't work yet, but it does compile cleanly.

Revision 1.13  2002/03/12 03:58:34  ariebs
Clean up the usage() message formatting.

Revision 1.12  2002/03/10 20:16:46  ariebs
Implement the --mysql={innodb | bdb} option for the MySQL tests.

Revision 1.11  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.10  2002/03/05 03:30:21  ariebs
Add a createTable() call in preparation for allowing database-specific
syntax, e.g., mysql's "table=" options.

Revision 1.9  2001/09/09 07:00:26  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.8  2001/08/16 02:22:05  ariebs
Modify the MySQL tests to use the new foreign key syntax, and update
the "expected" results

Revision 1.7  2001/08/15 01:07:35  ariebs
Apply Maciej's fixes for foreign key support

Revision 1.6  2001/08/11 21:02:18  ariebs
Make all the cluster indexes unique (as they should have been --
Thanks Maciej!)

Revision 1.5  2001/04/13 15:46:20  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

Revision 1.4  2001/02/23 20:38:27  ariebs
Cosmetic changes to
(a) Use forkpty() (so I added LDFLAGS to all the makefiles while I was
    at it, and
(b) Add "diagnostics()" to osdb.c (which perturbed most programs)

Revision 1.2  2001/02/18 14:34:50  ariebs
Clean out obsolete modification history

*/

#include <stdio.h>
#include <string.h>
#include "callable-sql.h"
#include "mysql.h"
#include "osdb.h"

m4_changequote([,])
m4_changequote(`,~)

/* MySQL doesn't do BTREE or HASH */
m4_define(`BTREE~,`($1)~)m4_dnl
m4_define(`HASH~,`($1)~)m4_dnl
m4_define(`CLUSTER~,`~)m4_dnl
m4_define(`SQL_DATE~, $1)m4_dnl
m4_define(`SQL_DATE~,
        `qvPtr+=sprintf(qvPtr, "%s %s", comma, $1);~)m4_dnl 

m4_define(`USE_INTO_TABLE~, `into table $1~)m4_dnl
m4_define(`USE_INTO_TEMP~, `~)m4_dnl


/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    char(20) $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float4 $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`DROP_INDEX~, `ddl("alter table $1 drop index $2");~)

m4_include(`../callable-sql.m4~)
m4_include(`../../the-tests.m4~)


#define     CMD_LENGTH 2048
char        cmd[CMD_LENGTH];   /* frequently used, but very transient */
int         result;
MYSQL       mysql;
MYSQL_RES   *mysql_res;
MYSQL_ROW   mysql_row;
int         TABLE_MODE = 0;
char        *TABLE_MODE_NAMES[]={"normal", "innodb", "bdb", "\0"};


int
argument_check(char* arg)
{
    int     i;
    char*   p;

    if (strncmp(arg, "--mysql=", 8)==0) {
        if (TABLE_MODE) {
            fprintf(stderr, "\nOnly one MySQL mode may be specified\n");
            return(FALSE);
        }
        p=arg+8;
        for (i=0; ; i++) {
            if (TABLE_MODE_NAMES[i][0]==0) {
                fprintf(stderr, "\nUnknown mode for --mysql\n");
                return(FALSE);
            }
            if (strncmp(p, TABLE_MODE_NAMES[i], strlen(TABLE_MODE_NAMES[i]))==0) {
                TABLE_MODE=i;
                return(TRUE);
            }
        }
    }
    return(FALSE);
}

void
argument_choices() {
    int     i;

    printf("\n  MySQL-specific option:\n"
"    [--mysql=<dbtype>]  Use the <dbtype> engine, where <dbtype> can\n"
"                        one of"
);
    for (i=0; ; i++ ) {
        if (TABLE_MODE_NAMES[i][0]==0) {
            break;
        }
        printf(" %s", TABLE_MODE_NAMES[i]);
    }
    printf("\n");
}


void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    if (mysql_errno(&mysql))
            fprintf(stderr,
                "mysql reports: %d: %s\n",
                mysql_errno(&mysql), mysql_error(&mysql));
    diagnostics();
    if (++failureCount >= MAX_FAILURES) {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate) {
        mysql_close(&mysql);
        if (iAmParent) {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed=1;
}


void
createIndexBtree(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin(); 
        if (mysql_query(&mysql, cmd))
            TESTFAILED("btree error");
    transactionCommit();
}


void
createIndexCluster(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create unique index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        if (mysql_query(&mysql, cmd))
            TESTFAILED("btree error");
    transactionCommit();
}


void
createIndexForeign(char* tName, char* keyName, char* keyCol,
                        char* fTable, char* fFields) {
    sprintf(cmd,
        "alter table %s add constraint %s foreign key (%s) references %s (%s)",
        tName, keyName, keyCol, fTable, fFields);
    transactionBegin(); 
        if (mysql_query(&mysql, cmd))
            TESTFAILED("foreign key error"); 
    transactionCommit();
}


void
createIndexHash(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        if (mysql_query(&mysql, cmd))
            TESTFAILED("hash error"); 
    transactionCommit();
}


int
create_idx_hundred_foreign() {
    if (doIndexes) {  /* Pointless without index on updates.key */
        createIndexForeign("hundred", "fk_hundred_updates", "col_signed",
                            "updates", "col_key");
    }
    return 0;
}



void
createTable(char* stg) {
    strncpy(cmd, stg, CMD_LENGTH);
    if (TABLE_MODE) {
        switch (TABLE_MODE) {
          case 1:   strncat(cmd, " type=innodb", CMD_LENGTH);
                    break;
          case 2:   strncat(cmd, " type=bdb", CMD_LENGTH);
        }
    }
    ddl(cmd);
}


void
cursorClose() {
    /* MySQL has no cursor to close. */

    OSDB_ERROR.error=ERR_OK;
    if (watchAll)
        printf("close cursor\n");
    mysql_free_result(mysql_res);
}


void
cursorDeclare(char* stg) {
/* simply pass the query without declaring a cursor */

    OSDB_ERROR.error=ERR_OK;
    if (watchAll)
        printf("declare cursor\n");
    if ((result=mysql_query(&mysql, stg))) {
        OSDB_ERROR.error=ERR_DML;
        return;
    }
    if ((mysql_res=mysql_use_result(&mysql))==NULL)
        OSDB_ERROR.error=ERR_DML;
}


void
cursorFetch(char *dest) {
    int     i;

    OSDB_ERROR.error=ERR_OK;
    if ((mysql_row=mysql_fetch_row(mysql_res))) {
        strncpy(dest,mysql_row[0], MAX_TUPLE_LENGTH);
        for (i=1; i<mysql_field_count(&mysql); i++) {
            strncat(dest, (char*)" ", MAX_TUPLE_LENGTH);
            strncat(dest, mysql_row[i], MAX_TUPLE_LENGTH);
            if (watchAll)
                printf("fetch> %70s\n", dest);
        }
    } else if (mysql_errno(&mysql)) {
        TESTFAILED("cursorFetch()");
        OSDB_ERROR.error=ERR_UNKNOWN;
    } else {
        OSDB_ERROR.error=ERR_NOTFOUND;
    }
}


void
cursorOpen() {
    if (watchAll)
        printf("open cursor\n");
    /* no-op */
    OSDB_ERROR.error=ERR_OK;
}

void
databaseConnect() {
    mysql_init(&mysql);
    if(mysql_real_connect(&mysql, DBHOST, DBUSER, DBPASSWORD, DBNAME, DBPORT, NULL, 0))
        return;
    BUGOUT("database connect failed");
}

void
databaseCreate(char* dName) {
    mysql_init(&mysql);
    if ((mysql_real_connect(&mysql, DBHOST, DBUSER, DBPASSWORD, NULL, DBPORT, NULL, 0))==NULL)
        BUGOUT("connect for create failed");

    snprintf(cmd, CMD_LENGTH, "drop database %s", dName);
        /* It's OK if "drop database" fails */
    mysql_query(&mysql, cmd);
    snprintf(cmd, CMD_LENGTH, "create database %s", dName);
    if (mysql_query(&mysql,cmd))
        BUGOUT("create database failed");
}


void
databaseDisconnect() {
    OSDB_ERROR.error=ERR_OK;
    mysql_close(&mysql);
}


void
ddl(char* stg) {
    
    OSDB_ERROR.error=ERR_OK;
    if (watchAll) {
        printf("\nddl> %s\n", stg);
    } 
    if (mysql_query(&mysql,  stg))
        OSDB_ERROR.error=ERR_DDL;
}


void
dml(char* stg) {
    OSDB_ERROR.error=ERR_OK;
    if (watchAll) {
        printf("\ndml> %s\n", stg);
    } 
    if (mysql_query(&mysql,  stg))
        OSDB_ERROR.error=ERR_DML;
}


int
load() {
    OSDB_ERROR.error=ERR_OK;
    snprintf(cmd, CMD_LENGTH,
        "load data infile '%s/%s' into table %s fields terminated by ','",
        dataDir, FILENAME_UPDATES, "updates");
    if (mysql_query(&mysql, cmd))
        BUGOUT("load updates");
    snprintf(cmd, CMD_LENGTH,
        "load data infile '%s/%s' into table %s fields terminated by ','",
        dataDir, FILENAME_HUNDRED, "hundred");
    if (mysql_query(&mysql, cmd))
        BUGOUT("load hundred");
    snprintf(cmd, CMD_LENGTH,
        "load data infile '%s/%s' into table %s fields terminated by ','",
        dataDir, FILENAME_TENPCT, "tenpct");
    if (mysql_query(&mysql, cmd))
        BUGOUT("load tenpct");
    snprintf(cmd, CMD_LENGTH,
        "load data infile '%s/%s' into table %s fields terminated by ','",
        dataDir, FILENAME_UNIQUES, "uniques");
    if (mysql_query(&mysql, cmd))
        BUGOUT("load uniques");
    snprintf(cmd, CMD_LENGTH,
        "load data infile '%s/%s' into table %s fields terminated by ','",
        dataDir, FILENAME_TINY, "tiny");
    if (mysql_query(&mysql, cmd))
        BUGOUT("load tiny");
    return 0;
}


void
setDefaults() {
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=(char*)"root";
    DBNAME=(char*)"osdb";
    DBPASSWORD=NULL;
    DBPORT=0;
}


void
transactionBegin() {
    dml("begin");
}


void
transactionCommit() {
/*  not available */
    dml("commit");
}


void
transactionRollback() {
    dml("rollback");
}

