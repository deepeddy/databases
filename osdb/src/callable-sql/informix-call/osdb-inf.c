/***************************************************************************
                  osdb-inf.c  -  Informix calls for OSDB/callable-sql
                     -------------------
    begin        : Sun Apr 15 2001
    copyright    : (C) 2001 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-inf.c,v $
Revision 1.6  2002/07/12 21:32:36  ariebs
Apply Neil Conway's cleanups for compiler warnings

Revision 1.5  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.4  2002/03/05 03:30:21  ariebs
Add a createTable() call in preparation for allowing database-specific
syntax, e.g., mysql's "table=" options.

Revision 1.3  2001/09/09 07:00:26  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.2  2001/08/11 21:02:18  ariebs
Make all the cluster indexes unique (as they should have been --
Thanks Maciej!)

Revision 1.1  2001/04/16 01:12:27  ariebs
Commit the first vestiges of callable Informix support

*/

#include "callable-sql.h"
#include "osdb.h"
#include "libpq-fe.h"

void interpretPGresult();

char        cmd[200],   /* frequently used, but very transient */
            DB_CONNECT_CMD[200]; /* PQfinish() wants this string to exist! */
PGresult    *PGRESULT;
PGconn      *PGCONN;


int
argument_check(char* arg)
{
    return(FALSE);  /* we recognize no arguments here */
}

void
argument_choices() {
}


void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    if (PGRESULT)
        fprintf(stderr,
            "PQresultStatus: %d\n", PQresultStatus(PGRESULT));
    if (PQerrorMessage(PGCONN))
    fprintf(stderr,
            "postgres reports: %s",
            PQerrorMessage(PGCONN));
    diagnostics();
    if (++failureCount >= MAX_FAILURES) {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate) {
        PQfinish(PGCONN);
        if (iAmParent) {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed=1;
}


int
countTuples(char* table) {
    char    cmd[120];
    int     count;

    snprintf(cmd, 119, "select count(col_key) from %s", table);
    transactionBegin();
        PGRESULT=PQexec(PGCONN,cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("error counting tuples");
        count=atoi(PQgetvalue(PGRESULT, 0, 0));
    transactionCommit();
    return count;
}


void
createIndexBtree(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create index %s on %s using btree (%s)",
        iName, tName, fields);
    transactionBegin();
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
    transactionCommit();
}


void
createIndexCluster(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create unique index %s on %s using btree (%s)",
        iName, tName, fields);
    transactionBegin();
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
        PQclear(PGRESULT);
        sprintf(cmd, "cluster %s on %s", iName, tName );
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("cluster error"); 
    transactionCommit();
}


void
createIndexForeign(char* tName, char* keyCol,
                        char* fTable, char* fFields) {
    sprintf(cmd,
        "alter table %s add foreign key (%s) references %s (%s)",
        tName, keyCol, fTable, fFields);
    transactionBegin(); 
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("foreign key error"); 
    transactionCommit();
}


void
createIndexHash(char* iName, char* tName, char* fields) {
    sprintf(cmd,
        "create index %s on %s using hash (%s)",
        iName, tName, fields);
    transactionBegin();
        PGRESULT=PQexec(PGCONN, cmd);
        interpretPGresult();
        if (OSDB_ERROR.error)
            BUGOUT("hash error"); 
    transactionCommit();
}


void
createTable(char* stg) {
    ddl(stg);
}


void
cursorClose() {
    PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "close MYCURSOR");
    interpretPGresult();
    if (OSDB_ERROR.error)
        TESTFAILED("cursor close");
}


void
cursorDeclare(char* stg) {
    sprintf(cmd, "declare MYCURSOR cursor for %s", stg);
    PGRESULT=PQexec(PGCONN, cmd);
    interpretPGresult(); 
    if (OSDB_ERROR.error)
        TESTFAILED("cursor close");
}


void
cursorFetch(char *dest) {
    int     i;

    if (PGRESULT!=NULL)
        PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "fetch MYCURSOR");
    interpretPGresult();
    switch (OSDB_ERROR.error) {
        case ERR_OK:        break;
        case ERR_NOTFOUND:  return; 
        default:            TESTFAILED("fetch error");
    }
    if (PQntuples(PGRESULT)!=1)
        TESTFAILED("fetched too many tuples");
    strncpy(dest,PQgetvalue(PGRESULT,0,0), MAX_TUPLE_LENGTH);
    for (i=1; i<PQnfields(PGRESULT); i++) {
        strncat(dest, (char*)" ", MAX_TUPLE_LENGTH);
        strncat(dest, PQgetvalue(PGRESULT,0,i), MAX_TUPLE_LENGTH);
    }
}


void
cursorOpen() {
    /* no-op under Postgres */
}

void
databaseConnect() {
    sprintf(DB_CONNECT_CMD, "dbname=%s", DBNAME);
    PGCONN=PQconnectdb(DB_CONNECT_CMD);
    if (PQstatus(PGCONN)==CONNECTION_BAD)
        BUGOUT(DB_CONNECT_CMD);
}

void
databaseCreate(char* dName) {
    char    *args[3];
    OSDB_RUN *pgm;

    fprintf(stderr, "Ignore message if \"drobdb\" fails\n");
    args[0]=(char*)"dropdb";
    args[1]=dName;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
    args[0]=(char*)"createdb";
    args[1]=dName;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
}


void
databaseDisconnect() {
    PQfinish(PGCONN);
}


void
ddl(char* stg) {
    PGRESULT=PQexec(PGCONN,  stg);
    interpretPGresult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DDL;
}


void
dml(char* stg) {
    PGRESULT=PQexec(PGCONN, stg);
    interpretPGresult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DML;
}


void
interpretPGresult() {
    if (PGRESULT==NULL)
        TESTFAILED("interpretPGresult error");
    switch (PQresultStatus(PGRESULT)) {
        case PGRES_COMMAND_OK:  OSDB_ERROR.error=ERR_OK;
                                break;
        case PGRES_TUPLES_OK:   if (PQntuples(PGRESULT)) {
                                    OSDB_ERROR.error=ERR_OK;
                                } else {
                                    OSDB_ERROR.error=ERR_NOTFOUND;
                                }
                                break;
        case PGRES_BAD_RESPONSE:
        case PGRES_NONFATAL_ERROR:
        case PGRES_FATAL_ERROR: OSDB_ERROR.error=ERR_UNKNOWN;
                                break;
        case PGRES_COPY_OUT:
        case PGRES_COPY_IN:
        case PGRES_EMPTY_QUERY:
        default:                OSDB_ERROR.error=ERR_UNKNOWN;
                                TESTFAILED(PQresultErrorMessage(PGRESULT));
    }
}


int
load() {
    char    cmd[256];

    snprintf(cmd, 255,
        "copy updates from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UPDATES);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy hundred from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_HUNDRED);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy tenpct from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TENPCT);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy uniques from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UNIQUES);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    snprintf(cmd, 255,
        "copy tiny from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TINY);
    dml(cmd);
    if (OSDB_ERROR.error) {
        BUGOUT("cmd");
    }
    return 0;
}

void
setDefaults() {
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=NULL;
    DBNAME="osdb";
    DBPORT=0;
}


void
transactionBegin() {
    PGRESULT=PQexec(PGCONN, "begin");
    interpretPGresult(); 
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
    PQclear(PGRESULT);
}


void
transactionCommit() {
    PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "commit");
    interpretPGresult(); 
    PQclear(PGRESULT);
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}


void
transactionRollback() {
    PQclear(PGRESULT);
    PGRESULT=PQexec(PGCONN, "rollback");
    interpretPGresult(); 
    PQclear(PGRESULT);
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}

