/***************************************************************************
                  oracle-call.c  -  Oracle calls for OSDB/callable-sql
                     -------------------
    begin        : Justin Sabelko and Paul Wagner, 
                   University of Wisconsin - Eau Clalire
    copyright    : 
    email        : sabelkjc@uwec.edu / wagnerpj@uwec.edu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$LOG: osdb-ora.m4,v $

Revision 1.3  2004/08/03 12:42:12  wagnerpj
Modified format to mimic osdb-my.m4
Cleaned up code

Revision 1.2  2004/07/15 13:12:31  wagnerpj
Switched to m4 file, but encountered link problems

Revision 1.1  2003/10/15 20:11:46  wagnerpj
Completed data loading, fixed several bugs in osdb-ora.c

Revision 1.0  2003/09/01 15:32:10  sabelkjc and wagnerpj
Implement basic structure as osdb-ora.c

Revision 0.1  2003/03/15 00:00:00  sabelkjc and wagnerpj
Tested basic structure of data loading and executing SQL through OCI
*/

#include "callable-sql.h"
#include "osdb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <oci.h>

m4_changequote([,])
m4_changequote(`,~)

/* MySQL doesn't do BTREE or HASH */
m4_define(`BTREE~,`($1)~)m4_dnl
m4_define(`HASH~,`($1)~)m4_dnl
m4_define(`CLUSTER~,`~)m4_dnl
m4_define(`SQL_DATE~, $1)m4_dnl
m4_define(`SQL_DATE~,
        `qvPtr+=sprintf(qvPtr, "%s %s", comma, $1);~)m4_dnl 

m4_define(`USE_INTO_TABLE~, `into table $1~)m4_dnl
m4_define(`USE_INTO_TEMP~, `~)m4_dnl

/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    char(20) $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    real $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`DROP_INDEX~, `ddl("alter table $1 drop index $2");~)

m4_include(`../callable-sql.m4~)
m4_include(`../../the-tests.m4~)


#define MAX_PARAMS 15                     
				// maximum number of parameters in result

static text *username = (text *) "OSDB";  // database username
static text *password = (text *) "OSDB"; // database password

OCIEnv *envhp;                            // environment handle
OCIError *errhp;                          // error handle

OCIServer *srvhp;                         // server handle
OCISvcCtx *svchp;                         // service context handle
OCISession *authp = (OCISession *) 0;     // session handle
OCIStmt *stmtp;                           // statement handle
OCIParam *paramhp;                        // parameter handle

ub4 numOfParams;                          // number of parameters in SQL result
ub2   params_dtype[MAX_PARAMS];           // data type of params

sword params_num[MAX_PARAMS];             // number params
float params_float[MAX_PARAMS];           // float params
text * params_text[MAX_PARAMS];           // char array params
double params_double[MAX_PARAMS];         // double/signed params

// -- function declarations
void checkerr(OCIError* errp, sword status);
ub4 countParams(OCIError* errhp, OCIStmt* stmthp);

// --- function bodies
int
argument_check(char* arg)
{
  return 0;
}

void
argument_choices() {
}

void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
}

void checkerr(OCIError *errp, sword status)
{
  text errbuf[512];
  sb4 errcode = 0;

  switch (status)
  {
  case OCI_SUCCESS:
    OSDB_ERROR.error=ERR_OK;
    break;
  case OCI_SUCCESS_WITH_INFO:
    (void) printf("Error - OCI_SUCCESS_WITH_INFO\n");
    break;
  case OCI_NEED_DATA:
    (void) printf("Error - OCI_NEED_DATA\n");
    break;
  case OCI_NO_DATA:
    OSDB_ERROR.error=ERR_OK;
    break;
  case OCI_ERROR:
    (void) OCIErrorGet((dvoid *)errp, (ub4) 1, (text *) NULL, &errcode,
                        errbuf, (ub4) sizeof(errbuf), OCI_HTYPE_ERROR);
    (void) printf("Error - %.*s\n", 512, errbuf);
    break;
  case OCI_INVALID_HANDLE:
    (void) printf("Error - OCI_INVALID_HANDLE\n");
    break;
  case OCI_STILL_EXECUTING:
    (void) printf("Error - OCI_STILL_EXECUTE\n");
    break;
  case OCI_CONTINUE:
    (void) printf("Error - OCI_CONTINUE\n");
    break;
  default:
    break;
  }
}


ub4 countParams(OCIError* errhp, OCIStmt* stmthp) {
  OCIParam     *mypard;
  ub4          counter;
  ub2          dtype;
  text         *col_name;
  ub4          col_name_len;
  ub4          dlen;
  sb4          parm_status;

  OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &mypard, OCI_DTYPE_PARAM,
		  (size_t) 0, (dvoid **) 0);

  /* Request a parameter descriptor for position 1 in the select-list */
  counter = 1;
  parm_status = OCIParamGet(stmthp, OCI_HTYPE_STMT, errhp, (dvoid **) &mypard,
			    (ub4) counter);
  /* Loop only if a descriptor was successfully retrieved for
     current position, starting at 1 */
  while (parm_status==OCI_SUCCESS) {
    /* Retrieve the data type attribute */
    checkerr(errhp, OCIAttrGet((dvoid*) mypard, (ub4) OCI_DTYPE_PARAM, 
			       (dvoid*) &dtype,(ub4 *) 0, 
                               (ub4) OCI_ATTR_DATA_TYPE, 
			       (OCIError *) errhp  ));
    /* Retrieve the column name attribute */
    checkerr(errhp, OCIAttrGet((dvoid*) mypard, (ub4) OCI_DTYPE_PARAM, 
			       (dvoid**) &col_name,(ub4 *) &col_name_len, 
                               (ub4) OCI_ATTR_NAME, 
			       (OCIError *) errhp ));
    /* Retrieve the size of the data */
    checkerr(errhp, OCIAttrGet((dvoid*) mypard, (ub4) OCI_DTYPE_PARAM, 
			       (dvoid*) &dlen, (ub4 *) 0, 
                               (ub4) OCI_ATTR_DATA_SIZE, 
			       (OCIError *) errhp  ));
    printf("column=%s  datatype=%d  datasize=%d\n\n", col_name, dtype, dlen);
    fflush(stdout);
    /* increment counter and get next descriptor, if there is one */
    counter++;
    parm_status = OCIParamGet(stmthp, OCI_HTYPE_STMT, errhp, 
                              (dvoid **) &mypard,
			      (ub4) counter);
  }
  return counter;
}

// --- NOTE: check if this (b/b+-tree) or hash is the default under Oracle
void
createIndexBtree(char* iName, char* tName, char* fields) {
  char cmd[200];
  sprintf(cmd, "create index %s on %s (%s)",
        iName, tName, fields);
  transactionBegin();
    ddl(cmd);
  transactionCommit();
}


// --- PG: create a unique index
// --- NOTE: Oracle supports index on cluster, but only if table created
//            as a cluster - do this instead?
void
createIndexCluster(char* iName, char* tName, char* fields) {
  char cmd[200];
  sprintf(cmd, "create unique index %s on %s (%s)",
        iName, tName, fields);
  transactionBegin();
    ddl(cmd);
  transactionCommit();
}


// --- PG: create a foreign key
// --- NOTE: need to implement
void
createIndexForeign(char* tName, char* keyName, char* keyCol,
		   char* fTable, char* fFields) {
}


// --- PG: if available, create hash index; if not, create regular index
// --- NOTE: need to implement
void
createIndexHash(char* iName, char* tName, char* fields) {
}


// --- create_idx_hundred_foreign 
// --- NOTE: copied in from mysql code for now to satisfy the linker
int
create_idx_hundred_foreign() {
    if (doIndexes) {  /* Pointless without index on updates.key */
        createIndexForeign("hundred", "fk_hundred_updates", "col_signed",
                            "updates", "col_key");
    }
    return 0;
}

void
createTable(char* stg) {
  ddl(stg);
}


void
cursorClose() {
  /* no-op for now */
  // temporary fix
  OSDB_ERROR.error=ERR_OK; 
}


void
cursorDeclare(char* stg) {
  ub4 paramLen;               // length of current parameter
  OCIDefine* defnpArray[MAX_PARAMS]; // parameter definition handle
  sword status;               // status holder for position definition
  ub2 dtype;                  // current parameter data type
  text *col_name;             // current parameter column name
  ub4 col_name_len;           // current parameter column name length
  sword parm_status;          // status holder for parameter
  int index;                  // loop index


  /* --- allocate the statement handle */
  checkerr(errhp, OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &stmtp, 
                  OCI_HTYPE_STMT,
		  (size_t) 0, (dvoid **) 0));

  /* // look into setting prefetch rows for efficiency later
  checkerr(errhp, OCIAttrSet( (dvoid *)stmtp, OCI_HTYPE_STMT,
                                OCI_ATTR_PREFETCH_ROWS, xx   )
  */

  /* --- prepare the statement */
  checkerr(errhp, OCIStmtPrepare(stmtp, errhp, stg, (ub4) strlen(stg),
				 (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT ));

  /* --- execute the statement */
  checkerr( errhp, OCIStmtExecute(svchp, stmtp, errhp, (ub4) 0, (ub4) 0, 
		 (CONST OCISnapshot *) NULL, (OCISnapshot *) NULL,
		 OCI_DEFAULT));

  /* --- get the parameters (counting starts at 1, not 0) */
  // --- initialize parameter definition handle array
  for (index = 0; index < MAX_PARAMS; index++)
  {
    defnpArray[index] = NULL;
  }
  // --- get each parameter
  numOfParams = 1;
  parm_status = OCIParamGet(stmtp, OCI_HTYPE_STMT, errhp, (dvoid *) &paramhp,
                            (ub4) numOfParams);
  checkerr(errhp, parm_status);  // NOTE: should do this for each param???
  /* --- loop only if a descriptor was successfully retrieved for
     current position, starting at 1 */
  while (parm_status==OCI_SUCCESS)
  {
    // --- retrieve the parameter length in bytes
    checkerr(errhp, OCIAttrGet((dvoid*) paramhp, (ub4) OCI_DTYPE_PARAM, 
			       (dvoid*) &paramLen, (ub4 *) 0, 
			       (ub4) OCI_ATTR_DATA_SIZE,
			       (OCIError *) errhp  ));
    paramLen = (short)paramLen;  // values coming out large; bring back into
                                 //    correct range

    // --- retrieve the data type attribute
    checkerr(errhp, OCIAttrGet((dvoid*) paramhp, (ub4) OCI_DTYPE_PARAM, 
			       (dvoid*) &dtype,(ub4 *) 0, 
			       (ub4) OCI_ATTR_DATA_TYPE, 
			       (OCIError *) errhp  ));
    params_dtype[numOfParams] = dtype;

    // --- retrieve the column name attribute
    checkerr(errhp, OCIAttrGet((dvoid*) paramhp, (ub4) OCI_DTYPE_PARAM, 
			       (dvoid**) &col_name,(ub4 *) &col_name_len, 
			       (ub4) OCI_ATTR_NAME, 
			       (OCIError *) errhp ));

    /* --- define the position variable */
    switch (dtype)
    {
      case SQLT_CHR:       // 1 - VARCHAR2 (char[n])
        params_text[numOfParams] = (text *) malloc((int) paramLen + 1);

        if (status = OCIDefineByPos(stmtp, &(defnpArray[numOfParams]), errhp,
				    numOfParams, 
				    params_text[numOfParams], 
                                    paramLen+1, SQLT_CHR, (dvoid *) 0,
				    (ub2 *) 0, (ub2 *) 0, OCI_DEFAULT))
        {
          checkerr(errhp, status);
        }
        break;

      case SQLT_NUM:       // 2 - NUMBER  // work for both ints & fixed point
	                                  // if store in doubles
	if (status = OCIDefineByPos(stmtp, &(defnpArray[numOfParams]), 
				    errhp, numOfParams, 
				    (dvoid *)&(params_double[numOfParams]),
				    sizeof(double), SQLT_NUM, (dvoid *) 0,
				    (ub2 *) 0, (ub2 *) 0, OCI_DEFAULT))
        {
	  checkerr(errhp, status);
	}
	break;

      case SQLT_FLT:       // 4 - FLOAT - store in double
        if (status = OCIDefineByPos(stmtp, &(defnpArray[numOfParams]), errhp,
				    numOfParams, 
				    (dvoid *) &(params_double[numOfParams]), 
                                    sizeof(double), SQLT_FLT, (dvoid *) 0,
				    (ub2 *) 0, (ub2 *) 0, OCI_DEFAULT))
        {
          checkerr(errhp, status);
        }
        break;

      case SQLT_AFC:       // 96 - CHAR (char[n])
        params_text[numOfParams] = (text *) malloc((int) paramLen + 1);

        if (status = OCIDefineByPos(stmtp, &(defnpArray[numOfParams]), errhp,
				    numOfParams, 
				    params_text[numOfParams], 
                                    paramLen+1, SQLT_AFC, (dvoid *) 0,
				    (ub2 *) 0, (ub2 *) 0, OCI_DEFAULT))
        {
          checkerr(errhp, status);
        }
        break;

      default:
        printf("Error in result parameter - unknown data type\n");
    }

    // --- get the next parameter if possible
    numOfParams++;
    parm_status = OCIParamGet(stmtp, OCI_HTYPE_STMT, errhp, (dvoid *) &paramhp,
                              (ub4) numOfParams);
    if (parm_status == OCI_SUCCESS)
       checkerr(errhp, parm_status);  // not sure should do this for each param

  }  // end - while
  numOfParams--;
  OSDB_ERROR.error=ERR_OK;
}  // end - cursorDeclare


void
cursorFetch(char *dest) {
/* NOTE - need to change:
   fetch row from cursor
   initialize dest
   for each parameter
      get value from correct position in appropriate parallel array
      convert to string if needed
      concat to dest
*/
  sword status;
  char dummy;

  status = OCIStmtFetch2(stmtp, errhp, (ub4) 1,
	     OCI_FETCH_NEXT, (sb4) 0, OCI_DEFAULT);
  checkerr(errhp, status);
  if (status == OCI_SUCCESS)
  {
    OSDB_ERROR.error=ERR_OK;
  }
  else if (status == OCI_NO_DATA)
  {
    OSDB_ERROR.error=ERR_NOTFOUND;	// need this to avoid cursor problem
  }
  else
  {
    printf("cursorFetch - other error\n");
    OSDB_ERROR.error=ERR_UNKNOWN;
  }

  // need to put query result into dest
}


void
cursorOpen() {
  /* no-op for now */
}


void
databaseConnect() {
  /* initialize environment */
  OCIInitialize((ub4) OCI_DEFAULT, (dvoid *)0,
		(dvoid * (*)(dvoid *, size_t)) 0,
		(dvoid * (*)(dvoid *, dvoid *, size_t))0,
		(void (*)(dvoid *, dvoid *)) 0 );

  OCIEnvInit( (OCIEnv **) &envhp, OCI_DEFAULT, (size_t) 0,
	      (dvoid **) 0 );

  /* get error handler */
  OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &errhp, OCI_HTYPE_ERROR,
		  (size_t) 0, (dvoid **) 0);

  /* server contexts */
  OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &srvhp, OCI_HTYPE_SERVER,
		  (size_t) 0, (dvoid **) 0);

  OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &svchp, OCI_HTYPE_SVCCTX,
		  (size_t) 0, (dvoid **) 0);

  /* connect to default host */
  OCIServerAttach( srvhp, errhp, (text *)"", strlen(""), 0);

  /* set attribute server context in the service context */
  OCIAttrSet( (dvoid *) svchp, OCI_HTYPE_SVCCTX, (dvoid *)srvhp,
	      (ub4) 0, OCI_ATTR_SERVER, (OCIError *) errhp);

  /* set authorization */
  OCIHandleAlloc((dvoid *) envhp, (dvoid **)&authp,
		 (ub4) OCI_HTYPE_SESSION, (size_t) 0, (dvoid **) 0);

  OCIAttrSet((dvoid *) authp, (ub4) OCI_HTYPE_SESSION,
	     (dvoid *) username, (ub4) strlen((char *)username),
	     (ub4) OCI_ATTR_USERNAME, errhp);

  OCIAttrSet((dvoid *) authp, (ub4) OCI_HTYPE_SESSION,
	     (dvoid *) password, (ub4) strlen((char *)password),
	     (ub4) OCI_ATTR_PASSWORD, errhp);

  /* authenticate user */
  checkerr(errhp, OCISessionBegin ( svchp,  errhp, authp, OCI_CRED_RDBMS,
				    (ub4) OCI_DEFAULT));

  OCIAttrSet((dvoid *) svchp, (ub4) OCI_HTYPE_SVCCTX,
	     (dvoid *) authp, (ub4) 0,
	     (ub4) OCI_ATTR_SESSION, errhp);
}   

void
databaseCreate(char* dName) {
  databaseConnect();

  // --- in Oracle don't drop entire DB, just tables
  transactionBegin();
  ddl("drop table hundred");
  ddl("drop table tenpct");
  ddl("drop table tiny");
  ddl("drop table uniques");
  ddl("drop table updates");
  transactionCommit();

  // --- 2) create new database
  /* no-op for now - no need to create a new database under Oracle
     assuming user schema already exists */

  databaseDisconnect();
}


void
databaseDisconnect() {
  OCISessionEnd ( svchp, errhp, authp, OCI_DEFAULT );

  OCIServerDetach ( srvhp, errhp, OCI_DEFAULT );

  if (envhp) {
    OCIHandleFree((dvoid *) envhp, OCI_HTYPE_ENV);
  }
}


void
ddl(char* stg) {
  /* NEEDS BETTER ERROR CHECKING(?) */
  checkerr(errhp, OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &stmtp, 
                  OCI_HTYPE_STMT,
		  (size_t) 0, (dvoid **) 0));

  checkerr(errhp, OCIStmtPrepare(stmtp, errhp, stg, (ub4) strlen(stg),
				 (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT ));

  OCIStmtExecute(svchp, stmtp, errhp, (ub4) 1, (ub4) 0, 
		 (CONST OCISnapshot *) NULL, (OCISnapshot *) NULL,
		 OCI_DEFAULT);
}


void
dml(char* stg) {
  /* NEEDS BETTER ERROR CHECKING */
  OCIHandleAlloc( (dvoid *) envhp, (dvoid **) &stmtp, OCI_HTYPE_STMT,
                  (size_t) 0, (dvoid **) 0);

  checkerr(errhp, OCIStmtPrepare(stmtp, errhp, stg, (ub4) strlen(stg),
                                 (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT ));

  OCIStmtExecute(svchp, stmtp, errhp, (ub4) 1, (ub4) 0,
                 (CONST OCISnapshot *) NULL, (OCISnapshot *) NULL,
                 OCI_DEFAULT);
}


int
load() {
  char cmd[256];

  // NOTE: should make path relative
  snprintf(cmd, 255,"/usr/local/osdb/data/oraload/oraload.sh %s %s", 
           username, password);
  system(cmd);
 
  return 0;
}


void
setDefaults() {
  osdblog=stdout;
}


void
transactionBegin() {
  /* no-op - Oracle maintains an implicit transaction */
}


void
transactionCommit() {
  checkerr( errhp, OCITransCommit(svchp, errhp, (ub4) 0));
}


void
transactionRollback() {
  OCITransRollback(svchp, errhp, (ub4) 0);
}
