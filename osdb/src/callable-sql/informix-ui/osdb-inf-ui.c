/***************************************************************************
                  osdb-inf-ui.c  -  Informix::dbaccess for OSDB/osdb-inf-ui
                     -------------------
    begin        : Fri Mar 17 2001
    copyright    : (C) 2001 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-inf-ui.c,v $
Revision 1.9  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.8  2002/03/05 03:30:21  ariebs
Add a createTable() call in preparation for allowing database-specific
syntax, e.g., mysql's "table=" options.

Revision 1.7  2001/09/09 07:00:26  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.6  2001/08/11 21:02:18  ariebs
Make all the cluster indexes unique (as they should have been --
Thanks Maciej!)

Revision 1.5  2001/04/13 15:46:20  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

Revision 1.4  2001/03/27 23:58:40  ariebs
Think I finally have dbaccess quirks mastered?

Revision 1.3  2001/03/27 00:30:37  ariebs
Getting closer! (Still fighting with dbaccess's multi-line output)

Revision 1.2  2001/03/20 02:53:45  ariebs
More changes for using Informix's dbaccess program

Revision 1.1  2001/03/18 02:40:33  ariebs
Add support for osdb-inf-ui

*/

#include "callable-sql.h"
#include "osdb.h"

int  getTuple();
void getTupleHeader();
int  getTupleHorizontal(char* iPtr, char* oPtr);
int  isEmpty(char *p);

#define CMDBUFLEN   1024
char        cmd[CMDBUFLEN],      /* frequently used, but very transient */
            *deferredTuple,
            *pBuffer,
            *pTuple,
            tupleReturned[PGO_BUFSIZE];
int         dataInColumns=0;    /* dbaccess returns data in multiple
                                 * columns if the combined field
                                 * widths are "too wide" to fit on a
                                 * line. We figure out what format to
                                 * expect in cursorOpen(), and use
                                 * that knowledge in getTuple().
                                 */
OSDB_RUN    *pgm;


int
argument_check(char* arg)
{
    return(FALSE);  /* we recognize no arguments here */
}

void
argument_choices() {
}


void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    diagnostics();
    if (++failureCount >= MAX_FAILURES) {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate) {
        if (iAmParent) {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed=1;
}


int
countTuples(char* table) {
    char    cmd[120];
    int     count;

    snprintf(cmd, 119, "select count(col_key) from %s", table);
    transactionBegin();
        cursorDeclare(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("error counting tuples");
        cursorOpen();
        if (OSDB_ERROR.error)
            BUGOUT("error counting tuples2");
        cursorFetch(tupleReturned);
        if (OSDB_ERROR.error)
            BUGOUT("error counting tuples3");
        count=atoi(tupleReturned);
    transactionCommit();
    return count;
}


void
createIndexBtree(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
    transactionCommit();
}


void
createIndexCluster(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create unique index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
/* dbaccess doesn't do clustered indexes
 *        snprintf(cmd, CMDBUFLEN, "cluster %s on %s", iName, tName );
 *        ddl(cmd);
 *        if (OSDB_ERROR.error)
 *            BUGOUT("cluster error"); 
*/
    transactionCommit();
}


void
createIndexForeign(char* tName, char* keyCol,
                        char* fTable, char* fFields) {
    snprintf(cmd, CMDBUFLEN,
        "alter table %s add foreign key (%s) references %s (%s)",
        tName, keyCol, fTable, fFields);
    transactionBegin(); 
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("foreign key error"); 
    transactionCommit();
}


void
createIndexHash(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("hash error"); 
    transactionCommit();
}


void
createTable(char* stg) {
    ddl(stg);
}


void
cursorClose() {
    /* dbaccess don't know no close */
}


void
cursorDeclare(char* stg) {
    programFlushOutput(pgm);
    dml(stg);
    if (OSDB_ERROR.error)
        TESTFAILED("declare failed");
    
}


void
cursorFetch(char *dest) {
    getTuple();
    if (watchAll)
        fprintf(stderr, "cuFe> %s", tupleReturned);
}


void
cursorOpen() {
    /* no-op under Informix; dbaccess rejects "open" */
    dataInColumns=0;
    getTupleHeader();     /* filter out the header lines */
    if (OSDB_ERROR.error) {
        TESTFAILED("cursor open failed");
        return;
    }
    if (watch)
        printf("cuOp> %s\n", tupleReturned);
}

void
databaseConnect() {
    char    *args[4];

    args[0]=(char*)"dbaccess";
    args[1]=DBNAME;
    args[2]=(char*)"-";
    args[3]=NULL;
    pgm=programStartPipe(args);
}

void
databaseCreate(char* dName) {
    char    *args[4],
            *cmds[2];

    args[0]=(char*)"dbaccess";
    args[1]=(char*)"-";
    args[2]=(char*)"-";
    args[3]=NULL;
    cmds[0]=(char*)"drop database osdb;\n";
    cmds[1]=NULL;
    runProg(args, cmds);
    cmds[0]=(char*)"create database osdb;\n";
    runProg(args, cmds);
}


void
databaseDisconnect() {
    programEnd(pgm, NULL);
}


void
ddl(char* stg) {
    OSDB_ERROR.error=ERR_OK;
    programFeed2(pgm, stg, (char*)";\n");
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DDL;
}


void
dml(char* stg) {
    OSDB_ERROR.error=ERR_OK;
    programFeed2(pgm, stg, (char*)";\n");
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DML;
}


char*
getProgramOutputLine(char *lineBuffer) {

    char    *lb;

    *lineBuffer=0;
    lb=lineBuffer;
    if (pBuffer==NULL) 
        pBuffer=programGetOutput(pgm);
    while (pBuffer!=NULL) {
        switch (*lb++=*pBuffer++) {
          case '\n':    *lb=0;
                        if (watch)
                            printf("lb> %s", lineBuffer);
                        return lineBuffer;

          case 0:       lb--; /* back over the NULL */
                        pBuffer=programGetOutput(pgm);
        }
    }
    return lineBuffer;
}


/* GetTuple()
 *
 * getTuple() returns a line of non-blank data, or an error.
 * It is complicated by the fact that it may have to reformat
 * results that look like
 *
 *  col_key     1000
 *  col_name    zzXXADFh
 *  col_int     1000
 *
 *  col_key      1001
 *  col_name    DADFA34havW
 *  col_int     1001
 *
 * into a stream that looks like
 *
 *  1000 zzXXADFh 1000
 *  1001 DADFA34havW 1001
*/

int
getTuple() {
    char        inputBuffer[MAX_TUPLE_LENGTH],
                *iPtr;
    if (deferredTuple) {     /* If called after getTupleHeader(), and
                             * we had to reformat the header line...
                             */
        iPtr=deferredTuple;
        deferredTuple=NULL;
    } else {
        iPtr=getProgramOutputLine(inputBuffer);
    }
    while (isEmpty(iPtr)) {
        if ((iPtr=getProgramOutputLine(inputBuffer))==NULL) {
            BUGOUT("unexpected EOD in getTuple");
            break;
        }
    }
    while (isblank(*iPtr)) {
        iPtr++;
    }
    if (dataInColumns && (strncmp(iPtr, "col_", 4)==0)) {
        getTupleHorizontal(iPtr,tupleReturned);
        return 0;
    }
    strcpy(tupleReturned, iPtr);
    /* Check for "n row(s) retrieved." */
    while (isdigit(*iPtr))
        iPtr++;
    while (isblank(*iPtr))
        iPtr++;
    if (strncmp(iPtr, "row(s) retrieved.",17)==0) {
        OSDB_ERROR.error=ERR_NOTFOUND; /* signal EOD */
    } else {
        if (strncmp(iPtr, "No rows found.", 14)==0) {
            OSDB_ERROR.error=ERR_NOTFOUND; /* signal EOD */
        }
    }
    return 0;
}

void
getTupleHeader() {
    static char    inputBuffer[MAX_TUPLE_LENGTH];
    char           tmp[MAX_TUPLE_LENGTH],
                   *tup;
    int            iTmp;

    while (1) {
        tup=getProgramOutputLine(inputBuffer);
        while (isEmpty(tup)) {
            if ((tup=getProgramOutputLine(inputBuffer))==NULL) {
                BUGOUT("unexpected EOD in getTupleHeader");
                break;
            }
        }
    while (isblank(*tup)) {
        tup++;
    }

        /* Here's where dbaccess gets goofy. We're looking at 3 clues
         * into the format of the data to be returned. We might see
         * something like...
         *
         * 1. Header lines may start with "(" for aggregates,
         *    or "col_" for non-aggregates.
         *
         *      (count)
         *      1000
         *
         * 2. "Short" tuples are returned in rows (dataInColumns=0)
         *
         *      col_key     col_name
         *      1000        ZSffdfdaf9hadfadf9y32WW
         *      1001        afhADe3dhhADF13hsafdXxQdfd
         *
         * 3. "Long" tuples are returned in columns (dataInColumns=1)
         *
         *      col_key     2324
         *      col_name    Zzd9dadfh;ADAadQEAF
         *      col_float   790786343.434
         *
         *      col_key     2325
         *      col_name    dfoqn#!@%dfhaADADyadf
         *      col_float   861251453.978
         */

        if ((sscanf(tup, "col_%s col_%s", tmp, tmp)==2) ||
            (sscanf(tup, "(%s) (%s)", tmp, tmp)==2)) { /* short? */
            dataInColumns=0;
            strcpy(tupleReturned,tup); 
            return;
        }
        if ((sscanf(tup, "col_%s %s", tmp, tmp)==2) ||
            (sscanf(tup, "(%s) %s", tmp, tmp)==2)) { /* long? */
            dataInColumns=1;
            getTupleHorizontal(tup, inputBuffer);
            deferredTuple=inputBuffer;
            strcpy(tupleReturned, "<simulated header line>\n");
            return;
        }
        if ((strncmp(tup, "col_", 4)==0) ||
            (*tup=='(')) {       /* single datum per line? */
            dataInColumns=0;
            strcpy(tupleReturned,tup); 
            return;
        } 
        if (strncmp(tup, "No rows found.", 14)==0) { /* defer msg */
            strcpy(inputBuffer, tup);
            deferredTuple=inputBuffer;
            strcpy(tupleReturned, "<simulated header, EOF follows>\n");
            return;
        }
        if ((strncmp(tup, "Error", 5)==0) ||  /* error message? */
                (strncmp(tup, "Unknown", 6)==0)) {
            OSDB_ERROR.error=ERR_UNKNOWN;
            return;
        }
        if (sscanf(tup, "%d: %s", &iTmp, tmp)==2) {
                /* Some errors are like "  203: something happened" */
            OSDB_ERROR.error=ERR_UNKNOWN;
            return;
        }
    }
}


int
getTupleHorizontal(char* iPtr, char* oPtr) {
    char    inputBuffer[MAX_TUPLE_LENGTH];

    while (!isEmpty(iPtr)) {
        while (isgraph(*iPtr))
            iPtr++;
        while (*iPtr) {
            switch (*oPtr++=*iPtr++) {
              case '\n':    oPtr--;     /* Discard the newline */
            }
        }
        if ((iPtr=getProgramOutputLine(inputBuffer))==NULL) {
            BUGOUT("unexpected EOD in getTuple(2)");
            break;
        }
        while (isblank(*iPtr))
            iPtr++;
    }
    return 0;
}


int
isEmpty(char *p) {
    while (isspace(*p++));
    return (*--p==0);
}


int
load() {
    char    *args[4],
            *cmds[7],
            Scmds[5][256];
    int     i;

    snprintf(Scmds[0], 255,
        "load from '%s/%s' insert into updates;\n",
        dataDir, FILENAME_UPDATES);
    snprintf(Scmds[1], 255,
        "load from '%s/%s' insert into hundred;\n",
        dataDir, FILENAME_HUNDRED);
    snprintf(Scmds[2], 255,
        "load from '%s/%s' insert into tenpct;\n",
        dataDir, FILENAME_TENPCT);
    snprintf(Scmds[3], 255,
        "load from '%s/%s' insert into uniques;\n",
        dataDir, FILENAME_UNIQUES);
    snprintf(Scmds[4], 255,
        "load from '%s/%s' insert into tiny;\n",
        dataDir, FILENAME_TINY);

    for (i=0; i<5; i++) {
        cmds[i]=Scmds[i];
    }
    cmds[5]=NULL;
    args[0]=(char*)"dbaccess";
    args[1]=(char*)"osdb";
    args[2]=(char*)"-";
    args[3]=NULL;
    return runProg(args, cmds);
}

void
setDefaults() {
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=NULL;
    DBNAME="osdb";
    DBPORT=0;
    OSDB_ERROR.error=ERR_OK;
    OSDB_ERROR.text=(char*)"no error text";
    pBuffer=NULL;
    tupleReturned[0]=0;
    if (getenv("DBDELIMITER")==NULL)
        putenv("DBDELIMITER=,");
}


void
transactionBegin() {
    /* no "begin" in dbaccess */
    programFlushOutput(pgm);   /* start with a clean slate */
}


void
transactionCommit() {
    /* no "commit" in dbaccess */
}


void
transactionRollback() {
    /* no "rollback" in dbaccess */
}

