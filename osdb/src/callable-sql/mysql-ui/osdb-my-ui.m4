/***************************************************************************
                  mysql-ui.c  -  MySQL calls for OSDB/mysql-ui
                     -------------------
    begin        : Fri Dec 1 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-my-ui.m4,v $
Revision 1.4  2004/10/11 22:17:44  ariebs
Make the help message helpful!

Revision 1.3  2003/05/04 18:14:39  ariebs
More changes to incorporate the-tests.m4 across the board.

Revision 1.2  2003/04/22 10:19:02  ariebs
Add DROP_INDEX macro to accomodate differing syntax between databases.
(Note that SQL92 appears not to have defined standard language for this.)

Revision 1.1  2003/03/05 04:02:54  ariebs
Make the changes necessary to make the MySQL tests work with the-tests.m4;
note that the new stuff doesn't work yet, but it does compile cleanly.

Revision 1.15  2002/03/12 03:58:35  ariebs
Clean up the usage() message formatting.

Revision 1.14  2002/03/10 20:16:47  ariebs
Implement the --mysql={innodb | bdb} option for the MySQL tests.

Revision 1.13  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.12  2002/03/05 03:30:21  ariebs
Add a createTable() call in preparation for allowing database-specific
syntax, e.g., mysql's "table=" options.

Revision 1.11  2002/02/17 13:28:10  ariebs
A variety of changes to remove forkpty() from the code.

Revision 1.10  2001/09/09 07:00:26  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.9  2001/08/16 02:22:06  ariebs
Modify the MySQL tests to use the new foreign key syntax, and update
the "expected" results

Revision 1.8  2001/08/15 01:07:35  ariebs
Apply Maciej's fixes for foreign key support

Revision 1.7  2001/08/11 21:02:18  ariebs
Make all the cluster indexes unique (as they should have been --
Thanks Maciej!)

Revision 1.6  2001/04/13 15:46:20  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

Revision 1.5  2001/02/23 20:38:27  ariebs
Cosmetic changes to
(a) Use forkpty() (so I added LDFLAGS to all the makefiles while I was
    at it, and
(b) Add "diagnostics()" to osdb.c (which perturbed most programs)

Revision 1.3  2001/02/21 03:40:50  ariebs
Make programStartPipe() and osdb-my-ui work better together (but still a
couple of problems to go)

Revision 1.2  2001/02/18 14:30:31  ariebs
Implement programStartPty() for MySQL, and eliminate my hideous patch
for the mysql program. Also renamed "programStart()" to
"programStartPipe()"


*/

#include "callable-sql.h"
#include "osdb.h"

m4_changequote([,])
m4_changequote(`,~)

m4_define(`BTREE~,`using btree($*)~)m4_dnl
m4_define(`HASH~,`using hash($*)~)m4_dnl
m4_define(`CLUSTER~,`~)m4_dnl
m4_define(`SQL_DATE~, $1)m4_dnl
m4_define(`SQL_DATE~,
        `qvPtr+=sprintf(qvPtr, "%s %s", comma, $1);~)m4_dnl

m4_define(`USE_INTO_TABLE~, `into table $1~)m4_dnl
m4_define(`USE_INTO_TEMP~, `~)m4_dnl


/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    char(20) $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float4 $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`DROP_INDEX~, `ddl("alter table $1 drop index $2");~)

m4_include(`../callable-sql.m4~)
m4_include(`../../the-tests.m4~)


int  getTuple();
void interpretResult();

#define CMDBUFLEN   2048
char        cmd[CMDBUFLEN],      /* frequently used, but very transient */
            *pBuffer,
            *pTuple;
OSDB_RUN    *pgm; 
int         TABLE_MODE = 0;
char        *TABLE_MODE_NAMES[]={"normal", "innodb", "bdb", "\0"};


int
argument_check(char* arg)
{
    int     i;
    char*   p;

    if (strncmp(arg, "--mysql=", 8)==0) {
        if (TABLE_MODE) {
            fprintf(stderr, "\nOnly one MySQL mode may be specified\n");
            return(FALSE);
        }
        p=arg+8;
        for (i=0; ; i++) {
            if (TABLE_MODE_NAMES[i][0]==0) {
                fprintf(stderr, "\nUnknown mode for --mysql\n");
                return(FALSE);
            }
            if (strncmp(p, TABLE_MODE_NAMES[i], strlen(TABLE_MODE_NAMES[i]))==0) {
                TABLE_MODE=i;
                return(TRUE);
            }
        }
    }
    return(FALSE);
}

void
argument_choices() {
}
    int     i;

    printf("\n  MySQL-specific option:\n"
"    [--mysql=<dbtype>]  Use the <dbtype> engine, where <dbtype> can\n"
"                        one of"
);
    for (i=0; ; i++ ) {
        if (TABLE_MODE_NAMES[i][0]==0) {
            break;
        }
        printf(" %s", TABLE_MODE_NAMES[i]);
    }
    printf("\n");



void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    diagnostics();
    if (++failureCount >= MAX_FAILURES) {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate) {
        if (iAmParent) {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed=1;
}


void
createIndexBtree(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            TESTFAILED("btree error");
    transactionCommit();
}


void
createIndexCluster(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create unique index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            TESTFAILED("btree cluster error");
    transactionCommit();
}


void
createIndexForeign(char* tName, char* keyName, char* keyCol,
                        char* fTable, char* fFields) {
    snprintf(cmd, CMDBUFLEN,
        "alter table %s add constraint %s foreign key (%s) references %s (%s)",
        tName, keyName, keyCol, fTable, fFields);
    transactionBegin(); 
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("foreign key error"); 
    transactionCommit();
}


void
createIndexHash(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create index %s on %s (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            TESTFAILED("hash error"); 
    transactionCommit();
}


int
create_idx_hundred_foreign() {
    if (doIndexes) {  /* Pointless without index on updates.key */
        createIndexForeign("hundred", "fk_hundred_updates", "col_signed",
                            "updates", "col_key");
    }
    return 0;
}



void
createTable(char* stg) {
    strncpy(cmd, stg, CMDBUFLEN);
    if (TABLE_MODE) {
        switch (TABLE_MODE) {
          case 1:   strncat(cmd, " type=innodb", CMDBUFLEN);
                    break;
          case 2:   strncat(cmd, " type=bdb", CMDBUFLEN); }
    }
    ddl(cmd);
}


void
cursorClose() {
            /* mysql just doesn't do cursors, open, declare, or close
            */
}


void
cursorDeclare(char* stg) {
    dml(stg);
    /* return error status from dml() */
}


void
cursorFetch(char *dest) {
    getTuple();
    if ((*tupleReturned==0)) {  /* found end of data */
        OSDB_ERROR.error=ERR_NOTFOUND;
        return;
    }
    OSDB_ERROR.error=ERR_OK;
}


void
cursorOpen() {
    /* no cursors to open! */
}


void
databaseConnect() {
    char    *args[7];

    args[0]=(char*)"mysql";
    args[1]=(char*)"--force";
    args[2]=(char*)"--unbuffered";
    args[3]=(char*)"--user";
    args[4]=DBUSER;
    args[5]=DBNAME;
    args[6]=NULL;
    pgm=programStartPipe(args);
}


void
databaseCreate(char* dName) {
    char    *args[6],
            cmdbuf[1024],
            *cmds[3];

    args[0]=(char*)"mysql";
    args[1]=(char*)"--force";
    args[2]=(char*)"--unbuffered";
    args[3]=(char*)"--user";
    args[4]=DBUSER;
    args[5]=NULL;
    snprintf(cmdbuf, 1023, "drop database %s;\n", dName);
    cmds[0]=cmdbuf;
    cmds[1]=(char*)"quit\n";
    cmds[2]=NULL;
    runProg(args,cmds);
    snprintf(cmdbuf, 1023, "create database %s;\n", dName);
    cmds[1]=(char*)"quit\n";
    cmds[2]=NULL;
    runProg(args,cmds);
}


void
databaseDisconnect() {
    programEnd(pgm,(char*)"quit;\n");
}


void
ddl(char* stg) {
    programFeed2(pgm, stg, (char*)";\n");
    interpretResult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DDL;
}


void
dml(char* stg) {
    programFeed2(pgm, stg, (char*)";\n");
    interpretResult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DML;
}


int
getTuple() {
    pTuple=tupleReturned;
    *pTuple=0;
    if (pBuffer==NULL) {
        if ((pBuffer=programGetOutput(pgm))==NULL)
            return 0;
    }
    if (*pBuffer==0) {
        pBuffer=programGetOutput(pgm);
    }
    while (pBuffer) {
        switch (*pTuple++=*pBuffer++) {
        case '\r':  pTuple--;   /* discard the "^M"s for */
                    break;      /* debugging readability */

        case '\n':  *pTuple=0;
                    if (watch)
                        printf("gt> %s", tupleReturned);
                    goto gotIt;

        case 0:     pTuple--; /* back over the NULL byte */ 
                        pBuffer=programGetOutput(pgm);
        }
    }
gotIt:
    return 0;
}

void
interpretResult() {
    getTuple();
    if (strncmp(tupleReturned,"ERROR",5)==0) {
        OSDB_ERROR.error=ERR_UNKNOWN;
        OSDB_ERROR.text=tupleReturned;
        return;
    }
    OSDB_ERROR.error=ERR_OK;
    return;
}


int
load() {
    OSDB_ERROR.error=ERR_OK;
    snprintf(cmd, CMDBUFLEN,
        "load data infile '%s/%s' into table %s fields terminated by ',';\n",
        dataDir, FILENAME_UPDATES, "updates");
    programFeed(pgm,cmd);
    snprintf(cmd, CMDBUFLEN,
        "load data infile '%s/%s' into table %s fields terminated by ',';\n",
        dataDir, FILENAME_HUNDRED, "hundred");
    programFeed(pgm,cmd);
    snprintf(cmd, CMDBUFLEN,
        "load data infile '%s/%s' into table %s fields terminated by ',';\n",
        dataDir, FILENAME_TENPCT, "tenpct");
    programFeed(pgm,cmd);
    snprintf(cmd, CMDBUFLEN,
        "load data infile '%s/%s' into table %s fields terminated by ',';\n",
        dataDir, FILENAME_UNIQUES, "uniques");
    programFeed(pgm,cmd);
    snprintf(cmd, CMDBUFLEN,
        "load data infile '%s/%s' into table %s fields terminated by ',';\n",
        dataDir, FILENAME_TINY, "tiny");
    programFeed(pgm,cmd);
    return 0;
}

void
setDefaults() {
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER="root";
    DBNAME="osdb";
    DBPORT=0;
    OSDB_ERROR.error=ERR_OK;
    OSDB_ERROR.text=(char*)"no error text";
    pBuffer=NULL;
    tupleReturned[0]=0;
}



void
transactionBegin() {
    dml("begin");
}


void
transactionCommit() {
    dml("commit");
}


void
transactionRollback() {
    dml("rollback");
} 
