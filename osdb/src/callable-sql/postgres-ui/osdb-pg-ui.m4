/***************************************************************************
                  postgres-ui.c  -  PostgreSQL calls for OSDB/postgres-ui
                     -------------------
    begin        : Fri Dec 1 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-pg-ui.m4,v $
Revision 1.4  2004/10/11 22:17:44  ariebs
Make the help message helpful!

Revision 1.3  2003/05/04 18:14:39  ariebs
More changes to incorporate the-tests.m4 across the board.

Revision 1.2  2003/04/22 10:19:03  ariebs
Add DROP_INDEX macro to accomodate differing syntax between databases.
(Note that SQL92 appears not to have defined standard language for this.)

Revision 1.1  2003/03/04 03:32:16  ariebs
Modify osdb-pg-ui to compile with (but not yet work with) the-tests.m4

Revision 1.16  2002/07/12 21:32:36  ariebs
Apply Neil Conway's cleanups for compiler warnings

Revision 1.15  2002/03/12 03:58:35  ariebs
Clean up the usage() message formatting.

Revision 1.14  2002/03/10 20:17:51  ariebs
Implement the --postgresql=no_hash_index option.

Revision 1.13  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.12  2002/03/05 03:30:21  ariebs
Add a createTable() call in preparation for allowing database-specific
syntax, e.g., mysql's "table=" options.

Revision 1.11  2002/01/29 00:10:14  ariebs
Apply Peter Eisentraut's portability patches.

Revision 1.10  2001/09/12 05:12:20  vapour
Changed some tests to be = TRUE instead of = 1

Revision 1.9  2001/09/09 07:00:26  vapour
Updated the braces to follow the OSDB Style Guide recommendations

Revision 1.8  2001/08/15 01:07:35  ariebs
Apply Maciej's fixes for foreign key support

Revision 1.7  2001/08/11 21:02:18  ariebs
Make all the cluster indexes unique (as they should have been --
Thanks Maciej!)

Revision 1.6  2001/04/13 15:46:21  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

Revision 1.5  2001/02/23 20:38:27  ariebs
Cosmetic changes to
(a) Use forkpty() (so I added LDFLAGS to all the makefiles while I was
    at it, and
(b) Add "diagnostics()" to osdb.c (which perturbed most programs)

Revision 1.3  2001/02/18 14:34:50  ariebs
Clean out obsolete modification history

Revision 1.2  2001/02/18 14:30:31  ariebs
Implement programStartPty() for MySQL, and eliminate my hideous patch
for the mysql program. Also renamed "programStart()" to
"programStartPipe()"

Revision 1.1.1.1  2001/02/06 03:46:38  ariebs
Establish base level for code on Source Forge!
*/

#include "callable-sql.h"
#include "osdb.h"

m4_changequote([,])
m4_changequote(`,~)

m4_define(`BTREE~,`using btree($*)~)m4_dnl
m4_define(`HASH~,`using hash($*)~)m4_dnl
m4_define(`CLUSTER~,`ddl("cluster $1 on $2");~)m4_dnl
m4_define(`SQL_DATE~,
        `qvPtr+=sprintf(qvPtr, "%s DATE(%s%s%s)", comma, apostrophe, $1,
apostrophe);~)m4_dnl

m4_define(`USE_INTO_TABLE~, `into table $1~)m4_dnl
m4_define(`USE_INTO_TEMP~, `~)m4_dnl


/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    timestamp $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float4 $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`DROP_INDEX~, `ddl("drop index $2");~) 

m4_include(`../callable-sql.m4~)
m4_include(`../../the-tests.m4~)

int  getTuple();
void interpretResult();

#define CMDBUFLEN   1024
char        cmd[CMDBUFLEN],      /* frequently used, but very transient */
            DB_CONNECT_CMD[200], /* PQfinish() wants this string to exist! */
            *pBuffer,
            *pTuple;
OSDB_RUN    *pgm;


int
argument_check(char* arg)
{
    char*   p;

    if (strncmp(arg, "--postgresql=", 13)==0) {
        p=arg+13;
        if (strncmp(p, "no_hash_index", 13)==0) {
            NO_HASH_INDEX=TRUE;
            return(TRUE);
        }
    }
    return(FALSE);
}

void
argument_choices() {
    printf("\n  PostgreSQL-specific options:\n"
"    [--postgresql=no_hash_index]\n"
"                        Do not use hash indexes (historically\n"
"                        a performance problem in PostgreSQL.\n"
);
}

void
bugout(int terminate, char* stg, char* fileName, int lineNo) {
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    diagnostics();
    if (++failureCount >= MAX_FAILURES) {
        terminate = TRUE;  /* Updated to be = TRUE.  JC */
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate) {
        if (iAmParent) {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed = 1;  /* Updated to be = TRUE.  JC */
}



void
createIndexBtree(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create index %s on %s using btree (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
    transactionCommit();
}


void
createIndexCluster(char* iName, char* tName, char* fields) {
    snprintf(cmd, CMDBUFLEN,
        "create unique index %s on %s using btree (%s)",
        iName, tName, fields);
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("btree error");
        snprintf(cmd, CMDBUFLEN, "cluster %s on %s", iName, tName );
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("cluster error"); 
    transactionCommit();
}


void
createIndexForeign(char* tName, char* keyName, char* keyCol,
                        char* fTable, char* fFields) {
    snprintf(cmd, CMDBUFLEN,
        "alter table %s add constraint %s foreign key (%s) references %s (%s)",
        tName, keyName, keyCol, fTable, fFields);
    transactionBegin(); 
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("foreign key error"); 
    transactionCommit();
}


void
createIndexHash(char* iName, char* tName, char* fields) {
    if (NO_HASH_INDEX) {
        snprintf(cmd, CMDBUFLEN,
            "create index %s on %s (%s)",
            iName, tName, fields);
    } else {
        snprintf(cmd, CMDBUFLEN,
            "create index %s on %s using hash (%s)",
            iName, tName, fields);
    }
    transactionBegin();
        ddl(cmd);
        if (OSDB_ERROR.error)
            BUGOUT("hash error"); 
    transactionCommit();
}


int
create_idx_hundred_foreign() {
    if (doIndexes) {  /* Pointless without index on updates.key */
        createIndexForeign("hundred", "fk_hundred_updates", "col_signed",
                            "updates", "col_key");
    }
    return 0;
}


void
createTable(char* stg) {
    ddl(stg);
}


void
cursorClose() {
    dml("close MYCURSOR");
    /* return with the error status */
}


void
cursorDeclare(char* stg) {
    snprintf(cmd, CMDBUFLEN, "declare MYCURSOR cursor for %s", stg);
    dml(cmd);
    if (OSDB_ERROR.error)
        TESTFAILED("cursor declare");
    dml("fetch all from MYCURSOR");
    if (OSDB_ERROR.error)
        TESTFAILED("fetch failed");
    
}


void
cursorFetch(char *dest) {
    getTuple();
    if (*tupleReturned=='(') {
          /* found something like "(1000 rows)", so we're at end of data */
        OSDB_ERROR.error=ERR_NOTFOUND;
        return;
    }
    OSDB_ERROR.error=ERR_OK;
}


void
cursorOpen() {
    /* no-op under Postgres; psql rejects "open" */
    getTuple();     /* filter out the header lines */
    while (strncmp(tupleReturned,"---",3)) {
        if (*tupleReturned==0)
            return;
        if (watch)
            printf("cuOp> %s\n", tupleReturned);
        getTuple();
    }
}

void
databaseConnect() {
    char    *args[3];

    args[0]=(char*)"psql";
    args[1]=DBNAME;
    args[2]=NULL;
    pgm=programStartPipe(args);
}

void
databaseCreate(char* dName) {
    char    *args[3];
    OSDB_RUN *pgm;

    args[0]=(char*)"dropdb";
    args[1]=dName;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
    args[0]=(char*)"createdb";
    args[1]=dName;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
}


void
databaseDisconnect() {
    programEnd(pgm,(char*)"\\q\n");
}


void
ddl(char* stg) {
    programFeed2(pgm, stg, (char*)";\n");
    interpretResult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DDL;
}


void
dml(char* stg) {
    programFeed2(pgm, stg, (char*)";\n");
    interpretResult(); 
    if (OSDB_ERROR.error==ERR_UNKNOWN)
        OSDB_ERROR.error=ERR_DML;
}


int
getTuple() {
    pTuple=tupleReturned;
    *pTuple=0;
    if (pBuffer==NULL) {
        if ((pBuffer=programGetOutput(pgm))==NULL)
            return 0;
    }
    if (*pBuffer==0) {
        pBuffer=programGetOutput(pgm);
    }
    while (pBuffer) {
        switch (*pTuple++=*pBuffer++) {
            case '\n':  *pTuple=0;
                        if (watch)
                            printf("gt> %s", tupleReturned);
                        return 0;

            case 0:     pTuple--; /* back over the NULL byte */ 
                        pBuffer=programGetOutput(pgm);
        }
    }
    return 1;
}

void
interpretResult() {
    getTuple();
    if (strncmp(tupleReturned,"ERROR",5)==0) {
        OSDB_ERROR.error=ERR_UNKNOWN;
        OSDB_ERROR.text=tupleReturned;
        return;
    }
    OSDB_ERROR.error=ERR_OK;
    return;
}


int
load() {
    char    *args[3],
            *cmds[7],
            Scmds[5][256];
    int     i;

    snprintf(Scmds[0], 256,
        "copy updates from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UPDATES);
    snprintf(Scmds[1], 256,
        "copy hundred from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_HUNDRED);
    snprintf(Scmds[2], 256,
        "copy tenpct from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TENPCT);
    snprintf(Scmds[3], 256,
        "copy uniques from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UNIQUES);
    snprintf(Scmds[4], 256,
        "copy tiny from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TINY);

    for (i=0; i<5; i++) {
        cmds[i]=Scmds[i];
    }
    cmds[5]=(char*)"\\q\n";
    cmds[6]=NULL;
    args[0]=(char*)"psql";
    args[1]=(char*)"osdb";
    args[2]=NULL;
    return runProg(args, cmds);
}

void
setDefaults() {
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=NULL;
    DBNAME="osdb";
    DBPORT=0;
    OSDB_ERROR.error=ERR_OK;
    OSDB_ERROR.text=(char*)"no error text";
    pBuffer=NULL;
    tupleReturned[0]=0;
}


void
transactionBegin() {
    dml("begin");
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}


void
transactionCommit() {
    dml("commit");
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}


void
transactionRollback() {
    dml("rollback");
    if (OSDB_ERROR.error)
        TESTFAILED("dml error"); 
}

