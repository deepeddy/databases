/***************************************************************************
                  callable-sql.m4  -  Driver for callable SQL interfaces
                     -------------------
    begin        : Mon Jan 20 2003
    copyright    : (C) 2003 by Andy Riebs
    email        : ariebs@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: callable-sql.m4,v $
Revision 1.4  2003/06/04 02:17:19  ariebs
Fix CREATE_TABLE to invoke createTable()

Revision 1.3  2003/04/09 02:58:27  ariebs
Create CURSOR_DECLARE_SVAR() to take a variable string argument.

Revision 1.2  2003/03/05 03:42:45  ariebs
Remove SQL_DATE from the common code (move it to the implementation-specific
code).

Revision 1.1  2003/02/17 18:15:24  ariebs
More of the "the-tests.m4" checkin.

*/ 

char    *apostrophe="'";
char    *comma=",",
        tupleReturned[MAX_TUPLE_LENGTH],
            /* handy place to dump fetch/select results */
        queryString[MAX_TUPLE_LENGTH],
            /* build the query string here */
        queryValues[MAX_TUPLE_LENGTH];
            /* */

m4_changequote([,])
m4_changequote(`,~)
m4_define(`SQL_MODE~, `callable~)m4_dnl
m4_define(`EXEC_SQL_INCLUDE~, `~)m4_dnl
m4_define(`TRANSACTION_BEGIN~, `transactionBegin();~)m4_dnl
m4_define(`TRANSACTION_COMMIT~, `transactionCommit();~)m4_dnl
m4_define(`TRANSACTION_ROLLBACK~, `transactionRollback();~)m4_dnl
m4_define(`CURSOR_CLOSE~, `cursorClose();~)m4_dnl
m4_define(`CURSOR_DECLARE~, `cursorDeclare("$1");~)m4_dnl
m4_define(`CURSOR_DECLARE_SVAR~, `cursorDeclare($1);~)m4_dnl
m4_define(`CURSOR_DECLARE_WITH_VARIABLE~,
        `sprintf(queryString, "$1", $2); cursorDeclare(queryString);~)m4_dnl
m4_define(`CURSOR_OPEN~, `cursorOpen();~)m4_dnl
m4_define(`CREATE_TABLE~, `createTable("create table $1 ($2)");~)m4_dnl
m4_define(`DECLARE_BEGIN~, `~)m4_dnl
m4_define(`DECLARE_END~, `~)m4_dnl
m4_define(`DDL~, `ddl("$1");~)m4_dnl
m4_define(`DML~, `dml("$1");~)m4_dnl
m4_define(`DML_S~, `dml($1);~)m4_dnl
m4_define(`DML_INSERT_VARIABLE_VALUES~,
          `{ char *qvPtr;
          
             qvPtr=queryValues;
             comma="";
             MATERIALIZE_VARIABLES($3);
             sprintf(queryString, "insert into $1 ($2) values (%s)", queryValues);
           }
           dml(queryString);~)m4_dnl
m4_define(`DML_WITH_VARIABLE~,
             `sprintf(queryString, "$1", $2);
              dml(queryString);~)m4_dnl
m4_define(`FETCH~, `cursorFetch(tupleReturned);
                    if (OSDB_ERROR.error == ERR_OK)
                        sscanf(tupleReturned, "$3" REFERENCE_VARIABLES($2));~)m4_dnl)
m4_define(`REFERENCE_VARIABLES~,
        `m4_ifelse($#, 0, ,
                   $#, 1, `, &$1~,
                       ` , &$1
                         REFERENCE_VARIABLES(m4_shift($@)) ~)
        ~)m4_dnl
                         
m4_define(`MATERIALIZE_VARIABLES~,
          `m4_ifelse(m4_regexp($1, `.*-~), -1,
                      `m4_define(`MVARGS~, `m4_patsubst($1, ` ~, `,~)~)~,
                      `m4_define(`MVARGS~, `m4_patsubst($1, `-~, `, -~)~)~
           )
           /* MV ARGS="MVARGS" */
           m4_ifelse($#, 0, ,
                     $#, 1, `MATERIALIZE_SPRINTF(MVARGS)~,
                       `MATERIALIZE_SPRINTF(MVARGS);
                        comma=",";
                        MATERIALIZE_VARIABLES(m4_shift($@))~)
         ~)m4_dnl
m4_define(`MATERIALIZE_SPRINTF~,
          `m4_ifelse($2,  -,   $1,
                     $2, `%s~,
                       `qvPtr+=sprintf(qvPtr, "%s %s$2%s", comma, apostrophe, $1, apostrophe);~,
                     `qvPtr+=sprintf(qvPtr, "%s $2", comma, $1);~)
           ~)m4_dnl
m4_define(`VARIABLES_LIST~,
    `m4_ifelse($#, 0, ,
               $#, 1, `$1~,
               `$1, VARIABLES_LIST(m4_shift($@))~)~)m4_dnl

m4_define(`EXPECT_ANY~, `{ if (OSDB_ERROR.error != ERR_OK) { $1 } }~)
m4_define(`IF_NOTFOUND~, `if (OSDB_ERROR.error == ERR_NOTFOUND) { $1 }~)
m4_define(`EXPECT_FAILURE~, `{ if (OSDB_ERROR.error != $1) { TESTFAILED($2); $3 } }~)
m4_define(`EXPECT_SUCCESS~, `{ if (OSDB_ERROR.error != ERR_OK)
                                    { TESTFAILED("query failed"); $1} }~)

m4_define(`FETCH_TUPLE~,
    `FETCH(`CURRENT_CURSOR~,
        `col_key, col_int, col_signed, col_float, col_double, col_decim, col_date, col_code, col_name, col_address~,
        `%d       %d       %d          %f         %lf         %ld        %s        %s %s %s~);
        if (OSDB_ERROR.error == ERR_OK) 
            snprintf(column_buffer, 300,
                "%s %s %s %s %s %s %s %s %s %s\n",
                col_key, col_int, col_signed, col_float,
                col_double, col_decim, col_date, col_code,
                col_name, col_address);~
)
