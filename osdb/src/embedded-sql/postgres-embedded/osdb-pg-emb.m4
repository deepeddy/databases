/***************************************************************************
                  osdb-pg-emb.m4  -  osdb code for PostgreSQL embedded SQL
                     -------------------
    begin        : Fri Dec 20 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-pg-emb.m4,v $
Revision 1.18  2004/10/11 22:17:44  ariebs
Make the help message helpful!

Revision 1.17  2003/05/15 03:10:36  ariebs
Implement --dbuser, --dbpassword, --dbhost, and --dbport for embedded pgsql.

Revision 1.16  2003/05/14 03:48:13  ariebs
Implement --dbuser and --dbpassword.

Revision 1.15  2003/04/22 10:19:04  ariebs
Add DROP_INDEX macro to accomodate differing syntax between databases.
(Note that SQL92 appears not to have defined standard language for this.)

Revision 1.14  2003/02/17 18:02:24  ariebs
Bunch of changes to use a common source, the-tests.m4, for both "embedded"
and "callable" tests. This version is a bit hacky, and only supports
PostgreSQL (except -ui) and Informix (I hope).

Revision 1.13  2003/01/21 00:19:26  ariebs
Convert the embedded tests to use a new set of m4 macros (the next step
will be to modify the callable tests to define and use the same set of
macros).

Revision 1.12  2002/07/12 21:32:36  ariebs
Apply Neil Conway's cleanups for compiler warnings

Revision 1.11  2002/05/31 23:18:38  ariebs
Change the database name from "as3ap" to "osdb".

Revision 1.10  2002/03/12 03:58:35  ariebs
Clean up the usage() message formatting.

Revision 1.9  2002/03/11 04:14:55  ariebs
Implement --postgresql=no_hash_index for the embedded SQL PostgreSQL test.

Revision 1.8  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.7  2002/01/29 00:10:14  ariebs
Apply Peter Eisentraut's portability patches.

Revision 1.6  2001/04/17 23:59:29  ariebs
Generalize some of the error handling for embedded SQL, and add
Informix diagnostics

Revision 1.5  2001/04/13 15:46:21  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

*/

/* Simple macros */

#define EMB_DUPLICATE_ERROR -400
#define EMB_NOTFOUND_ERROR  ECPG_NOT_FOUND

/* M4-based definitions */

/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_changequote([,])
m4_changequote(`,~)

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    timestamp $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float4 $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`CREATE_TABLE~, `exec sql create table $1($2);~)

m4_define(`DECLARE_COLUMNS~,
    `exec sql begin declare section;m4_dnl
        char   col_key[20], m4_dnl
               col_int[20], m4_dnl
               col_signed[20], m4_dnl
               col_float[20], m4_dnl
               col_double[30], m4_dnl
               col_decim[22], m4_dnl
               col_date[20], m4_dnl
               col_code[11], m4_dnl
               col_name[21]; m4_dnl
        char   col_address[80]; m4_dnl
    exec sql end declare section;
        char  column_buffer[300];~
)

m4_define(`FETCH_TUPLE~,
    `exec sql fetch CURRENT_CURSOR into m4_dnl
               :col_key, m4_dnl
               :col_int, m4_dnl
               :col_signed, m4_dnl
               :col_float, m4_dnl
               :col_double, m4_dnl
               :col_decim, m4_dnl
               :col_date, m4_dnl
               :col_code, m4_dnl
               :col_name, m4_dnl
               :col_address; m4_dnl
        if (sqlca.sqlcode==0) { /* only do this on success */
            snprintf(column_buffer, 300,
                "%s %s %s %s %s %s %s %s %s %s\n",
                col_key, col_int, col_signed, col_float,
                col_double, col_decim, col_date, col_code,
                col_name, col_address);~
        }
)


/* Postgres's ecpg, at least, requires unique cursor names for 
 * each block, so we'll generate unique names here.
*/

m4_define(`CURSOR_DECLARE~,
               `m4_define(`CURSOR_N~,m4_incr(CURSOR_N))m4_dnl
                m4_define(`CURRENT_CURSOR~,`cursor_~CURSOR_N)m4_dnl
                exec sql declare CURRENT_CURSOR cursor for $1;~)m4_dnl
m4_define(`CURSOR_N~, `0~)

m4_define(`DROP_INDEX~,`exec sql drop index $2;~)

m4_include(`../embedded-sql.m4~)
m4_include(`../../the-tests.m4~)
 

extern int      NO_HASH_INDEX;

int
argument_check(char* arg)
{
    char*   p;

    if (strncmp(arg, "--postgresql=", 13)==0) {
        p=arg+13;
        if (strncmp(p, "no_hash_index", 13)==0) {
            NO_HASH_INDEX=TRUE;
            return(TRUE);
        }
    }
    return(FALSE);
}


void
argument_choices() {
    printf("\n  PostgreSQL-specific options:\n"
"    [--postgresql=no_hash_index]\n"
"                        Do not use hash indexes (historically\n"
"                        a performance problem in PostgreSQL.\n"
);
}


void
bugout(int terminate, char* stg, char* fileName, int lineNo)
{
    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    if (sqlca.sqlcode)
        fprintf(stderr,
                "sql: %ld, %s\n", sqlca.sqlcode, sqlca.sqlerrm.sqlerrmc);
    if (errno)
        perror("perror() reports");
    fprintf(stderr,
            "OSDB_ERROR.error: %d",
            OSDB_ERROR.error);
    if (++failureCount >= MAX_FAILURES)
    {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate)
    {
        exec sql disconnect osdb;
        if (iAmParent)
        {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed=1;
}


void
create_database()
{
char    *args[8],
    *cmds[8];

     fprintf(stderr, "Ignore message if \"dropdb\" fails\n");
     args[0]=(char*)"dropdb";
     args[1]=(char*)"osdb";
     args[2]=NULL;
     cmds[0]=NULL;
     runProg(args, cmds);
     args[0]=(char*)"createdb";
     args[1]=(char*)"osdb";
     args[2]=NULL;
     cmds[0]=NULL;
     runProg(args, cmds);
}


int
create_idx_hundred_foreign()
{
    if (doIndexes)  // Pointless without index on updates.key
    {
        char  *args[3],
              *sqlCmds[3];

        args[0]="psql";
        args[1]="osdb";
        args[2]=NULL;
        sqlCmds[0]=(char*)"alter table hundred \
                    add foreign key (col_signed) \
                    references updates (col_key);\n";
        sqlCmds[1]=(char*)"\\q\n";
        sqlCmds[2]=NULL;
        runProg(args, sqlCmds);
        EXPECT_SUCCESS;
    }
    return 0;
}


void databaseConnect()
{
    exec sql begin declare section;
        char    connection[4096];
        char    username[4096];
    exec sql end declare section;

    strncpy(connection, DBNAME, 4096);
    if (DBHOST) {
        if (DBPORT) {
            snprintf(connection, 4096, "%s@%s:%d", DBNAME, DBHOST, DBPORT);
        } else {
            snprintf(connection, 4096, "%s@%s", DBNAME, DBHOST);
        }
    }
    if (DBPASSWORD) {
        snprintf(username, 4096, "%s/%s", DBUSER, DBPASSWORD);
    } else {
        snprintf(username, 4096, "%s", DBUSER);
    }
    exec sql connect to :connection user :username;
    if (sqlca.sqlcode) {
        TESTFAILED("database connect");
        exit(1);
    }
}


void
databaseCreate(char* nosuch)
{
    char        *args[3];
    OSDB_RUN    *pgm;

    args[0]=(char*)"dropdb";
    args[1]=DBNAME;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL);
    args[0]=(char*)"createdb";
    args[1]=DBNAME;
    args[2]=NULL;
    pgm=(OSDB_RUN*)programStartPipe(args);
    programEnd(pgm,NULL); 
}


void databaseDisconnect()
{
    exec sql disconnect osdb;
}


int
load()
{
    char    *args[3],
            *cmds[7],
            sqlCmds[5][256];
    int     i;
    
    snprintf(sqlCmds[0], 255,
        "copy updates from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UPDATES);
    snprintf(sqlCmds[1], 255,
        "copy hundred from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_HUNDRED);
    snprintf(sqlCmds[2], 255,
        "copy tenpct from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TENPCT);
    snprintf(sqlCmds[3], 255,
        "copy uniques from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_UNIQUES);
    snprintf(sqlCmds[4], 255,
        "copy tiny from '%s/%s' using delimiters ',';\n",
        dataDir, FILENAME_TINY);
    for (i=0; i<5; i++)
    {
        cmds[i]=sqlCmds[i];
    }
    cmds[5]=(char*)"\\q\n";
    cmds[6]=NULL;
    args[0]=(char*)"psql";
    args[1]=(char*)"osdb";
    args[2]=NULL;
    runProg(args,cmds);
    return 0;
}


void
setDefaults()
{
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=getlogin();
    DBNAME="osdb";
    DBPORT=0;
    OSDB_ERROR.error=ERR_OK;
    OSDB_ERROR.text=(char*)"no error text";
}

