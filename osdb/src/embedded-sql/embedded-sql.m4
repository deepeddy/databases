/***************************************************************************
                  embedded-sql.m4  -  osdb code for  embedded SQL
                     -------------------
    begin        : Fri Dec 20 2000
    copyright    : (C) 2000 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: embedded-sql.m4,v $
Revision 1.25  2003/02/17 18:02:21  ariebs
Bunch of changes to use a common source, the-tests.m4, for both "embedded"
and "callable" tests. This version is a bit hacky, and only supports
PostgreSQL (except -ui) and Informix (I hope).

Revision 1.24  2003/01/21 00:19:26  ariebs
Convert the embedded tests to use a new set of m4 macros (the next step
will be to modify the callable tests to define and use the same set of
macros).

*/
m4_changequote([,])
m4_changequote(`,~)

m4_define(`SQL_MODE~, `embedded~)m4_dnl
m4_define(`EXEC_SQL_INCLUDE~, `exec sql include $1;~)m4_dnl
m4_define(`TRANSACTION_BEGIN~, `exec sql begin;~)m4_dnl
m4_define(`TRANSACTION_COMMIT~, `exec sql commit;~)m4_dnl
m4_define(`TRANSACTION_ROLLBACK~, `exec sql rollback;~)m4_dnl
m4_define(`CURSOR_CLOSE~, `exec sql close $1;~)m4_dnl
m4_define(`CURSOR_OPEN~, `exec sql open $1;~)m4_dnl
m4_define(`DECLARE_BEGIN~, `exec sql begin declare section;~)m4_dnl
m4_define(`DECLARE_END~, `exec sql end declare section;~)m4_dnl
m4_define(`DDL~, `exec sql $1;~)m4_dnl
m4_define(`DML~, `exec sql $1;~)m4_dnl
m4_define(`DML_INSERT_VARIABLE_VALUES~,
        `exec sql insert into $1 ($2) values(m4_patsubst(`$3~, ` -~));~)m4_dnl
m4_define(`FETCH~, `exec sql fetch $1 into VARIABLES_LIST($2);~)m4_dnl
m4_define(`VARIABLE_ARG~, `:$1~)
m4_define(`VARIABLES_LIST~,
    `m4_ifelse($#, 0, ,
               $#, 1, VARIABLE_ARG(m4_patsubst($1,` ~,`,~)),
                 `VARIABLE_ARG(m4_patsubst($1,` ~, `,~)), VARIABLES_LIST(m4_shift($@))~)~)m4_dnl
m4_define(`BTREE~,`using btree($*)~)m4_dnl
m4_define(`CLUSTER~,`exec sql cluster $1 on $2;~)m4_dnl
m4_define(`HASH~,`using hash($*)~)m4_dnl

m4_define(`USE_INTO_TABLE~, `into table $1~)m4_dnl
m4_define(`USE_INTO_TEMP~, `~)m4_dnl

m4_define(`EXPECT_ANY~, `if (sqlca.sqlcode) { $1 }~)
m4_define(`EXPECT_FAILURE~, `if (sqlca.sqlcode != $1) { TESTFAILED( $2 ); $3}~)
m4_define(`IF_NOTFOUND~, `if (sqlca.sqlcode == EMB_NOTFOUND_ERROR) { $1 }~)
m4_define(`EXPECT_SUCCESS~, `if (sqlca.sqlcode) { TESTFAILED("query failed"); $1}~)

m4_define(`SQL_DATE~,`:$1~)m4_dnl

EXEC_SQL_INCLUDE(`sqlca~)
