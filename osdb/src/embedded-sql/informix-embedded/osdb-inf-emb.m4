/***************************************************************************
                  osdb-inf-emb.m4  -  osdb code for Informix embedded SQL
                     -------------------
    begin        : Fri Mar 30 2001
    copyright    : (C) 2000-2001 by Andy Riebs and Compaq Computer Corporation
    email        : andy.riebs@compaq.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software Foundation,*
 *  Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA          *
 **************************************************************************/

/*
$Log: osdb-inf-emb.m4,v $
Revision 1.10  2003/02/17 18:02:22  ariebs
Bunch of changes to use a common source, the-tests.m4, for both "embedded"
and "callable" tests. This version is a bit hacky, and only supports
PostgreSQL (except -ui) and Informix (I hope).

Revision 1.9  2003/01/21 00:19:26  ariebs
Convert the embedded tests to use a new set of m4 macros (the next step
will be to modify the callable tests to define and use the same set of
macros).

Revision 1.8  2002/03/07 11:45:09  ariebs
Add the infrastructure to allow product-specific run-time options.

Revision 1.7  2001/04/18 02:53:26  ariebs
Fix handling of create_index_hundred_foreign(), which (should)
generate a runtime error under Informix

Revision 1.6  2001/04/18 02:43:32  ariebs
Protect the transaction in mu_ir_select() with LOCK_PROTECT(),
and change the query from declare/open/fetch/close to "select into"

Revision 1.5  2001/04/17 23:59:29  ariebs
Generalize some of the error handling for embedded SQL, and add
Informix diagnostics

Revision 1.4  2001/04/14 16:03:59  ariebs
Inching our way to a working version of embedded SQL for Informix...

Revision 1.3  2001/04/13 15:46:21  ariebs
Mass change of global "log" to "osdblog" to avoid namespace collision
with Informix

*/

/* C preprocessor macros */

#define EMB_DUPLICATE_ERROR -239
#define EMB_NOTFOUND_ERROR  100

/* M4-based definitions */

/* N.B. Informix admits of knowing nothing of btrees, hash indexes,
 * or clustered indexes, so these macros must not do too much. (See 
 * osdb-pg-emb for an example that does use them.)
*/

m4_changequote([,])
m4_changequote(`,~)

m4_define(`BTREE~,`($*)~)m4_dnl
m4_define(`CLUSTER~,`~)m4_dnl
m4_define(`HASH~,`($*)~)m4_dnl

/* PostgreSQL uses "select * into table foo where ..."; Informix
 * uses "select * where ... into temp foo". Cover both cases.
*/
m4_define(`USE_INTO_TABLE~, `~)m4_dnl
m4_define(`USE_INTO_TEMP~, `into temp $1~)m4_dnl

/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
*/

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    char(20) $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    int $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`CREATE_TABLE~, `exec sql create table $1($2)~);

m4_define(`DECLARE_COLUMNS~,
    `exec sql begin declare section;m4_dnl
        char   col_key[20], m4_dnl
               col_int[20], m4_dnl
               col_signed[20], m4_dnl
               col_float[20], m4_dnl
               col_double[30], m4_dnl
               col_decim[22], m4_dnl
               col_date[20], m4_dnl
               col_code[11], m4_dnl
               col_name[21]; m4_dnl
        char   col_address[80]; m4_dnl
    exec sql end declare section;~
        char  column_buffer[300];
)

m4_define(`FETCH_TUPLE~,
    `exec sql fetch CURRENT_CURSOR into m4_dnl
               :col_key, m4_dnl
               :col_int, m4_dnl
               :col_signed, m4_dnl
               :col_float, m4_dnl
               :col_double, m4_dnl
               :col_decim, m4_dnl
               :col_date, m4_dnl
               :col_code, m4_dnl
               :col_name, m4_dnl
               :col_address; m4_dnl
        if (sqlca.sqlcode==0) {  /* Only if the query succeeded */
            snprintf(column_buffer, 300,
                "%s %s %s %s %s %s %s %s %s %s\n",
                col_key, col_int, col_signed, col_float,
                col_double, col_decim, col_date, col_code,
                col_name, col_address);~
        }
)


/* Postgres's ecpg, at least, requires unique cursor names for 
 * each block, so we'll generate unique names here.
*/

m4_define(`CURSOR_DECLARE~,
               `m4_define(`CURSOR_N~,m4_incr(CURSOR_N))m4_dnl
                m4_define(`CURRENT_CURSOR~,`cursor_~CURSOR_N)m4_dnl
                exec sql declare CURRENT_CURSOR cursor for $1~)m4_dnl
m4_define(`CURSOR_N~, `0~)

m4_include(`../embedded-sql.m4~)
m4_include(`../../the-tests.m4~)
 
int
argument_check(char* arg) {
    return(FALSE);
}


void
argument_choices() {
}


void
bugout(int terminate, char* stg, char* fileName, int lineNo)
{
    exec sql begin declare section;
        int     i,
                infMsgLen,
                numExc;
        char    infMsg[8192];
    exec sql end declare section;

    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    if (sqlca.sqlcode)
        fprintf(stderr,
                "sql: %ld, %s\n", sqlca.sqlcode, sqlca.sqlerrm);
    if (errno)
        perror("perror() reports");
    fprintf(stderr,
            "OSDB_ERROR.error: %d\n",
            OSDB_ERROR.error);
    fprintf(stderr, "\n----- Informix diagnostics follow  -----\n");
    exec sql get diagnostics :numExc = NUMBER;
    for (i=1; i<=numExc; i++) {
        exec sql get diagnostics exception :i
            :infMsg=MESSAGE_TEXT, :infMsgLen=MESSAGE_LENGTH;
        infMsg[infMsgLen]=0;
        fprintf(stderr, "%d. %s\n", i, infMsg);
    }
    fprintf(stderr, "----- End of Informix diagnostics  -----\n");
    if (++failureCount >= MAX_FAILURES)
    {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate)
    {
        exec sql disconnect 'osdb';
        if (iAmParent)
        {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=ERR_UNKNOWN;
    testFailed=1;
}


void
create_database()
{
char    *args[8],
    *cmds[8];

     fprintf(stderr, "Ignore message if \"dropdb\" fails\n");
     args[0]=(char*)"dropdb";
     args[1]=(char*)"as3ap";
     args[2]=NULL;
     cmds[0]=NULL;
     runProg(args, cmds);
     args[0]=(char*)"createdb";
     args[1]=(char*)"as3ap";
     args[2]=NULL;
     cmds[0]=NULL;
     runProg(args, cmds);
}


int
create_idx_hundred_foreign()
{
    if (doIndexes)  // Pointless without index on updates.key
    {
        exec sql alter table hundred 
                    add foreign key (col_signed) 
                    references updates (col_key);
        EXPECT_SUCCESS;
    }
    return 0;
}


void databaseConnect()
{
    exec sql connect to 'osdb';
}


void
databaseCreate(char* nosuch)
{
    char        *args[4],
                *cmds[3];

    args[0]=(char*)"dbaccess";
    args[1]=(char*)"-";
    args[2]=(char*)"-";
    args[3]=NULL; 
    cmds[0]=(char*)"drop database osdb;\n";
    cmds[1]=(char*)"create database osdb with log ;\n";
    cmds[2]=NULL;
    runProg(args, cmds);
}


void databaseDisconnect()
{
    exec sql disconnect 'osdb';
}


int
load()
{
    char    *args[4],
            *cmds[7],
            sqlCmds[5][256];
    int     i;
    
    snprintf(sqlCmds[0], 255,
        "load from '%s/%s' delimiter ',' insert into updates;\n",
        dataDir, FILENAME_UPDATES);
    snprintf(sqlCmds[1], 255,
        "load from '%s/%s' delimiter ',' insert into hundred;\n",
        dataDir, FILENAME_HUNDRED);
    snprintf(sqlCmds[2], 255,
        "load from '%s/%s' delimiter ',' insert into tenpct;\n",
        dataDir, FILENAME_TENPCT);
    snprintf(sqlCmds[3], 255,
        "load from '%s/%s' delimiter ',' insert into uniques;\n",
        dataDir, FILENAME_UNIQUES);
    snprintf(sqlCmds[4], 255,
        "load from '%s/%s' delimiter ',' insert into tiny;\n",
        dataDir, FILENAME_TINY);
    for (i=0; i<5; i++)
    {
        cmds[i]=sqlCmds[i];
    }
    cmds[5]=(char*)"\\q\n";
    cmds[6]=NULL;
    args[0]=(char*)"dbaccess";
    args[1]=(char*)"osdb";
    args[2]=(char*)"-";
    args[3]=NULL;
    runProg(args,cmds);
    return 0;
}


void
setDefaults()
{
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=NULL;
    DBNAME="osdb";
    DBPORT=0;
    OSDB_ERROR.error=ERR_OK;
    OSDB_ERROR.text=(char*)"no error text";
}

