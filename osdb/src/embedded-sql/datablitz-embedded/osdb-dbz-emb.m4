/* Based on osdb-pg-emb.m4 */
/* C preprocessor macros */
/* Contact: Annamalai Gurusami <annamalai.gurusami@email.masconit.com> */

/* Todo List:
 *
 * Would like to remove the macros that is not required.  
 * Also would be removing/updating the comments 
 */

#define EMB_DUPLICATE_ERROR -239
#define EMB_NOTFOUND_ERROR  100

m4_changequote([,])
m4_changequote(`,~)

m4_define(`BTREE~,`($*)~)m4_dnl
m4_define(`CLUSTER~,`~)m4_dnl
m4_define(`HASH~,`($*)~)m4_dnl

/* m4_define(`USE_INTO_TABLE~, `~)m4_dnl */
m4_define(`USE_INTO_TEMP~, `into temp $1~)m4_dnl

/* For the following macros, the first arg is the var name, the
 * second is optional for parameters such as "NOT NULL."
 */

m4_define(`CHAR10~, `$1    char(10) $2~)
m4_define(`CHAR20~, `$1    char(20) $2~)
m4_define(`DATETIME8~, `$1    char(20) $2~)
m4_define(`DOUBLE8~, `$1    float $2~)
m4_define(`INT4~, `$1    integer $2~)
m4_define(`NUMERIC18_2~, `$1    numeric(18,2) $2~)
m4_define(`REAL4~, `$1    float $2~)
m4_define(`VARCHAR80~, `$1    varchar(80) $2~)

m4_define(`CREATE_TABLE~, `exec sql create table $1($2);~);

/* In Dharma, the drop index syntax is 
 *     drop index index_name; 
 * Table name is not used.
 */
m4_define(`DROP_INDEX~, `exec sql drop index $2;~);

m4_define(`DECLARE_COLUMNS~,
    `exec sql begin declare section;m4_dnl
        char   col_key[20], m4_dnl
               col_int[20], m4_dnl
               col_signed[20], m4_dnl
               col_float[20], m4_dnl
               col_double[30], m4_dnl
               col_decim[22], m4_dnl
               col_date[20], m4_dnl
               col_code[11], m4_dnl
               col_name[21]; m4_dnl
        char   col_address[80]; m4_dnl
    exec sql end declare section;~
        char  column_buffer[300];
)

m4_define(`FETCH_TUPLE~,
    `exec sql fetch CURRENT_CURSOR into m4_dnl
               :col_key, m4_dnl
               :col_int, m4_dnl
               :col_signed, m4_dnl
               :col_float, m4_dnl
               :col_double, m4_dnl
               :col_decim, m4_dnl
               :col_date, m4_dnl
               :col_code, m4_dnl
               :col_name, m4_dnl
               :col_address; m4_dnl
        if (sqlca.sqlcode==0) {  /* Only if the query succeeded */
            snprintf(column_buffer, 300,
                "%s %s %s %s %s %s %s %s %s %s\n",
                col_key, col_int, col_signed, col_float,
                col_double, col_decim, col_date, col_code,
                col_name, col_address);~
        }
)


/* Postgres's ecpg, at least, requires unique cursor names for 
 * each block, so we'll generate unique names here.
 */

m4_define(`CURSOR_DECLARE~,
               `m4_define(`CURSOR_N~,m4_incr(CURSOR_N))m4_dnl
                m4_define(`CURRENT_CURSOR~,`cursor_~CURSOR_N)m4_dnl
                exec sql declare CURRENT_CURSOR cursor for $1;~)m4_dnl
m4_define(`CURSOR_N~, `0~)

m4_include(`../embedded-sql.m4~)

/* BEGIN ANNA */
/* Dharma's ESQLC does NOT have this statement.  */
m4_define(`TRANSACTION_BEGIN~, `~)m4_dnl
m4_define(`TRANSACTION_COMMIT~, `exec sql commit work;~)m4_dnl
m4_define(`BTREE~,`($*)~)m4_dnl /* currently no type is given */
m4_define(`HASH~,`($*)~)m4_dnl /* currently no type is given */
m4_define(`CLUSTER~,`~)m4_dnl
m4_define(`TRANSACTION_ROLLBACK~, `exec sql rollback work;~)m4_dnl
/* END ANNA */

m4_include(`../../the-tests.m4~)


 
int
argument_check(char* arg) {
    return(FALSE);
}


void
argument_choices() {
}


void
bugout(int terminate, char* stg, char* fileName, int lineNo)
{
    exec sql begin declare section;
        integer     i,
                infMsgLen,
                numExc;
        char    infMsg[8192];
    exec sql end declare section;

    fprintf(stderr,
            "\nError in test %s at (%d)%s:%d:\n... %s\n",
            currentTest, getpid(), fileName, lineNo, stg); 
    if (sqlca.sqlcode)
        fprintf(stderr,
                "sql: %ld, %s\n", sqlca.sqlcode, sqlca.sqlerrm);
    if (errno)
        perror("perror() reports");
    fprintf(stderr,
            "OSDB_ERROR.error: %d\n",
            OSDB_ERROR.error);
    if (++failureCount >= MAX_FAILURES)
    {
        terminate=1;
        fprintf(stderr,
                "\nThreshold of %d errors reached.\n", failureCount);
    }
    if (terminate)
    {
        exec sql disconnect 'osdb';
        if (iAmParent)
        {
            kill(-getpid(),SIGHUP);
            sleep(5);
            exit(1);
        }
        kill(0,SIGHUP);
        exit(1);
    }
    OSDB_ERROR.error=OSDB_ERROR.ERR_UNKNOWN;
    testFailed=1;
}


void
create_database()
{
char    *args[8],
    *cmds[8];
     args[0]=(char*)"syscreat";
     args[1]=(char*)"osdb";
     args[2]=NULL;
     cmds[0]=NULL;
     runProg(args, cmds);
}


int
create_idx_hundred_foreign()
{
    if (doIndexes)  // Pointless without index on updates.key
    {
        /* This statement is not being parsed properly by Dharma's esqlc
           compiler.  So using isql tool for this purpose -anna 
           exec sql alter table hundred add FOREIGN key (col_signed) 
                    references updates (col_key);  */
        char *args[7];
        char *cmds[5];
        args[0] = "isql";
        args[1] = "-u";
        args[2] = "datablitz";
        args[3] = "-a";
        args[4] = "datablitz";
        args[5] = "osdb";
        args[6] = NULL;

        cmds[0] = "alter table hundred add foreign key (col_signed) "
                  "references updates (col_key);\n";
        cmds[1] = "select * from blzdba.sys_ref_constrs;\n";
        cmds[2] = "commit work;\n";
        cmds[3] = "quit;\n";
        cmds[4] = NULL;
        runProg(args, cmds); 
    }
    return 0;
}


void databaseConnect()
{
    exec sql connect to 'osdb';
}

void databaseCommit()
{
   exec sql commit work;
}

void
databaseCreate(char* nosuch)
{
    /* I am not going to do anything here for now */
    /* Probably call syscreat to create a new db -anna */
}


void databaseDisconnect()
{
    exec sql commit work;
    exec sql disconnect 'osdb';
}

int load_updates()
{
   char cmd[256];
   snprintf(cmd, 255, "cd data; dbload -c 10000 -f %s osdb", "updates.cmd");
   printf("dbz - %s\n", cmd);
   return system(cmd);
}

int load_hundred()
{
   char cmd[256];
   snprintf(cmd, 255, "cd data; dbload -c 10000 -f %s osdb", "hundred.cmd");
   printf("dbz - %s\n", cmd);
   return system(cmd);
}
int load_tenpct()
{
   char cmd[256];
   snprintf(cmd, 255, "cd data; dbload -c 10000 -f %s osdb", "tenpct.cmd");
   printf("dbz - %s\n", cmd);
   return system(cmd);
}

int load_tiny()
{
   char cmd[256];
   snprintf(cmd, 255, "cd data; dbload -f %s osdb", "tiny.cmd");
   printf("dbz - %s\n", cmd);
   return system(cmd);
}

int
load()
{
    char    *args[4],
            *cmds[8],
            sqlCmds[5][256];
    int     i;
    
    load_updates();
    load_hundred();
    load_tenpct();
    load_tiny();
    return 0;
}


void
setDefaults()
{
    dataDir=NULL;
    osdblog=stdout;
    DBHOST=NULL;
    DBUSER=NULL;
    DBNAME="osdb";
    DBPORT=0;
    OSDB_ERROR.error=OSDB_ERROR.ERR_OK;
    OSDB_ERROR.text=(char*)"no error text";
}

