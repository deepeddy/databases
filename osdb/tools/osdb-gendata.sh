#!/bin/bash
# This is a script to run the as3apgen.exe DOS utility, through Wine, on Linux.  Works on Mandrake Linux 8.0
# Version : 1.01
# Author  : Justin Clift
# Date    : 19-22nd August 2001

# These variables need to be pointed to the correct locations (no trailing / on the end of TARGETDIR)
AS3APGENLOC=/opt/pgsql/as3apgen.exe
TARGETDIR=/db/osdb/data
WINELOC=/usr/X11R6/bin/wine

# Locations of other programs used in this script, just in case they need to be
# changed to match different platforms
CHMODLOC=/bin/chmod
DATELOC=/bin/date
MVLOC=/bin/mv
MKDIRLOC=/bin/mkdir
RMDIRLOC=/bin/rmdir

# Give a message identifying this program
echo "osdb-gendata.sh - OSDB script to generate AS3AP benchmark data with Wine and DBStar's as3apgen.exe"
echo "Written by Justin Clift <justin@postgresql.org>, 19-22nd August 2001"
echo

# Check if the user has configured the variables for this script
if [ "$TARGETDIR" = "unset" ]
then
  echo "ERROR - You need to adjust a few variables at the start of this script before running it!"
  echo "Specifically the location of the as3apgen.exe program, the location of 'wine' (the windows"
  echo "compatibility layer), and the target directory."
  echo
  exit
fi

# Check if Wine is where its supposed to be
if [ ! -e "$WINELOC" ]
then
  echo "ERROR - Can't find the Wine executable at $WINELOC!"
  exit
fi

# Check if as3apgen.exe is where its supposed to be
if [ ! -e "$AS3APGENLOC" ]
then
  echo "ERROR - Can't find the as3apgen.exe utility at $AS3APGENLOC!"
  exit
fi

# Ask if the data should be fixed-width or not
unset FFY_N
unset FIXEDWIDTH
until [[ "$FFY_N" = "f" || "$FFY_N" = "F" || "$FFY_N" = "v" || "$FFY_N" = "V" ]]
do
read -p "Should the generated data be fixed-width or variable-width (f or v)? " FFY_N
done
if [[ "$FFY_N" = "f" || "$FFY_N" = "F" ]]
then
  FIXEDWIDTH="-ff"
fi

# Create temporary directory
if [ ! -d "tmp/dataset" ]
then
  $MKDIRLOC -p tmp/dataset
fi
if [ ! -d "tmp/dataset" ]
then
  echo "Need to be able to create a temporary directory ./tmp/dataset but can't!"
  exit
fi

cd tmp/dataset

# Create target directory
echo Creating $TARGETDIR directory
if [ ! -d "$TARGETDIR" ]
then
  $MKDIRLOC $TARGETDIR
fi
if [[ ! -d "$TARGETDIR" || ! -w "$TARGETDIR" ]]
then
  echo "$TARGETDIR doesn't exist and can't be created!  Maybe a permissions problem?"
  exit
fi

# Ask which size database should be created
unset DBSIZE
until [[ "$DBSIZE" = "a" || "$DBSIZE" = "A" || "$DBSIZE" = "b" || "$DBSIZE" = "B" || "$DBSIZE" = "c" || "$DBSIZE" = "C" || "$DBSIZE" = "d" || "$DBSIZE" = "D" || "$DBSIZE" = "e" || "$DBSIZE" = "E" ]]
do
  echo "a. 4MB  b. 44MB  c. 440MB  d. 4.4GB  e. 44GB"
  read -p "Which size benchmark data should be generated? " DBSIZE
  echo
done

# Generate the data

case "$DBSIZE" in
  ("a" | "A")
    echo "Generating 4MB dataset..."
    echo "Ignore any error messages about stuff like: \"fixme:console:SetConsoleCtrlHandler\" etc"
    $WINELOC -- $AS3APGENLOC "$FIXEDWIDTH" -4 -r
    $MVLOC asap.hun $TARGETDIR/asap.hundred
    $MVLOC asap.rpt $TARGETDIR/asap.report
    $MVLOC asap.ten $TARGETDIR/asap.tenpct
    $MVLOC asap.tny $TARGETDIR/asap.tiny
    $MVLOC asap.uni $TARGETDIR/asap.uniques
    $MVLOC asap.upd $TARGETDIR/asap.updates
  ;;
  ("b" | "B")
    echo "Generating 44MB dataset..."
    echo "Ignore any error messages about stuff like: \"fixme:console:SetConsoleCtrlHandler\" etc"
    $WINELOC -- $AS3APGENLOC "$FIXEDWIDTH" -40 -r
    $MVLOC asap.hun $TARGETDIR/asap.hundred
    $MVLOC asap.rpt $TARGETDIR/asap.report
    $MVLOC asap.ten $TARGETDIR/asap.tenpct
    $MVLOC asap.tny $TARGETDIR/asap.tiny
    $MVLOC asap.uni $TARGETDIR/asap.uniques
    $MVLOC asap.upd $TARGETDIR/asap.updates
  ;;
  ("c" | "C")
    echo "Generating 440MB dataset..."
    echo "Ignore any error messages about stuff like: \"fixme:console:SetConsoleCtrlHandler\" etc"
    $WINELOC -- $AS3APGENLOC "$FIXEDWIDTH" -400 -r
    $MVLOC asap.hun $TARGETDIR/asap.hundred
    $MVLOC asap.rpt $TARGETDIR/asap.report
    $MVLOC asap.ten $TARGETDIR/asap.tenpct
    $MVLOC asap.tny $TARGETDIR/asap.tiny
    $MVLOC asap.uni $TARGETDIR/asap.uniques
    $MVLOC asap.upd $TARGETDIR/asap.updates
  ;;
  ("d" | "D")
    echo "Generating 4.4GB dataset..."
    echo "Ignore any error messages about stuff like: \"fixme:console:SetConsoleCtrlHandler\" etc"
    $WINELOC -- $AS3APGENLOC "$FIXEDWIDTH" -r -n 10000000 
    $MVLOC asap.hun $TARGETDIR/asap.hundred
    $MVLOC asap.rpt $TARGETDIR/asap.report
    $MVLOC asap.ten $TARGETDIR/asap.tenpct
    $MVLOC asap.tny $TARGETDIR/asap.tiny
    $MVLOC asap.uni $TARGETDIR/asap.uniques
    $MVLOC asap.upd $TARGETDIR/asap.updates
  ;;
  ("e" | "E")
    echo "Generating 44GB dataset..."
    echo "Ignore any error messages about stuff like: \"fixme:console:SetConsoleCtrlHandler\" etc"
    $WINELOC -- $AS3APGENLOC "$FIXEDWIDTH" -r -n 100000000
    $MVLOC asap.hun $TARGETDIR/asap.hundred
    $MVLOC asap.rpt $TARGETDIR/asap.report
    $MVLOC asap.ten $TARGETDIR/asap.tenpct
    $MVLOC asap.tny $TARGETDIR/asap.tiny
    $MVLOC asap.uni $TARGETDIR/asap.uniques
    $MVLOC asap.upd $TARGETDIR/asap.updates
  ;;
esac

# Ensure data files can't be changed

$CHMODLOC 444 $TARGETDIR/asap*

# Remove temporary directory

cd ../..
$RMDIRLOC tmp/dataset

echo
echo "AS3AP Data Generated and placed in $TARGETDIR"
echo
