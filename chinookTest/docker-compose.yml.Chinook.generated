#docker-compose -f /databases/chinookTest/docker-compose.yml.Chinook.generated --project-name ContainerizedQuery  up -d
#docker stack deploy --compose-file /databases/chinookTest/docker-compose.yml.Chinook.generated query

version: '2.1'

services:

  rabbitMQ:
    image: rabbitmq:3.6.12-management
    ports:
     # allow access to rabbitmq monitor using HTTP over port 8080
     - 8080:15672
    hostname: ${HOSTNAME}
    container_name: rabbitMQ
    expose:
     # rabbitmq server port for communication with clients, supports TLS
     - 5672
    restart: always
    stdin_open: true
    tty: true

  DC-6:
    image: busybox:latest
    volumes:
     - ${HOME}/databases/chinookTest:/databases/chinookTest
    environment:
      COMPOSE_PROJECT_NAME: ContainerizedQuery
      DATABASE: chinook.db
      TABLE: tracks
      PARTITIONS: "[tracksPart_1, tracksPart_2]"
      SCHEMA: "Schema [relationName=tracks, schema=[TrackId INTEGER PRIMARY KEY; Name NVARCHAR(200); AlbumId INTEGER; MediaTypeId INTEGER; GenreId INTEGER; Composer NVARCHAR(220); Milliseconds INTEGER; Bytes INTEGER; UnitPrice NUMERIC(10,2)]]"
    container_name: DC-6
    restart: unless-stopped

  DC-7:
    image: busybox:latest
    volumes:
     - ${HOME}/databases/chinookTest:/databases/chinookTest
    environment:
      COMPOSE_PROJECT_NAME: ContainerizedQuery
      DATABASE: chinook.db
      TABLE: genres
      PARTITIONS: null
      SCHEMA: "Schema [relationName=genres, schema=[GenreId INTEGER PRIMAR; Name NVARCHAR(120)]]"
    container_name: DC-7
    restart: unless-stopped

  Master:
    image: deepeddy/mysqlite:master
    volumes:
       - ${HOME}/databases/chinookTest:/databases/chinookTest
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      TEXTUAL_QUERY_FILE: /databases/chinookTest//databases/chinookTest/Chinook.txt
      COMPOSE_FILE_NAME: /databases/chinookTest//databases/chinookTest/docker-compose.yml.Chinook.generated.generated
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
      COMPOSE_VERSION: 2.1
    depends_on:
      - rabbitMQ
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Master $${TEXTUAL_QUERY_FILE} $${RabbitIP}  $${COMPOSE_FILE_NAME} /databases/chinookTest 2.1 verbose;  /bin/bash -i " 
    container_name: Master
    stdin_open: true
    tty: true

  C1-2:
    image : deepeddy/mysqlite:worker
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: interiorCompute
      OPERATOR: ORDER
      PREDICATE: C2.Composer, <
      QUERY: SELECT * FROM C2 ORDER BY C2.Composer DESC
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - C1-MERGE
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite::memory:;Journal Mode=Off;Synchronous=Off;' $${RabbitIP} C1-2 interiorCompute verbose;  /bin/bash -i " 
    container_name: C1-2
    stdin_open: true
    tty: true

  C1-MERGE:
    image : deepeddy/mysqlite:worker
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: interiorCompute
      OPERATOR: SELECT
      PREDICATE: 1 = 1
      QUERY: SELECT  * FROM C1 WHERE 1 = 1
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite::memory:;Journal Mode=Off;Synchronous=Off;' $${RabbitIP} C1-MERGE interiorCompute verbose;  /bin/bash -i " 
    container_name: C1-MERGE
    stdin_open: true
    tty: true

  C1:
    image : deepeddy/mysqlite:worker
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: interiorCompute
      OPERATOR: ORDER
      PREDICATE: C2.Composer, <
      QUERY: SELECT * FROM C2 ORDER BY C2.Composer DESC
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - C1-MERGE
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite::memory:;Journal Mode=Off;Synchronous=Off;' $${RabbitIP} C1 interiorCompute verbose;  /bin/bash -i " 
    container_name: C1
    stdin_open: true
    tty: true

  C2:
    image : deepeddy/mysqlite:worker
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: interiorCompute
      OPERATOR: PROJECT
      PREDICATE: C3.Composer, C3.Name
      QUERY: SELECT C3.Composer, C3.Name FROM C3
      EXCHANGE: EvenPartitionExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - C1
      - C1-2
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite::memory:;Journal Mode=Off;Synchronous=Off;' $${RabbitIP} C2 interiorCompute verbose;  /bin/bash -i " 
    container_name: C2
    stdin_open: true
    tty: true

  C3-2:
    image : deepeddy/mysqlite:worker
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: interiorCompute
      OPERATOR: JOIN
      PREDICATE: C4.GenreId = C5.GenreId
      QUERY: SELECT C4.TrackId, C4.Name, C4.AlbumId, C4.MediaTypeId, C4.GenreId, C4.Composer, C4.Milliseconds, C4.Bytes, C4.UnitPrice, C5.GenreId AS GenreId_genres, C5.Name AS Name_genres FROM C4, C5 WHERE C4.GenreId = C5.GenreId
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - C2
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite::memory:;Journal Mode=Off;Synchronous=Off;' $${RabbitIP} C3-2 interiorCompute verbose;  /bin/bash -i " 
    container_name: C3-2
    stdin_open: true
    tty: true

  C3:
    image : deepeddy/mysqlite:worker
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: interiorCompute
      OPERATOR: JOIN
      PREDICATE: C4.GenreId = C5.GenreId
      QUERY: SELECT C4.TrackId, C4.Name, C4.AlbumId, C4.MediaTypeId, C4.GenreId, C4.Composer, C4.Milliseconds, C4.Bytes, C4.UnitPrice, C5.GenreId AS GenreId_genres, C5.Name AS Name_genres FROM C4, C5 WHERE C4.GenreId = C5.GenreId
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - C2
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite::memory:;Journal Mode=Off;Synchronous=Off;' $${RabbitIP} C3 interiorCompute verbose;  /bin/bash -i " 
    container_name: C3
    stdin_open: true
    tty: true

  C4-2:
    image : deepeddy/mysqlite:worker
    volumes_from:
     - DC-6
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: leafSelect
      OPERATOR: SELECT
      PREDICATE: tracksPart_2.UnitPrice < 1.0 AND tracksPart_2.Composer IS NOT NULL
      QUERY: SELECT  * FROM tracksPart_2 WHERE tracksPart_2.UnitPrice < 1.0 AND tracksPart_2.Composer IS NOT NULL
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - DC-6
      - C3-2
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite:/databases/chinookTest/tracksPart_2.db' $${RabbitIP} C4-2 leafSelect verbose;  /bin/bash -i " 
    container_name: C4-2
    stdin_open: true
    tty: true

  C4:
    image : deepeddy/mysqlite:worker
    volumes_from:
     - DC-6
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: leafSelect
      OPERATOR: SELECT
      PREDICATE: tracksPart_1.UnitPrice < 1.0 AND tracksPart_1.Composer IS NOT NULL
      QUERY: SELECT  * FROM tracksPart_1 WHERE tracksPart_1.UnitPrice < 1.0 AND tracksPart_1.Composer IS NOT NULL
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - DC-6
      - C3
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite:/databases/chinookTest/tracksPart_1.db' $${RabbitIP} C4 leafSelect verbose;  /bin/bash -i " 
    container_name: C4
    stdin_open: true
    tty: true

  C5:
    image : deepeddy/mysqlite:worker
    volumes_from:
     - DC-7
    expose:
     # worker server socket listening for input from upstream  container
     - 4325
    environment:
      DATA_PORT: 4325
      RabbitIP: rabbitMQ
      WORKER_TYPE: leafSelect
      OPERATOR: SELECT
      PREDICATE: genres.Name = 'Classical' OR genres.Name = 'Opera'
      QUERY: SELECT  * FROM genres WHERE genres.Name = 'Classical' OR genres.Name = 'Opera'
      EXCHANGE: DefaultExchange 
      CP: .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar:sqlite-jdbc-3.19.3.jar:com.sun.rowset.jar
      JVM_OPTS: '-d64 -server -XX:+UseG1GC -verbose:gc -Xms2048M'
      COMPOSE_PROJECT_NAME: ContainerizedQuery
    depends_on:
      - Master
      - DC-7
      - C3
      - C3-2
    working_dir: /usr/src/containerizedQuerySources/
    command: bash -c " java -cp $${CP} $${JVM_OPTS} containerizedQuery/Worker 'jdbc:sqlite:/databases/chinookTest/chinook.db' $${RabbitIP} C5 leafSelect verbose;  /bin/bash -i " 
    container_name: C5
    stdin_open: true
    tty: true

